<?php defined('BASEPATH') OR exit('No direct script access allowed');


$route['root-cms'] = 'backend/dashboard/index';
$route['root-cms/dashboard'] = 'backend/dashboard/index';

//Auth
$route['auth/login'] = 'auth/login/index';
$route['auth/logout'] = 'auth/auth/logout';
$route['auth/login-proccess'] = 'auth/login/proccess';

$route['auth/register'] = 'auth/register/index';
$route['auth/register-proccess'] = 'auth/register/proccess';
$route['auth/register-success'] = 'auth/register/success';

$route['auth/forgot'] = 'auth/forgot/index';
$route['auth/forgot-proccess'] = 'auth/forgot/index';
$route['auth/forgot-reset/(:any)'] = 'auth/forgot/resetPassword';
$route['auth/forgot-reset-success'] = 'auth/forgot/resetSuccess';


// Posts News
$route['root-cms/news/create/add'] = 'backend/posts/news/add';
$route['root-cms/news/create/save'] = 'backend/posts/news/save';
$route['root-cms/news/create/edit/(:num)'] = 'backend/posts/news/edit';
$route['root-cms/news/create/update'] = 'backend/posts/news/update';
$route['root-cms/posts/news/delete'] = 'backend/posts/news/delete';
//posts news draft
$route['root-cms/news/draft/view'] = 'backend/posts/news_draft/index';
$route['root-cms/news/draft/lists'] = 'backend/posts/news_draft/lists';
//posts news publish
$route['root-cms/news/published/view'] = 'backend/posts/news_publish/index';
$route['root-cms/news/published/lists'] = 'backend/posts/news_publish/lists';
//posts news Scheduled
$route['root-cms/news/scheduled/view'] = 'backend/posts/news_scheduled/index';
$route['root-cms/news/scheduled/lists'] = 'backend/posts/news_scheduled/lists';
//posts news Trash
$route['root-cms/news/trash/view'] = 'backend/posts/news_trash/index';
$route['root-cms/news/trash/lists'] = 'backend/posts/news_trash/lists';
$route['root-cms/news/trash/delete_permanent'] = 'backend/posts/news_trash/delete';


// Posts News Photo
$route['root-cms/galleries/photos/view'] = 'backend/galleries/photos/photos/index';
$route['root-cms/galleries/photos/lists'] = 'backend/galleries/photos/photos/lists';
$route['root-cms/galleries/photos/add'] = 'backend/galleries/photos/photos/add';
$route['root-cms/galleries/photos/save'] = 'backend/galleries/photos/photos/save';
$route['root-cms/galleries/photos/edit/(:num)'] = 'backend/galleries/photos/photos/edit';
$route['root-cms/galleries/photos/update'] = 'backend/galleries/photos/photos/update';
$route['root-cms/galleries/photos/delete'] = 'backend/galleries/photos/photos/delete';
$route['root-cms/galleries/photos/delete_permanen'] = 'backend/galleries/photos/photos/delete_permanen';
// galleries Videos
$route['root-cms/galleries/videos/view'] = 'backend/galleries/videos/videos/index';
$route['root-cms/galleries/Videos/lists'] = 'backend/galleries/videos/videos/lists';
$route['root-cms/galleries/videos/add'] = 'backend/galleries/videos/videos/add';
$route['root-cms/galleries/videos/save'] = 'backend/galleries/videos/videos/save';
$route['root-cms/galleries/videos/edit/(:num)'] = 'backend/galleries/videos/videos/edit';
$route['root-cms/galleries/videos/update'] = 'backend/galleries/videos/videos/update';
$route['root-cms/galleries/videos/delete'] = 'backend/galleries/videos/videos/delete';
$route['root-cms/galleries/videos/delete_permanen'] = 'backend/galleries/videos/videos/delete_permanen';


// Managements Headline
$route['root-cms/managements/headline/view'] = 'backend/managements/headline/index';
$route['root-cms/managements/headline/save'] = 'backend/managements/headline/save';
// Managements Choice
$route['root-cms/managements/pilihan/view'] = 'backend/managements/pilihan/index';
$route['root-cms/managements/pilihan/save'] = 'backend/managements/pilihan/save';
// Managements topic
$route['root-cms/managements/topic/view'] = 'backend/managements/topic/index';
$route['root-cms/managements/topic/lists'] = 'backend/managements/topic/lists';
$route['root-cms/managements/topic/add'] = 'backend/managements/topic/add';
$route['root-cms/managements/topic/edit/(:num)'] = 'backend/managements/topic/edit';
$route['root-cms/managements/topic/save'] = 'backend/managements/topic/save';
$route['root-cms/managements/topic/update'] = 'backend/managements/topic/update';
$route['root-cms/managements/topic/delete'] = 'backend/managements/topic/delete';
// Managements pages
$route['root-cms/managements/pages/view'] = 'backend/managements/pages/index';
$route['root-cms/managements/pages/lists'] = 'backend/managements/pages/lists';
$route['root-cms/managements/pages/add'] = 'backend/managements/pages/add';
$route['root-cms/managements/pages/save'] = 'backend/managements/pages/save';
$route['root-cms/managements/pages/edit/(:num)'] = 'backend/managements/pages/edit';
$route['root-cms/managements/pages/update'] = 'backend/managements/pages/update';
$route['root-cms/managements/pages/delete'] = 'backend/managements/pages/delete';
$route['root-cms/managements/pages/delete_permanen'] = 'backend/managements/pages/delete_permanen';


// Filemanager Photos
$route['root-cms/filemanager/filemanagerPhotos/view'] = 'backend/filemanager/photos/photos/index';
$route['root-cms/filemanager/filemanagerPhotos/save'] = 'backend/filemanager/photos/photos/save';
$route['root-cms/filemanager/filemanagerPhotos/edit/(:num)'] = 'backend/filemanager/photos/photos/edit';
$route['root-cms/filemanager/filemanagerPhotos/update'] = 'backend/filemanager/photos/photos/update';
$route['root-cms/filemanager/filemanagerPhotos/delete'] = 'backend/filemanager/photos/photos/delete';
// Filemanager videos
$route['root-cms/filemanager/filemanagerVideos/view'] = 'backend/filemanager/videos/videos/index';
$route['root-cms/filemanager/filemanagerVideos/save'] = 'backend/filemanager/videos/videos/save';
$route['root-cms/filemanager/filemanagerVideos/edit/(:num)'] = 'backend/filemanager/videos/videos/edit';
$route['root-cms/filemanager/filemanagerVideos/update'] = 'backend/filemanager/videos/videos/update';
$route['root-cms/filemanager/filemanagerVideos/delete'] = 'backend/filemanager/videos/videos/delete';

// Report
$route['root-cms/report/view'] = 'backend/report/report/index';
$route['root-cms/report/view/news'] = 'backend/report/report/newsList';

// Settings users
$route['root-cms/settings/users'] = 'backend/settings/users/index';
$route['root-cms/settings/users/lists'] = 'backend/settings/users/lists';
$route['root-cms/settings/users/add'] = 'backend/settings/users/add';
$route['root-cms/settings/users/edit/(:num)'] = 'backend/settings/users/edit';
$route['root-cms/settings/users/update'] = 'backend/settings/users/update';
$route['root-cms/settings/users/view/(:num)'] = 'backend/settings/users/detail';
$route['root-cms/settings/users/delete'] = 'backend/settings/users/delete';

// news user
$route['root-cms/settings/users/view/newsList'] = 'backend/settings/users/newsList';
$route['root-cms/settings/users/view/newsPhoto'] = 'backend/settings/users/newsPhoto';
$route['root-cms/settings/users/view/newsVideo'] = 'backend/settings/users/newsVideo';

// Serttings groups
$route['root-cms/settings/groups'] = 'backend/settings/groups/index';
$route['root-cms/settings/groups/lists'] = 'backend/settings/groups/lists';
$route['root-cms/settings/groups/add'] = 'backend/settings/groups/add';
$route['root-cms/settings/groups/edit/(:num)'] = 'backend/settings/groups/edit';
$route['root-cms/settings/groups/update'] = 'backend/settings/groups/update';
$route['root-cms/settings/groups/delete'] = 'backend/settings/groups/delete';

// Serttings rubrik
$route['root-cms/settings/rubrik/view'] = 'backend/settings/rubrik/index';
$route['root-cms/settings/rubrik/save'] = 'backend/settings/rubrik/save';
$route['root-cms/settings/rubrik/edit/(:num)'] = 'backend/settings/rubrik/edit';
$route['root-cms/settings/rubrik/update'] = 'backend/settings/rubrik/update';
$route['root-cms/settings/rubrik/delete'] = 'backend/settings/rubrik/delete';

// Serttings tags
$route['root-cms/settings/tags/view'] = 'backend/settings/tags/index';
$route['root-cms/settings/tags/lists'] = 'backend/settings/tags/lists';
$route['root-cms/settings/tags/save'] = 'backend/settings/tags/save';
$route['root-cms/settings/tags/edit/(:num)'] = 'backend/settings/tags/edit';
$route['root-cms/settings/tags/update'] = 'backend/settings/tags/update';
$route['root-cms/settings/tags/delete'] = 'backend/settings/tags/delete';
// Serttings menus
$route['root-cms/settings/menus/view'] = 'backend/settings/menus/index';
$route['root-cms/settings/menus/lists'] = 'backend/settings/menus/lists';
$route['root-cms/settings/menus/save'] = 'backend/settings/menus/save';
$route['root-cms/settings/menus/edit/(:num)'] = 'backend/settings/menus/edit';
$route['root-cms/settings/menus/update'] = 'backend/settings/menus/update';
$route['root-cms/settings/menus/delete'] = 'backend/settings/menus/delete';


// users view
$route['root-cms/users/(:num)'] = 'backend/users/profile/detail';
$route['root-cms/users/update'] = 'backend/users/profile/update';


// JSON Select2
$route['root-cms/JSON_select/rubrik_list'] = 'backend/Json_select/rubrik_list';
$route['root-cms/JSON_select/tags'] = 'backend/Json_select/tags';
$route['root-cms/JSON_select/author'] = 'backend/Json_select/author';
$route['root-cms/JSON_select/topic'] = 'backend/Json_select/topic';
$route['root-cms/JSON_select/related'] = 'backend/Json_select/related';
$route['root-cms/JSON_select/menu_list'] = 'backend/Json_select/menu_list';

// iframe
$route['root-cms/IframeContent/images'] = 'backend/IframeContent/imagesList';
$route['root-cms/IframeContent/images/edit/(:num)'] = 'backend/IframeContent/imageEdit';
$route['root-cms/IframeContent/videos'] = 'backend/IframeContent/videosList';
$route['root-cms/IframeContent/newsList'] = 'backend/IframeContent/newsList';


// frontend
$route['read/(:any)'] = 'welcome/read';






$route['default_controller'] = 'welcome';
$route['404_override'] = 'welcome/error';
$route['translate_uri_dashes'] = FALSE;
