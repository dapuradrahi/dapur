<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NewsModel extends CI_Model{
	
	function __construct() {

		$this->table = 'news';
		$this->fields = '
		news.id as news_id,
		author_id,
		editor_id,
		publisher_id,
		news.channel as news_channel,
		news.category_id as news_category_id,
		news.topic_id as news_topic_id,
		news.prefik as news_prefik,
		news.title as news_title,
		news.slug as news_slug,
		news.excerpt as news_excerpt,
		news.content as news_content,
		news.status as news_status,
		news.created_at as news_created_at,
		news.updated_at as news_updated_at,
		news.published_at as news_published_at,
		news.count as news_count,
		news.related_news as news_related_news,
		news.hash,
		categories.id as category_id,
		categories.name as category_name,
		categories.slug as category_slug,
		categories.status as category_status,
		categories.picture as category_picture,
		categories.description as category_desc,
		categories.keywords as category_keywords,
		categories.created_at as category_created,
		categories.updated_at as category_updated,
		categories.count as category_count';

		$this->column_order = array(null, 'news.id','title','created_at');
		$this->column_search = array('news.id','title');
		$this->order = array('news.published_at' => 'desc');
	}
	public function getRows($postData,$status,$user_id,$group,$channel){
		$this->_get_datatables_query($postData,$status,$user_id,$group,$channel);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData,$status,$user_id,$group,$channel){
		$this->_get_datatables_query($postData,$status,$user_id,$group,$channel);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData,$status,$user_id,$group,$channel){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->join('categories', 'categories.id=news.category_id','left');
		if ($group == 1 || $group == 2 || $group == 3) {

		}else{
			$this->db->where('author_id',$user_id);
		}

		if ($this->input->post('filter')) {
			$this->db->where('categories.id',$this->input->post('filter'));
		}
		
		if ($status == 'published') {
			$this->db->where('news.status','published');
			$this->db->where('news.published_at <=',date('Y-m-d H:i:s'));
		}elseif ($status == 'scheduled') {
			$this->db->where('news.status','published');
			$this->db->where('news.published_at >',date('Y-m-d H:i:s'));
		}elseif ($status == 'draft') {
			$this->db->where('news.status',$status);
		}else{
			$this->db->where('news.status','deleted');
		}
		$this->db->where('news.channel',$channel);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function related_news($newsId){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->join('categories', 'categories.id=news.category_id','left');
		$this->db->where_in('news.id',$newsId);
		return $this->db->get();
	}

	public function get_news_paginate($limit, $start,$channel='',$category_id='',$keywords= '',$date){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->join('categories', 'categories.id=news.category_id','left');

		if ($channel != '') {
			$this->db->where('news.channel', $channel);
		}

		if ($category_id != '') {
			$this->db->where('news.category_id', $category_id);
		}

		if ($keywords != '') {
			$this->db->like('news.title', $keywords);
		}

		if (!empty($date)) {
			$this->db->where('news.created_at >', date('Y-m-d',strtotime($date[0])));
			$this->db->where('news.created_at <', date('Y-m-d',strtotime($date[1])));
		}
		$this->db->where('news.status','published');
		$this->db->limit($limit,$start);
		// $this->db->order_by('news.id', 'DESC');
		$this->db->order_by('news.published_at', 'DESC');
		$query = $this->db->get();
		return $query;
	}

	public function saveNews($data,$tags)
	{
		$this->db->trans_start();
		$this->db->insert($this->table, $data);
		$newsId = $this->db->insert_id();

		for ($i=0; $i < count($tags); $i++) { 
			$dataTag = array(
				'news_id' => $newsId,
				'type' => 'news',
				'tag_id' => $tags[$i],
			);
			$this->db->insert('news_tags',$dataTag);
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return FALSE;
		}else{
			return $newsId;
		}
	}

	public function update($data,$tags,$newsId){
		$this->db->trans_start();
		$this->db->where('news.id', $newsId);
		$this->db->update($this->table, $data);

		//update tags
		$this->db->where('news_tags.news_id',$newsId);
		$this->db->delete('news_tags');

		for ($i=0; $i < count($tags); $i++) { 
			$dataTag = array(
				'news_id' => $newsId,
				'type' => 'news',
				'tag_id' => $tags[$i],
			);
			$this->db->insert('news_tags',$dataTag);
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return FALSE;
		}else{
			return TRUE;
		}
	}

	public function getNewsById($id){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('news.id',$id);
		$this->db->join('categories', 'categories.id=news.category_id','left');
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q;
		}else{
			return FALSE;
		}

	}


	// only change status post to deleted
	public function newsDelete($newsId){
		$this->db->where('news.id', $newsId);
		$this->db->update($this->table, ['status'=>'deleted']);
		return ($this->db->affected_rows() != 1) ? false : true;

		$this->db->where('news.id',$newsId);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function newsDeletePermanent($newsId){
		$this->db->where('news.id',$newsId);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}


}