<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HeadlineModel extends CI_Model{
	
	function __construct() {

		$this->table = 'news_headline';
		$this->fields = '
		news_headline.id as headline_id,
		news_headline.news_id as headline_news_id,
		news_headline.type as headline_type,
		news_headline.created_at as headline_created_at,
		news_headline.updated_at as headline_updated_at,
		news_headline.channel as headline_channel
		';
	}

	public function headline($type,$channel){
		$this->db->select($this->fields);
		$this->db->where('news_headline.type',$type); //type 0 for root and > 0 for categories
		$this->db->where('news_headline.channel',$channel);
		return $this->db->get($this->table);
	}

	public function getNewsHeadline($newsId){
		$this->db->where_in('news.id',$newsId);
		return $this->db->get('news')->result_array();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function update($headline_id,$data){
		$this->db->where('news_headline.id', $headline_id);
		$this->db->update($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

}