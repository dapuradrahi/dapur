<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PicturesModel extends CI_Model{
	
	function __construct() {
		$this->table = 'pictures';
		$this->fields = '
		pictures.id as picture_id,
		file_name,
		pictures.title,
		pictures.type,
		caption,
		source,
		pictures.channel,
		pictures.status as picture_status,
		pictures.created_at,
		pictures.updated_at,
		users.id as user_id,
		users.first_name,
		users.last_name,
		users.username,
		users.email';

		$this->column_order = array(null, 'file_name','users.first_name','users.username','users.email');
		$this->column_search = array('file_name','users.first_name','users.username','users.email');
		$this->order = array('pictures.id' => 'desc');
	}
	public function getRows($postData){
		$this->_get_datatables_query($postData);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData){
		$this->_get_datatables_query($postData);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData){
		$this->db->from($this->table);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_pictures($limit, $start,$channel='',$type='',$keywords= '',$date,$pictureId=''){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->join('users','users.id = pictures.user_id','left');

		if ($channel != '') {
			$this->db->where('pictures.channel', $channel);
		}

		if ($type != '') {
			$this->db->where('pictures.type', $type);
		}

		if ($keywords != '') {
			$this->db->like('pictures.title', $keywords);
		}

		if (!empty($date)) {
			$this->db->where('pictures.created_at >', date('Y-m-d',strtotime($date[0])));
			$this->db->where('pictures.created_at <', date('Y-m-d',strtotime($date[1])));
		}

		if ($pictureId != '') {
			$this->db->where('pictures.id',$pictureId);
			$this->db->limit(1);
		}else{
			$this->db->limit($limit,$start);
		}
		$this->db->order_by('pictures.id', 'DESC');
		$query = $this->db->get();
		return $query;
	}

	public function getPicturebyId($pictureId){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('pictures.id',$pictureId);
		$this->db->join('users','users.id = pictures.user_id','left');
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q;
		}else{
			return FALSE;
		}
	}

	public function insertPicture($data){
		$this->db->insert($this->table,$data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function update($pictureId,$data)
	{
		$this->db->where('pictures.id', $pictureId);
		$this->db->update($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function delete($pictureId){
		$this->db->where('id', $pictureId);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

}