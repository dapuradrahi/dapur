<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PilihanModel extends CI_Model{
	
	function __construct() {

		$this->table = 'pilihan_editor';
		$this->fields = '
		pilihan_editor.id as pilihan_id,
		pilihan_editor.news_id as pilihan_news_id,
		pilihan_editor.type as pilihan_type,
		pilihan_editor.created_at as pilihan_created_at,
		pilihan_editor.updated_at as pilihan_updated_at,
		pilihan_editor.channel as pilihan_channel
		';
	}

	public function pilihan($type,$channel){
		$this->db->select($this->fields);
		$this->db->where('pilihan_editor.type',$type);
		$this->db->where('pilihan_editor.channel',$channel);
		return $this->db->get($this->table);
	}

	public function getNewsPilihan($newsId){
		$this->db->where_in('news.id',$newsId);
		return $this->db->get('news')->result_array();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function update($pilihan_id,$data){
		$this->db->where('pilihan_editor.id', $pilihan_id);
		$this->db->update($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function delete($pilihan_id)
	{
		$this->db->where('pilihan_editor.id', $pilihan_id);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

}