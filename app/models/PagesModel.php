<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PagesModel extends CI_Model{
	
	function __construct() {

		$this->table = 'pages';
		$this->fields = '
		pages.id as page_id,
		author_id,
		pages.channel as page_channel,
		pages.title as page_title,
		pages.slug as page_slug,
		pages.excerpt as page_excerpt,
		pages.content as page_content,
		pages.status as page_status,
		pages.created_at as page_created_at,
		pages.updated_at as page_updated_at,
		pages.published_at as page_published_at,
		pages.count as page_count,
		pages.hash';

		$this->column_order = array(null, 'pages.id','title');
		$this->column_search = array('pages.id','title');
		$this->order = array('pages.id' => 'desc');
	}
	public function getRows($postData,$status,$channel){
		$this->_get_datatables_query($postData,$status,$channel);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData,$status,$channel){
		$this->_get_datatables_query($postData,$status,$channel);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData,$status,$channel){
		$this->db->select($this->fields);
		$this->db->from($this->table);

		if ($this->input->post('filter')) {
			$this->db->where('pages.channel',$this->input->post('filter'));
		}
		
		if ($status == 'published') {
			$this->db->where('pages.status','published');
			$this->db->where('pages.published_at <=',date('Y-m-d H:i:s'));
		}elseif ($status == 'draft') {
			$this->db->where('pages.status',$status);
		}else{
			$this->db->where('pages.status','deleted');
		}
		$this->db->where('pages.channel',$channel);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		$pageId = $this->db->insert_id();
		return ($this->db->affected_rows() != 1) ? false : $pageId;
	}

	public function getPageById($pageId){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('pages.id',$pageId);
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q;
		}else{
			return FALSE;
		}

	}

	public function update($pageId,$data){
		$this->db->where('pages.id', $pageId);
		$this->db->update($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}


	// only change status post to deleted
	public function pageDelete($pageId){
		$this->db->where('pages.id', $pageId);
		$this->db->update($this->table, ['status'=>'deleted']);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function pageDeletePermanent($pageId){
		$this->db->where('pages.id',$pageId);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}


}