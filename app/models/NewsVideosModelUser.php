<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NewsVideosModelUser extends CI_Model{
	
	function __construct() {

		$this->table = 'news_videos';
		$this->fields = '
		news_videos.id as news_videos_id,
		news_videos.author_id as news_videos_author_id,
		news_videos.editor_id as news_videos_editor_id,
		news_videos.publisher_id as news_videos_publisher_id,
		news_videos.title as news_videos_title,
		news_videos.slug as news_videos_slug,
		news_videos.excerpt as news_videos_excerpt,
		news_videos.videos as news_videos_videos,
		news_videos.content as news_videos_content,
		news_videos.status as news_videos_status,
		news_videos.related_news as news_videos_related_news,
		news_videos.created_at as news_videos_created_at,
		news_videos.updated_at as news_videos_updated_at,
		news_videos.published_at as news_videos_published_at,
		news_videos.channel as news_videos_channel,
		news_videos.hash as news_videos_hash,
		news_videos.count as news_videos_count';

		$this->column_order = array(null, 'news_videos.id','news_videos.title','news_videos.created_at');
		$this->column_search = array('news_videos.id','news_videos.title');
		$this->order = array('news_videos.published_at' => 'desc');
	}
	public function getRows($postData,$status,$user_id,$filterDate,$channel){
		$this->_get_datatables_query($postData,$status,$user_id,$filterDate,$channel);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData,$status,$user_id,$filterDate,$channel){
		$this->_get_datatables_query($postData,$status,$user_id,$filterDate,$channel);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData,$status,$user_id,$filterDate,$channel){
		$this->db->select($this->fields);
		$this->db->from($this->table);

		$this->db->where('news_videos.author_id',$user_id);
		
		if ($status == 'published') {
			$this->db->where('news_videos.status','published');
			$this->db->where('news_videos.published_at <=',date('Y-m-d H:i:s'));
		}elseif ($status == 'scheduled') {
			$this->db->where('news_videos.status','published');
			$this->db->where('news_videos.published_at >',date('Y-m-d H:i:s'));
		}elseif ($status == 'draft') {
			$this->db->where('news_videos.status',$status);
		}elseif ($status == 'deleted') {
			$this->db->where('news_videos.status','deleted');
		}else{
			// $this->db->where('news_videos.status','deleted');
			$this->db->where_not_in('news_videos.status', 'deleted');
		}

		$this->db->where('news_videos.published_at >=',date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $filterDate[0]))));
		$this->db->where('news_videos.published_at <=',date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $filterDate[1]))));
		
		$this->db->where('news_videos.channel',$channel);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


}