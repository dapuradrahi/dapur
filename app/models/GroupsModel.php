<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class GroupsModel extends CI_Model{
	
	function __construct() {
		$this->tableAccess = 'access';

		$this->table = 'groups';
		$this->fields = 'id,name,description';

		$this->column_order = array(null, 'id','name','description');
		$this->column_search = array('id','name','description');
		$this->order = array('id' => 'desc');
	}
	public function getRows($postData){
		$this->_get_datatables_query($postData);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData){
		$this->_get_datatables_query($postData);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData){
		$this->db->from($this->table);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function getGroup($groupId){
		$this->db->select('groups.id as groupId, name,description,access.access,created_at,updated_at');
		$this->db->where('groups.id',$groupId);
		$this->db->join('access','groups.id = access.group_id');
		return $this->db->get($this->table);
	}

	public function addAccessGroup($data)
	{
		$this->db->insert($this->tableAccess, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function updateAccessGroup($groupId,$data){
		$this->db->where('group_id', $groupId);
		$this->db->update($this->tableAccess, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function deleteAccessGroup($groupId)
	{
		$this->db->where('group_id', $groupId);
		$this->db->delete($this->tableAccess);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

}