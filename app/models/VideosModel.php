<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VideosModel extends CI_Model{
	
	function __construct() {
		$this->table = 'videos';
		$this->fields = '
		videos.id as videos_id,
		videos.title,
		videos.link,
		videos.description,
		videos.status,
		videos.source,
		videos.channel,
		videos.created_at,
		videos.updated_at,
		users.id as user_id,
		users.first_name,
		users.last_name,
		users.username,
		users.email';

		$this->column_order = array(null, 'videos.title','users.first_name','users.username','users.email');
		$this->column_search = array('videos.title','users.first_name','users.username','users.email');
		$this->order = array('videos.id' => 'desc');
	}
	public function getRows($postData){
		$this->_get_datatables_query($postData);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData){
		$this->_get_datatables_query($postData);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData){
		$this->db->from($this->table);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_videos($limit, $start, $channel='',$status='',$keywords='',$date,$videoId = ''){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->join('users','users.id = videos.user_id','left');

		if ($channel != '') {
			$this->db->where('videos.channel', $channel);
		}

		if ($status != '') {
			$this->db->where('videos.status', $status);
		}

		if ($keywords != '') {
			$this->db->like('videos.title', $keywords);
		}

		if (!empty($date)) {
			$this->db->where('videos.created_at >', date('Y-m-d',strtotime($date[0])));
			$this->db->where('videos.created_at <', date('Y-m-d',strtotime($date[1])));
		}

		if ($videoId != '') {
			$this->db->where('videos.id',$videoId);
			$this->db->limit(1);
		}else{
			$this->db->limit($limit,$start);
		}
		$this->db->order_by('videos.id', 'DESC');
		$query = $this->db->get();
		return $query;
	}

	public function getVideoById($videoId){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('videos.id',$videoId);
		$this->db->join('users','users.id = videos.user_id','left');
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q;
		}else{
			return FALSE;
		}
	}

	public function insertVideo($data){
		$this->db->insert($this->table,$data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function update($videoId,$data)
	{
		$this->db->where('videos.id', $videoId);
		$this->db->update($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function delete($videoId){
		$this->db->where('id', $videoId);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

}