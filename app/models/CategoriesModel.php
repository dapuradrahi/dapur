<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CategoriesModel extends CI_Model{
	
	function __construct() {
		$this->table = 'categories';
		$this->fields = '
		categories.id as category_id,
		categories.name as category_name,
		categories.slug as category_slug,
		categories.status as category_status,
		categories.picture as category_picture,
		categories.description as category_desc,
		categories.keywords as category_keywords,
		categories.channel as category_channel,
		categories.created_at as category_created,
		categories.updated_at as category_updated,
		categories.count as category_count';

		$this->column_order = array(null, 'categories.id','name');
		$this->column_search = array('categories.id','name');
		$this->order = array('categories.id' => 'desc');
	}
	public function getRows($postData){
		$this->_get_datatables_query($postData);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData){
		$this->_get_datatables_query($postData);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData){
		$this->db->from($this->table);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function delete($Id){
		$this->db->where('categories.id',$Id);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getCategories($channel,$status,$keywords){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('categories.channel',$channel);
		$this->db->where('categories.status',$status);
		if (isset($keywords)) {
			$this->db->like('categories.name',$keywords);
		}
		$this->db->limit(10);
		$parent = $this->db->get();
		$categories = $parent->result_array();
		return $categories;
	}

	public function getCategoryById($categoryId)
	{
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('categories.id',$categoryId);
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q;
		}else{
			return FALSE;
		}
	}

	public function update($categoryId,$data)
	{
		$this->db->where('categories.id', $categoryId);
		$this->db->update($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}


// get categories for select2 js
	public function get_categories($channel,$keywords=''){
		// $this->db->select($this->fields);
		$this->db->select('categories.id as id, categories.name as text');
		$this->db->from($this->table);
		$this->db->where('parent',0);
		$this->db->where('channel',$channel);
		if (isset($keywords)) {
			$this->db->like('name',$keywords);
		}
		$this->db->limit(10);
		$parent = $this->db->get();
		$categories = $parent->result_array();
		$i=0;
		foreach($categories as $p_cat){
			$categories[$i]['children'] = $this->sub_categories($p_cat['id']);
			$i++;
		}
		return $categories;
	}

	public function sub_categories($id){
		// $this->db->select($this->fields);
		$this->db->select('categories.id as id, categories.name as text');
		$this->db->from($this->table);
		$this->db->where('parent', $id);
		// $this->db->where('channel',$channel);
		$child = $this->db->get();
		$categories = $child->result_array();
		$i=0;
		foreach($categories as $p_cat){
			$categories[$i]['children'] = $this->sub_categories($p_cat['id']);
			$i++;
		}
		return $categories;       
	}

}