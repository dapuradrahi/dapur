<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UsersModel extends CI_Model{
	
	function __construct() {
		$this->table = 'users';
		$this->fields = 'users.id as id, ip_address,username,password,email,activation_selector,activation_code,forgotten_password_selector,forgotten_password_code,forgotten_password_time,remember_selector,remember_code,created_on,last_login,active,first_name,last_name,company,phone,picture, groups.id as group_id,groups.name as group_name';

		$this->column_order = array(null, 'first_name','last_name','username','email','phone');
		$this->column_search = array('first_name','last_name','username','email','phone');
		$this->order = array('id' => 'desc');
	}
	public function getRows($postData){
		$this->_get_datatables_query($postData);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData){
		$this->_get_datatables_query($postData);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData){

		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->join('users_groups','users.id = users_groups.user_id');
		$this->db->join('groups','users_groups.group_id = groups.id');

		if ($this->input->post('filter')) {
			$filter = $this->input->post('filter');
			$this->db->where('group_id',$filter);
		}

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function getUsers($type='',$keyword='',$limit=''){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->join('users_groups','users.id = users_groups.user_id');
		$this->db->join('groups','users_groups.group_id = groups.id');
		if (!$type == '') {
			$this->db->where('groups.id',$type);
		}
		if (isset($keyword)) {
			$this->db->like('first_name',$keyword);
			$this->db->like('username',$keyword);
			$this->db->like('email',$keyword);
		}
		if (isset($limit)) {
			$this->db->limit($limit);
		}

		return $this->db->get();
	}

}