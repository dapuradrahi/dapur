<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MenusModel extends CI_Model{
	
	function __construct() {

		$this->table = 'menu';
		$this->fields = '
		menu.id as menu_id,
		menu.name as menu_name,
		menu.parent_id as menu_parent_id,
		menu.position as menu_position,
		menu.url as menu_url,
		menu.class as menu_class,
		menu.order as menu_order,
		menu.status as menu_status,
		menu.channel as menu_channel,
		menu.created_at as menu_created_at,
		menu.updated_at as menu_updated_at';

		$this->column_order = array(null,'menu.name');
		$this->column_search = array('menu.name');
		$this->order = array('menu.id' => 'desc');
	}

	public function getMenus() {
		$this->db->select($this->fields);
		$this->db->order_by('order','ASC');
		$query = $this->db->get($this->table);
		$cat = array(
			'items' => array(),
			'parents' => array()
		);
		foreach ($query->result() as $cats) {
			$cat['items'][$cats->menu_id] = $cats;
			$cat['parents'][$cats->menu_parent_id][] = $cats->menu_id;
		}

		if ($cat) {
			$result = $this->build_menu(0, $cat);
			return $result;
		} else {
			return FALSE;
		}
	}
	public function build_menu($parent, $menu) {
		$html = "";
		if (isset($menu['parents'][$parent])) {
			$html .= "<ol class='dd-list todo-list'>\n";
			foreach ($menu['parents'][$parent] as $itemId) {
				if (!isset($menu['parents'][$itemId])) {
					$html .= "
					<li class='dd-item' data-id='".$menu['items'][$itemId]->menu_id."'>
					<div class='handle dd-handle' style='cursor: move;'>
					<i class='fas fa-ellipsis-v'></i>
					<i class='fas fa-ellipsis-v'></i>
					</div>
					<span class='text ".($menu['items'][$itemId]->menu_status == 0 ? 'text-red':'')."'>".$menu['items'][$itemId]->menu_name." #".$menu['items'][$itemId]->menu_order."</span>
					<div class='tools'>
					<a href='".backend.'settings/menus/edit/'.$menu['items'][$itemId]->menu_id."' <i class='fas fa-edit text-info'></i></a>
					<i class='fas fa-trash deleteMenu' data-id='".$menu['items'][$itemId]->menu_id."'></i>
					</div>
					</li>\n";
				}
				if (isset($menu['parents'][$itemId])) {
					$html .= "
					<li class='dd-item' data-id='".$menu['items'][$itemId]->menu_id."'>
					<div class='handle dd-handle' style='cursor: move;'>
					<i class='fas fa-ellipsis-v'></i>
					<i class='fas fa-ellipsis-v'></i>
					</div>
					<span class='text ".($menu['items'][$itemId]->menu_status == 0 ? 'text-red':'')."'>".$menu['items'][$itemId]->menu_name." #".$menu['items'][$itemId]->menu_order."</span>
					<div class='tools'>
					<a href='".backend.'settings/menus/edit/'.$menu['items'][$itemId]->menu_id."'<i class='fas fa-edit text-info'></i></a>
					<i class='fas fa-trash deleteMenu' data-id='".$menu['items'][$itemId]->menu_id."'></i>
					</div></li>\n";
					$html .= $this->build_menu($itemId, $menu);
					$html .= "</li>\n";
				}
			}
			$html .= "</ol>\n";
		}
		return $html;
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getMenuById($id){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('menu.id',$id);
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q;
		}else{
			return FALSE;
		}

	}

	public function update($menuId,$data)
	{
		$this->db->where('menu.id', $menuId);
		$this->db->update($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}


	public function delete($Id){
		$this->db->where('menu.id',$Id);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}


}