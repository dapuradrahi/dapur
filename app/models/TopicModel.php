<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TopicModel extends CI_Model{
	
	function __construct() {

		$this->table = 'topic';
		$this->fields = '
		topic.id as topic_id,
		topic.author_id as topic_author_id,
		topic.name as topic_name,
		topic.slug as topic_slug,
		topic.description as topic_description,
		topic.keywords as topic_keywords,
		topic.picture as topic_picture,
		topic.created_at as topic_created_at,
		topic.updated_at as topic_updated_at,
		topic.channel as topic_channel,
		topic.hash as topic_hash,
		topic.count as topic_count';

		$this->column_order = array(null,'topic.name');
		$this->column_search = array('topic.name');
		$this->order = array('topic.id' => 'desc');
	}
	public function getRows($postData,$channel){
		$this->_get_datatables_query($postData,$channel);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData,$channel){
		$this->_get_datatables_query($postData,$channel);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData,$channel){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('topic.channel',$channel);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function saveTopic($data)
	{
		$this->db->insert($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function update($topicId,$data){
		$this->db->where('topic.id', $topicId);
		$this->db->update($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getTopicById($id){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('topic.id',$id);
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q;
		}else{
			return FALSE;
		}

	}


	public function topicDelete($topicId){
		$this->db->where('topic.id',$topicId);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}


}