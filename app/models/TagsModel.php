<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TagsModel extends CI_Model{
	
	function __construct() {
		$this->table = 'tags';
		$this->fields = '
		tags.id as tag_id,
		tags.name as tag_name,
		tags.slug as tag_slug,
		tags.created_at as tag_created_at,
		tags.updated_at as tag_updated_at,
		tags.count as tag_count,
		';

		$this->column_order = array(null, 'tags.id','tags.name');
		$this->column_search = array('tags.name','tags_slug');
		$this->order = array('tags.id' => 'desc');
	}
	public function getRows($postData){
		$this->_get_datatables_query($postData);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData){
		$this->_get_datatables_query($postData);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData){
		$this->db->select($this->fields);
		$this->db->from($this->table);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function getNewsTags($newsId,$type){

		$this->db->select('
			news_tags.id as news_tag_id,
			news_tags.news_id as news_tag_news_id,
			news_tags.type as news_tag_type,
			news_tags.tag_id as news_tag_tag_id,
			news_tags.created_at as news_tag_created_at,
			tags.name as tag_name');
		$this->db->from('news_tags');
		$this->db->join('tags','news_tags.tag_id = tags.id');
		$this->db->where('news_id',$newsId);
		$this->db->where('type',$type);
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q;
		}else{
			return array();
		}
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getTagById($tagId){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('tags.id',$tagId);
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q;
		}else{
			return FALSE;
		}
	}

	public function update($tagId,$data)
	{
		$this->db->where('tags.id', $tagId);
		$this->db->update($this->table, $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function delete($id){
		$this->db->where('tags.id',$id);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

}