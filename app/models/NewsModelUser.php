<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NewsModelUser extends CI_Model{
	
	function __construct() {

		$this->table = 'news';
		$this->fields = '
		news.id as news_id,
		author_id,
		editor_id,
		publisher_id,
		news.channel as news_channel,
		news.category_id as news_category_id,
		news.topic_id as news_topic_id,
		news.prefik as news_prefik,
		news.title as news_title,
		news.slug as news_slug,
		news.excerpt as news_excerpt,
		news.content as news_content,
		news.status as news_status,
		news.created_at as news_created_at,
		news.updated_at as news_updated_at,
		news.published_at as news_published_at,
		news.count as news_count,
		news.related_news as news_related_news,
		news.hash,
		categories.id as category_id,
		categories.name as category_name,
		categories.slug as categorY_slug,
		categories.status as category_status,
		categories.picture as category_picture,
		categories.description as category_desc,
		categories.keywords as category_keywords,
		categories.created_at as category_created,
		categories.updated_at as category_updated,
		categories.count as category_count';

		$this->column_order = array(null, 'news.id','title','created_at');
		$this->column_search = array('news.id','title');
		$this->order = array('news.published_at' => 'desc');
	}
	public function getRows($postData,$status,$user_id,$filterDate,$channel){
		$this->_get_datatables_query($postData,$status,$user_id,$filterDate,$channel);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData,$status,$user_id,$filterDate,$channel){
		$this->_get_datatables_query($postData,$status,$user_id,$filterDate,$channel);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData,$status,$user_id,$filterDate,$channel){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->join('categories', 'categories.id=news.category_id','left');
		
		if ($status == 'published') {
			$this->db->where('news.status','published');
			$this->db->where('news.published_at <=',date('Y-m-d H:i:s'));
		}elseif ($status == 'scheduled') {
			$this->db->where('news.status','published');
			$this->db->where('news.published_at >',date('Y-m-d H:i:s'));
		}elseif ($status == 'draft') {
			$this->db->where('news.status',$status);
		}else{
			$this->db->where('news.status','deleted');
		}

		$this->db->where('author_id',$user_id);

		$this->db->where('news.published_at >=',date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $filterDate[0]))));
		$this->db->where('news.published_at <=',date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $filterDate[1]))));

		$this->db->where('news.channel',$channel);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

}