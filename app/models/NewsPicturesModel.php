<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NewsPicturesModel extends CI_Model{
	
	function __construct() {

		$this->table = 'news_pictures';
		$this->fields = '
		news_pictures.id as news_picture_id,
		news_pictures.author_id as news_pictures_author_id,
		news_pictures.editor_id as news_pictures_editor_id,
		news_pictures.publisher_id as news_pictures_publisher_id,
		news_pictures.title as news_pictures_title,
		news_pictures.slug as news_pictures_slug,
		news_pictures.excerpt as news_pictures_excerpt,
		news_pictures.pictures as news_pictures_pictures,
		news_pictures.content as news_pictures_content,
		news_pictures.status as news_pictures_status,
		news_pictures.related_news as news_pictures_related_news,
		news_pictures.created_at as news_pictures_created_at,
		news_pictures.updated_at as news_pictures_updated_at,
		news_pictures.published_at as news_pictures_published_at,
		news_pictures.channel as news_pictures_channel,
		news_pictures.hash as news_pictures_hash,
		news_pictures.count as news_pictures_count';

		$this->column_order = array(null, 'news_pictures.id','news_pictures.title','news_pictures.created_at');
		$this->column_search = array('news_pictures.id','news_pictures.title');
		$this->order = array('news_pictures.published_at' => 'desc');
	}
	public function getRows($postData,$status,$user_id,$group,$channel){
		$this->_get_datatables_query($postData,$status,$user_id,$group,$channel);
		if($postData['length'] != -1){
			$this->db->limit($postData['length'], $postData['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function countAll(){
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function countFiltered($postData,$status,$user_id,$group,$channel){
		$this->_get_datatables_query($postData,$status,$user_id,$group,$channel);
		$query = $this->db->get();
		return $query->num_rows();
	}

	private function _get_datatables_query($postData,$status,$user_id,$group,$channel){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		if ($group == 1 || $group == 2 || $group == 3) {

		}else{
			$this->db->where('news_pictures.author_id',$user_id);
		}
		
		if ($status == 'published') {
			$this->db->where('news_pictures.status','published');
			$this->db->where('news_pictures.published_at <=',date('Y-m-d H:i:s'));
		}elseif ($status == 'scheduled') {
			$this->db->where('news_pictures.status','published');
			$this->db->where('news_pictures.published_at >',date('Y-m-d H:i:s'));
		}elseif ($status == 'draft') {
			$this->db->where('news_pictures.status',$status);
		}elseif ($status == 'deleted') {
			$this->db->where('news_pictures.status','deleted');
		}else{
			// $this->db->where('news_pictures.status','deleted');
			$this->db->where_not_in('news_pictures.status', 'deleted');
		}
		$this->db->where('news_pictures.channel',$channel);

		$i = 0;
		foreach($this->column_search as $item){
			if($postData['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $postData['search']['value']);
				}else{
					$this->db->or_like($item, $postData['search']['value']);
				}
				if(count($this->column_search) - 1 == $i){
					$this->db->group_end();
				}
			}
			$i++;
		}

		if(isset($postData['order'])){
			$this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function saveNewsPicture($data,$tags)
	{
		$this->db->trans_start();
		$this->db->insert($this->table, $data);
		$newsId = $this->db->insert_id();

		for ($i=0; $i < count($tags); $i++) { 
			$dataTag = array(
				'news_id' => $newsId,
				'type' => 'photo',
				'tag_id' => $tags[$i],
			);
			$this->db->insert('news_tags',$dataTag);
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return FALSE;
		}else{
			return $newsId;
		}
	}

	public function update($data,$tags,$newsPictureId)
	{
		$this->db->trans_start();
		$this->db->where('news_pictures.id', $newsPictureId);
		$this->db->update($this->table, $data);

		$this->db->where('news_tags.news_id',$newsPictureId);
		$this->db->where('news_tags.type','photo');
		$this->db->delete('news_tags');
		for ($i=0; $i < count($tags); $i++) { 
			$dataTag = array(
				'news_id' => $newsPictureId,
				'type' => 'photo',
				'tag_id' => $tags[$i],
			);
			$this->db->insert('news_tags',$dataTag);
		}

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			return FALSE;
		}else{
			return TRUE;
		}
	}

	public function getNewsPictureById($id){
		$this->db->select($this->fields);
		$this->db->from($this->table);
		$this->db->where('news_pictures.id',$id);
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			return $q;
		}else{
			return FALSE;
		}

	}


	// only change status post to deleted
	public function newsPicuteDelete($newsId){
		$this->db->where('news_pictures.id', $newsId);
		$this->db->update($this->table, ['status'=>'deleted','updated_at'=> date('Y-m-d H:i:s')]);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function newsDeletePermanent($newsId){
		$this->db->where('news_pictures.id',$newsId);
		$this->db->delete($this->table);
		return ($this->db->affected_rows() != 1) ? false : true;
	}


}