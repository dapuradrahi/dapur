<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IframeContent extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation','pagination']);
		$this->load->model(['GroupsModel','NewsModel','CategoriesModel','PicturesModel','VideosModel']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init(){
		$this->output->set_template('backend/iframe/iframe_app');
		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');

		// datetime picker
		$this->load->css(assets.'backend/plugins/daterangepicker/daterangepicker.css');
		$this->load->js(assets.'backend/plugins/daterangepicker/daterangepicker.js');

		// bs-custom-file-input
		$this->load->js(assets.'backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');
	}

	public function imagesList()
	{

		if (!$this->access_menu->access('filemanager-filemanagerPhotos-read',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Not Authorized.!',
			);
			return $this->output->set_output(json_encode($output));;
		}
		$data['title'] = 'Photo Lists';
		$this->output->set_common_meta('Photo Lists');
		$data['module_access'] = $this->access_menu->module_access();

		$config['base_url'] = backend.'IframeContent/images';
		$config['total_rows'] = $this->PicturesModel->countAll();
		$config['per_page'] = 8;
		// $config["uri_segment"] = 5;
		$config['num_links'] = 2;

		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['use_page_numbers'] = TRUE;
		$config['reuse_query_string'] = TRUE;
		$config['query_string_segment'] = 'page';

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination pagination-sm m-0 float-right">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		
		$data['page'] = ($this->input->get('page')) ? $this->input->get('page') : 0;
		$data['channel'] = ($this->input->get('channel')) ? $this->input->get('channel') : '';
		$data['type'] = ($this->input->get('type')) ? $this->input->get('type') : '';
		$data['keywords'] = ($this->input->get('keywords')) ? $this->input->get('keywords') : '';
		$dateRange = ($this->input->get('dateRange')) ? explode('-', $this->input->get('dateRange')): '';
		if ($dateRange == '') {
			$date = [];
		}else{	
			$date = $dateRange;
		}
		
		$data['pictures'] = $this->PicturesModel->get_pictures($config["per_page"], $data['page'],$data['channel'],$data['type'],$data['keywords'],$date,'');
		$data['pagination'] = $this->pagination->create_links();

		$this->load->section('js', 'backend/filemanager/photos/js/index_js',$data);
		$this->load->view('backend/iframe/v-pictures',$data);
	}

	public function imageEdit()
	{
		
		$this->output->set_common_meta('Edit Photo');
		$data['title'] = 'Edit Photo';

		$pictureId = $this->uri->segment(5);
		$dataPicture = $this->PicturesModel->getPictureById($pictureId);
		if ($dataPicture === FALSE) {
			redirect(backend.'dashboard');
		}

		$data['dataPicture'] = $dataPicture->row();

		$this->load->view('backend/iframe/v-edit-picture', $data);

	}

	public function videosList()
	{
		if (!$this->access_menu->access('filemanager-filemanagerVideos-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$data['title'] = 'Video Lists';
		$this->output->set_common_meta('Video Lists');
		$data['module_access'] = $this->access_menu->module_access();

		$config['base_url'] = backend.'IframeContent/videos';
		$config['total_rows'] = $this->VideosModel->countAll();
		$config['per_page'] = 8;
		// $config["uri_segment"] = 5;
		$config['num_links'] = 2;

		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['use_page_numbers'] = TRUE;
		$config['reuse_query_string'] = TRUE;
		$config['query_string_segment'] = 'page';

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination pagination-sm m-0 float-right">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		
		$data['page'] = ($this->input->get('page')) ? $this->input->get('page') : 0;
		$data['channel'] = ($this->input->get('channel')) ? $this->input->get('channel') : '';
		$data['status'] = ($this->input->get('status') == '' ? '':$this->input->get('status'));
		$data['keywords'] = ($this->input->get('keywords')) ? $this->input->get('keywords') : '';		
		$dateRange = ($this->input->get('dateRange')) ? explode('-', $this->input->get('dateRange')): '';
		if ($dateRange == '') {
			$date = [];
		}else{	
			$date = $dateRange;
		}
		
		$data['videos'] = $this->VideosModel->get_videos($config["per_page"], $data['page'],$data['channel'],$data['status'],$data['keywords'],$date,'');
		$data['pagination'] = $this->pagination->create_links();

		$this->load->section('js', 'backend/filemanager/videos/js/index_js',$data);
		$this->load->view('backend/iframe/v-videos', $data);
	}

	public function newsList()
	{

		$data['title'] = 'News List';
		$this->output->set_common_meta('News Lists');

		$config['base_url'] = backend.'IframeContent/newsList';
		$config['total_rows'] = $this->NewsModel->countAll();
		$config['per_page'] = 8;
		// $config["uri_segment"] = 5;
		$config['num_links'] = 2;

		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['use_page_numbers'] = TRUE;
		$config['reuse_query_string'] = TRUE;
		$config['query_string_segment'] = 'page';

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination pagination-sm m-0 float-right">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		
		$data['page'] = ($this->input->get('page')) ? $this->input->get('page') : 0;
		$data['channel'] = ($this->input->get('channel')) ? $this->input->get('channel') : '';
		$data['category_id'] = ($this->input->get('category')) ? $this->input->get('category') : '';
		$data['keywords'] = ($this->input->get('keywords')) ? $this->input->get('keywords') : '';
		$dateRange = ($this->input->get('dateRange')) ? explode('-', $this->input->get('dateRange')): '';
		if ($dateRange == '') {
			$date = [];
		}else{	
			$date = $dateRange;
		}
		
		$data['newsList'] = $this->NewsModel->get_news_paginate($config["per_page"], $data['page'],$data['channel'],$data['category_id'],$data['keywords'],$date);
		$data['pagination'] = $this->pagination->create_links();

		$this->load->section('js', 'backend/filemanager/photos/js/index_js',$data);
		$this->load->view('backend/iframe/v-news',$data);
	}
	
}
