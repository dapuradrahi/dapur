<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filemanager extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('id'))->result_array()[0];
		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);
	}

	public function index()
	{
		
		if (!$this->access_menu->access('filemanager-view',$this->session->userdata('id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = '';
		$this->output->set_common_meta('Dashboard');
		// $this->load->section('js', 'backend/dashboard_js',$data);
		$this->load->view('backend/filemanager/v-index',$data);
	}

}
