<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->helper('tgl_indo');
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];
		
		$this->load->js(assets.'backend/plugins/chart-js/dist/Chart.min.js');
		$this->load->js(assets.'backend/js/utils.js');

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);
	}

	public function index()
	{
		
		$data['dd'] = implode(',', $this->apps->count_news_graph_day(1)['dd']);
		$data['dp'] = implode(',', $this->apps->count_news_graph_day(1)['dp']);
		$data['ds'] = implode(',', $this->apps->count_news_graph_day(1)['ds']);

		$data['dnp'] = implode(',', $this->apps->count_news_graph_day(1)['dnp']);
		$data['dnv'] = implode(',', $this->apps->count_news_graph_day(1)['dnv']);

		if (!$this->access_menu->access('dashboard-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = '';
		$this->output->set_common_meta('Dashboard');
		$this->load->section('js', 'backend/dashboard_js',$data);
		$this->load->view('backend/dashboard',$data);

	}

}
