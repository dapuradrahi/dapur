<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->helper('tgl_indo');
		$this->load->model(['ReportNews']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');

		// datetime picker
		$this->load->css(assets.'backend/plugins/daterangepicker/daterangepicker.css');
		$this->load->js(assets.'backend/plugins/daterangepicker/daterangepicker.js');

		$this->load->css(assets.'backend/plugins/bootstrap4-timepicker/css/tempusdominus-bootstrap-4.min.css');
		$this->load->js(assets.'backend/plugins/bootstrap4-timepicker/js/tempusdominus-bootstrap-4.min.js');
		
		$this->load->js(assets.'backend/plugins/chart-js/dist/Chart.min.js');
		$this->load->js(assets.'backend/js/utils.js');

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);
	}

	public function index()
	{
		// Harian
		$harian['dd'] = implode(',', $this->apps->count_news_graph_day(1)['dd']);
		$harian['ds'] = implode(',', $this->apps->count_news_graph_day(1)['ds']);
		$harian['dp'] = implode(',', $this->apps->count_news_graph_day(1)['dp']);

		$harian['dnp'] = implode(',', $this->apps->count_news_graph_day(1)['dnp']);
		$harian['dnv'] = implode(',', $this->apps->count_news_graph_day(1)['dnv']);

		// Mingguan

		$mingguan['wd'] = implode(',',$this->apps->count_news_graph_week(1)['wd']);
		$mingguan['wp'] = implode(',',$this->apps->count_news_graph_week(1)['wp']);
		$mingguan['ws'] = implode(',',$this->apps->count_news_graph_week(1)['ws']);

		$mingguan['wnp'] = implode(',',$this->apps->count_news_graph_week(1)['wnp']);
		$mingguan['wnv'] = implode(',',$this->apps->count_news_graph_week(1)['wnv']);

		// Bulanan
		$bulanan['md'] = implode(',',$this->apps->count_news_graph_month(1)['md']);
		$bulanan['mp'] = implode(',',$this->apps->count_news_graph_month(1)['mp']);
		$bulanan['ms'] = implode(',',$this->apps->count_news_graph_month(1)['ms']);

		$bulanan['mnp'] = implode(',',$this->apps->count_news_graph_month(1)['mnp']);
		$bulanan['mnv'] = implode(',',$this->apps->count_news_graph_month(1)['mnv']);

		$data['harian'] = $harian;
		$data['mingguan'] = $mingguan;
		$data['bulanan'] = $bulanan;


		if (!$this->access_menu->access('report-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Report';
		$this->output->set_common_meta('Report');
		$this->load->section('js', 'backend/report/js/index_js.php',$data);
		$this->load->view('backend/report/v-index.php',$data);

	}

	public function newsList(){
		$this->output->unset_template();

		if (!$this->access_menu->access('news-published-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" =>'',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->input->post('search')) {
			$output = array(
				"draw" =>'',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$status = 'published';
		$user_id = $_POST['uid'];
		$filterDate = explode('-', $_POST['filterDate']);
		$channel = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		$memData = $this->ReportNews->getRows($_POST,$status,$user_id,$filterDate,$channel);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = '<a href="'.backend.'news/create/edit/'.$v->news_id.'">'.$v->news_title;
			$row[] = '<span class="badge badge-success">'.$v->category_name.'</span>';
			$row[] = $this->ion_auth->user($v->author_id)->row()->first_name.' '.$this->ion_auth->user($v->author_id)->row()->last_name;
			$row[] = ($v->editor_id == NULL ? '<span class="text-red">Unknown</span>':$this->ion_auth->user($v->editor_id)->row()->first_name.' '.$this->ion_auth->user($v->editor_id)->row()->last_name);
			$row[] = '<span class="badge badge-info">'.date('d-M-Y H:i:s',strtotime($v->news_published_at)).'</span>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->ReportNews->countAll(),
			"recordsFiltered" => $this->ReportNews->countFiltered($_POST,$status,$user_id,$filterDate,$channel),
			"data" => $data,
		);

		return $this->output->set_output(json_encode($output));
	}

}
