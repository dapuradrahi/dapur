<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photos extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation','pagination']);
		$this->load->model(['GroupsModel','CategoriesModel','PicturesModel']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');

		// datetime picker
		$this->load->css(assets.'backend/plugins/daterangepicker/daterangepicker.css');
		$this->load->js(assets.'backend/plugins/daterangepicker/daterangepicker.js');

		// bs-custom-file-input
		$this->load->js(assets.'backend/plugins/bs-custom-file-input/bs-custom-file-input.min.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('filemanager-filemanagerPhotos-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Photo Lists';
		$this->output->set_common_meta('Photo Lists');
		$data['module_access'] = $this->access_menu->module_access();

		$config['base_url'] = site_url('root-cms/filemanager/filemanagerPhotos/view');
		$config['total_rows'] = $this->PicturesModel->countAll();
		$config['per_page'] = 12;
		// $config["uri_segment"] = 5;
		$config['num_links'] = 2;

		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['use_page_numbers'] = TRUE;
		$config['reuse_query_string'] = TRUE;
		$config['query_string_segment'] = 'page';

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination pagination-sm m-0 float-right">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		
		$data['page'] = ($this->input->get('page')) ? $this->input->get('page') : 0;
		$data['channel'] = ($this->input->get('channel')) ? $this->input->get('channel') : '';
		$data['type'] = ($this->input->get('type')) ? $this->input->get('type') : '';
		$data['keywords'] = ($this->input->get('keywords')) ? $this->input->get('keywords') : '';
		$dateRange = ($this->input->get('dateRange')) ? explode('-', $this->input->get('dateRange')): '';
		if ($dateRange == '') {
			$date = [];
		}else{	
			$date = $dateRange;
		}
		
		$data['pictures'] = $this->PicturesModel->get_pictures($config["per_page"], $data['page'],$data['channel'],$data['type'],$data['keywords'],$date,'');
		$data['pagination'] = $this->pagination->create_links();

		$this->load->section('js', 'backend/filemanager/photos/js/index_js',$data);
		$this->load->view('backend/filemanager/photos/v-index', $data);
	}

	public function save()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('caption','caption','trim|required');

		if (!$this->access_menu->access('filemanager-filemanagerPhotos-create',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Not Authorized.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Caption empty',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}
		if (empty($_FILES['picture']['name'])) {
			$output = array(
				'status' => FALSE,
				'message' => 'Image empty',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}else{
			$config['upload_path']      = storagePath.'/picture/origin/';
			$config['allowed_types']    = 'jpg|png|jpeg';
			$config['max_size']         = 3000;
			$config['file_name']		= time().'_'.$this->session->userdata('user_id');
			$config['max_width']        = 1920;
			$config['max_height']       = 1080;
			$config['overwrite']		= TRUE;
			$config['mod_mime_fix']		= TRUE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('picture')){
				$error = $this->upload->display_errors();
				$output = array(
					'status' => FALSE,
					'message' => $error,
					'data' => '',
					'token' => $this->security->get_csrf_hash(),
				);
				return $this->output->set_output(json_encode($output));
			}else{
				$dataUpload = $this->upload->data();
				$this->apps->create_thumbs($dataUpload['file_name']);
			}

			$data = array(
				'user_id' => $this->session->userdata('user_id'),
				'file_name' => $dataUpload['file_name'],
				'title' => $this->input->post('title'),
				'caption' => $this->input->post('caption'),
				'source' => $this->input->post('source'),
				'type' => $this->input->post('type'),
				'status' => $this->input->post('status'),
				'channel' => ($this->input->post('channel') ? $this->input->post('channel'):1),
				'created_at' => date('Y-m-d H:i:s'),
			);
			if ($this->PicturesModel->insertPicture($data)) {
				$output = array(
					'status' => TRUE,
					'message' => 'Image uploaded.!',
					'data' => '',
					'token' => $this->security->get_csrf_hash(),
				);
				return $this->output->set_output(json_encode($output));
			}else{
				$output = array(
					'status' => FALSE,
					'message' => 'Error, Try again.!',
					'data' => '',
					'token' => $this->security->get_csrf_hash(),
				);
				return $this->output->set_output(json_encode($output));
			}
		}
	}

	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('pictureId','pictureId','required');

		if (!$this->access_menu->access('filemanager-filemanagerPhotos-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Not Authorized.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Picture not found.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$pictureId = $this->input->post('pictureId');

		$dataPicture = $this->PicturesModel->get_pictures('','','','','','',$pictureId)->result_array();
		unlink(storagePath.'/picture/origin/'.$dataPicture[0]['file_name']);
		unlink(storagePath.'/picture/low/'.$dataPicture[0]['file_name']);
		unlink(storagePath.'/picture/medium/'.$dataPicture[0]['file_name']);
		unlink(storagePath.'/picture/high/'.$dataPicture[0]['file_name']);

		if (!$this->PicturesModel->delete($pictureId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error try again.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'Image deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}

	public function edit(){
		if (!$this->access_menu->access('filemanager-filemanagerPhotos-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Edit Photo');
		$data['title'] = 'Edit Photo';

		$pictureId = $this->uri->segment(5);
		$dataPicture = $this->PicturesModel->getPictureById($pictureId);
		if ($dataPicture === FALSE) {
			redirect(backend.'dashboard');
		}

		$data['dataPicture'] = $dataPicture->row();

		$this->load->view('backend/filemanager/photos/v-edit', $data);
	}

	public function update()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('pictureId','pictureId','trim|required');
		$this->form_validation->set_rules('channel','channel','trim|required');
		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('caption','caption','trim|required');

		$pictureId = $this->input->post('pictureId');

		if (!$this->access_menu->access('filemanager-filemanagerPhotos-update',$this->session->userdata('user_id'))) {
			$this->session->set_flashdata('error','No access');
			redirect(backend.'dashboard');
		}

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form.!');
			redirect(backend.'filemanager/filemanagerPhotos/edit/'.$pictureId);
		}
		if (empty($_FILES['picture']['name'])) {
			$dataUpload['file_name'] = $this->input->post('file_name');
		}else{
			$config['upload_path']      = storagePath.'/picture/origin/';
			$config['allowed_types']    = 'jpg|png|jpeg';
			$config['max_size']         = 3000;
			$config['file_name']		= time().'_'.$this->session->userdata('user_id');
			$config['max_width']        = 1920;
			$config['max_height']       = 1080;
			$config['overwrite']		= TRUE;
			$config['mod_mime_fix']		= TRUE;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('picture')){
				$error = $this->upload->display_errors();
				$this->session->set_flashdata('success',$error);
				redirect(backend.'filemanager/filemanagerPhotos/edit/'.$pictureId);
			}else{
				unlink(storagePath.'/picture/low/'.$this->input->post('file_name'));
				unlink(storagePath.'/picture/high/'.$this->input->post('file_name'));
				unlink(storagePath.'/picture/medium/'.$this->input->post('file_name'));
				unlink(storagePath.'/picture/origin/'.$this->input->post('file_name'));

				$dataUpload = $this->upload->data();
				$this->apps->create_thumbs($dataUpload['file_name']);
			}
		}

		$data = array(
			'user_id' => $this->input->post('author'),
			'file_name' => $dataUpload['file_name'],
			'title' => $this->input->post('title'),
			'caption' => $this->input->post('caption'),
			'source' => $this->input->post('source'),
			'type' => $this->input->post('type'),
			'status' => $this->input->post('status'),
			'channel' => ($this->input->post('channel') ? $this->input->post('channel'):1),
			'updated_at' => date('Y-m-d H:i:s'),
		);
		// print_r($data);
		// die();
		if ($this->PicturesModel->update($pictureId,$data)) {
			$this->session->set_flashdata('success','Picture Updated.!');
			// redirect(backend.'filemanager/filemanagerPhotos/edit/'.$pictureId);
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			$this->session->set_flashdata('error','Error Try again.!');
			// redirect(backend.'filemanager/filemanagerPhotos/edit/'.$pictureId);
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
