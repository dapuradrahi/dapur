<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation','pagination']);
		$this->load->model(['GroupsModel','CategoriesModel','VideosModel']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');

		// datetime picker
		$this->load->css(assets.'backend/plugins/daterangepicker/daterangepicker.css');
		$this->load->js(assets.'backend/plugins/daterangepicker/daterangepicker.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('filemanager-filemanagerVideos-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$data['title'] = 'Video Lists';
		$this->output->set_common_meta('Video Lists');
		$data['module_access'] = $this->access_menu->module_access();

		$config['base_url'] = site_url(backend.'filemanager/filemanagerVideos/view');
		$config['total_rows'] = $this->VideosModel->countAll();
		$config['per_page'] = 12;
		// $config["uri_segment"] = 5;
		$config['num_links'] = 4;

		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['use_page_numbers'] = TRUE;
		$config['reuse_query_string'] = TRUE;
		$config['query_string_segment'] = 'page';

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination pagination-sm m-0 float-right">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

		$this->pagination->initialize($config);
		$data['total_rows'] = $config['total_rows'];
		
		$data['page'] = ($this->input->get('page')) ? $this->input->get('page') : 0;
		$data['channel'] = ($this->input->get('channel')) ? $this->input->get('channel') : '';
		$data['status'] = ($this->input->get('status') == '' ? '':$this->input->get('status'));
		$data['keywords'] = ($this->input->get('keywords')) ? $this->input->get('keywords') : '';		
		$dateRange = ($this->input->get('dateRange')) ? explode('-', $this->input->get('dateRange')): '';
		if ($dateRange == '') {
			$date = [];
		}else{	
			$date = $dateRange;
		}
		
		$data['videos'] = $this->VideosModel->get_videos($config["per_page"], $data['page'],$data['channel'],$data['status'],$data['keywords'],$date,'');
		$data['pagination'] = $this->pagination->create_links();

		$this->load->section('js', 'backend/filemanager/videos/js/index_js',$data);
		$this->load->view('backend/filemanager/videos/v-index', $data);
		// print_r($data['pictures']);
	}

	public function save()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('link','link','trim|required');

		if (!$this->access_menu->access('filemanager-filemanagerVideos-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Complete all Form.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}
		$data = array(
			'user_id' => $this->session->userdata('user_id'),
			'title' => $this->input->post('title'),
			'link' => $this->input->post('link'),
			'description' => $this->input->post('description'),
			'source' => $this->input->post('source'),
			'status' => $this->input->post('status'),
			'channel' => ($this->input->post('channel') ? $this->input->post('channel'):1),
			'created_at' => date('Y-m-d H:i:s'),
		);
		if ($this->VideosModel->insertVideo($data)) {
			$output = array(
				'status' => TRUE,
				'message' => 'Video uploaded.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => FALSE,
				'message' => 'Error, Try again.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}
	}

	public function edit(){
		if (!$this->access_menu->access('filemanager-filemanagerVideos-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Edit Video');
		$data['title'] = 'Edit Video';

		$videoId = $this->uri->segment(5);
		$dataVideo = $this->VideosModel->getVideoById($videoId);
		if ($dataVideo === FALSE) {
			redirect(backend.'dashboard');
		}

		$data['dataVideo'] = $dataVideo->row();

		$this->load->section('js', 'backend/filemanager/videos/js/edit_js',$data);
		$this->load->view('backend/filemanager/videos/v-edit', $data);
	}

	public function update()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('link','link','trim|required');

		$videoId = $this->input->post('videoId');

		if (!$this->access_menu->access('filemanager-filemanagerVideos-update',$this->session->userdata('user_id'))) {
			$this->session->set_flashdata('error','No have access.!');
			redirect(backend.'dashboard');
		}

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form.!');
			redirect(backend.'filemanager/filemanagerVideos/edit/'.$videoId);
		}

		$data = array(
			'user_id' => $this->session->userdata('user_id'),
			'title' => $this->input->post('title'),
			'link' => $this->input->post('link'),
			'description' => $this->input->post('description'),
			'source' => $this->input->post('source'),
			'status' => $this->input->post('status'),
			'channel' => ($this->input->post('channel') ? $this->input->post('channel'):1),
		);
		if ($this->VideosModel->update($videoId,$data)) {
			$this->session->set_flashdata('success','Video Updated.!');
			redirect(backend.'filemanager/filemanagerVideos/edit/'.$videoId);
		}else{
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'filemanager/filemanagerVideos/edit/'.$videoId);
		}
	}

	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('videoId','videoId','required');

		if (!$this->access_menu->access('filemanager-filemanagerVideos-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Not Authorized.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Video not found.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$videoId = $this->input->post('videoId');

		$dataVideo = $this->VideosModel->get_videos('','','','','',$videoId)->result_array();

		if (!$this->VideosModel->delete($videoId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error try again.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'Video deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}
}
