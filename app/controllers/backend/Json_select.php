<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Json_select extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['GroupsModel','NewsModel','CategoriesModel','UsersModel','MenusModel']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init(){
		if (!$this->ion_auth->logged_in()){
			$output = array(
				'status' => FALSE,
				'message' => 'Error',
			);
			return $this->output->set_output(json_encode($output));
		}
	}

	public function rubrik_list()
	{
		$keyword = ($this->input->get('search')) ? $this->input->get('search'):'';
		$status = ($this->input->get('status')) ? $this->input->get('status'):1;
		$categories = $this->CategoriesModel->getCategories(1,$status,$keyword);
		foreach($categories as $key => $value) {
			$cat[] = ['id'=>$value['category_id'],'text'=>$value['category_name']];
		}

		$output['results'] = $cat;
		return $this->output->set_output(json_encode($output));
	}

	public function tags(){
		$this->db->select('*');
		$this->db->like('name',$this->input->get('search'));
		$this->db->limit(10);
		$tags = $this->db->get('tags');
		if (!$tags->num_rows() > 0) {
			$output['results'] = array();
			return $this->output->set_output(json_encode($output));
		}

		foreach ($tags->result() as $key => $value) {
			$data[] = ['id'=> $value->id,'text'=>$value->name];
		}
		$output['results'] = $data;
		return $this->output->set_output(json_encode($output));
	}

	public function author(){
		// for type author,editor or publisher
		$type = $this->input->get('type');

		if ($type == 'editor') {
			$type = 3;
		}else{
			$type = '';
		}

		$keyword = ($this->input->get('search')) ? $this->input->get('search'):'';

		$users = $this->UsersModel->getUsers($type,$keyword,10);
		if (!$users->num_rows() > 0) {
			$output['results'] = array();
			return $this->output->set_output(json_encode($output));
		}

		foreach ($users->result() as $key => $value) {
			$data[] = ['id'=> $value->id,'text'=>$value->first_name.' '.$value->last_name];
		}
		$output['results'] = $data;
		return $this->output->set_output(json_encode($output));
	}

	public function topic(){
		$this->db->select('*');
		$this->db->like('name',$this->input->get('search'));
		$this->db->limit(10);
		$tags = $this->db->get('topic');
		if (!$tags->num_rows() > 0) {
			$output['results'] = array();
			return $this->output->set_output(json_encode($output));
		}

		foreach ($tags->result() as $key => $value) {
			$data[] = ['id'=> $value->id,'text'=>$value->name];
		}
		$output['results'] = $data;
		return $this->output->set_output(json_encode($output));
	}

	public function related(){
		
		$this->db->select('*');
		$this->db->like('title',$this->input->get('search'));
		$this->db->where('status','published');
		$this->db->limit(10);
		$tags = $this->db->get('news');
		if (!$tags->num_rows() > 0) {
			$output['results'] = array();
			return $this->output->set_output(json_encode($output));
		}

		foreach ($tags->result() as $key => $value) {
			$data[] = ['id'=>$value->id,'text'=>$value->title];
		}
		$output['results'] = $data;
		return $this->output->set_output(json_encode($output));
	}

	public function menu_list(){
		$this->db->select('*');
		$this->db->like('name',$this->input->get('search'));
		$this->db->limit(10);
		$menu = $this->db->get('menu');
		if (!$menu->num_rows() > 0) {
			$output['results'] = array();
			return $this->output->set_output(json_encode($output));
		}

		foreach ($menu->result() as $key => $value) {
			$data[] = ['id'=> $value->id,'text'=>$value->name];
		}
		$output['results'] = $data;
		return $this->output->set_output(json_encode($output));
	}
	
}
