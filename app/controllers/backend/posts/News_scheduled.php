<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_scheduled extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['GroupsModel','NewsModel','CategoriesModel']);
		$this->load->helper(['date']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');

		// datetime picker
		$this->load->css(assets.'backend/plugins/bootstrap4-timepicker/css/tempusdominus-bootstrap-4.min.css');
		$this->load->js(assets.'backend/plugins/bootstrap4-timepicker/js/tempusdominus-bootstrap-4.min.js');

		$this->load->js(assets.'plugins/tinymce/tinymce.min.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('news-scheduled-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Scheduled News';
		$this->output->set_common_meta('Scheduled News');

		$this->load->section('js', 'backend/posts/news/js/scheduled_js',$data);
		$this->load->view('backend/posts/news/v-scheduled', $data);
	}

	public function lists()
	{
		$this->output->unset_template();

		if (!$this->access_menu->access('news-scheduled-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->input->post('search')) {
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$status = 'scheduled';
		$user_id = $this->session->userdata('user_id');
		$group = $this->ion_auth->get_users_groups($user_id)->row()->id;
		$channel = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		$memData = $this->NewsModel->getRows($_POST,$status,$user_id,$group,$channel);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = '<a href="'.backend.'news/create/edit/'.$v->news_id.'">'.$v->news_title;
			$row[] = '<span class="badge badge-success">'.$v->category_name.'</span>';
			$row[] = $this->ion_auth->user($v->author_id)->row()->first_name;
			$row[] = ($v->editor_id == NULL ? '<span class="text-red">Unknown</span>':$this->ion_auth->user($v->author_id)->row()->first_name);
			$row[] = '<span class="badge badge-info">'.date('d-M-Y H:i:s',strtotime($v->news_published_at)).'</span>';
			$row[] = '<div class="btn-group">
			'.($this->access_menu->access('news-scheduled-update',$this->session->userdata('user_id')) ? '<a href="'.backend.'news/create/edit/'.$v->news_id.'"><button type="button" class="btn btn-flat btn-xs btn-success" title="Edit"><i class="fas fa-edit"></i></button></a></div>':'<button type="button" class="btn btn-flat btn-xs btn-success disabled" title="Edit"><i class="fas fa-edit"></i></button></div>').'
			'.($this->access_menu->access('news-scheduled-delete',$this->session->userdata('user_id')) ? '<button type="button" data-id="'.$v->news_id.'" class="btn btn-flat btn-xs btn-danger newsDelete" title="Delete"><i class="fas fa-trash"></i></button></div>':'<button type="button" class="btn btn-flat btn-xs btn-danger disabled" title="Delete"><i class="fas fa-trash"></i></button></div>').'';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->NewsModel->countAll(),
			"recordsFiltered" => $this->NewsModel->countFiltered($_POST,$status,$user_id,$group,$channel),
			"data" => $data,
		);

		return $this->output->set_output(json_encode($output));
	}
	
}
