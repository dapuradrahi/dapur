<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['GroupsModel','NewsModel','CategoriesModel','TagsModel','TopicModel']);
		$this->load->helper(['date','string','security']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');

		// datetime picker
		$this->load->css(assets.'backend/plugins/bootstrap4-timepicker/css/tempusdominus-bootstrap-4.min.css');
		$this->load->js(assets.'backend/plugins/bootstrap4-timepicker/js/tempusdominus-bootstrap-4.min.js');

		$this->load->js(assets.'plugins/tinymce/tinymce.min.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}
	
	public function add()
	{
		if (!$this->access_menu->access('news-create-create',$this->session->userdata('id'))) {
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Add News');
		$data['title'] = 'Add News';

		$this->load->section('js', 'backend/posts/news/js/add_js',$data);
		$this->load->view('backend/posts/news/v-add', $data);
	}

	public function save()
	{
		$this->output->unset_template();
		if (!$this->access_menu->access('news-create-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('category_id','category_id','trim|required');
		$this->form_validation->set_rules('author','author','trim|required');
		$this->form_validation->set_rules('category_id','category_id','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form.!');
			redirect(backend.'news/create/add');
		}

		$tags = $this->input->post('tags');

		$status = $this->input->post('status');
		if ($status == 'publish') {
			$status = 'published';
		}else{
			$status = 'draft';
		}

		$dataPost = array(
			'title'			=> $this->input->post('title'),
			'slug'			=> url_title($this->input->post('title'), 'dash', true),
			'excerpt'		=> $this->input->post('excerpt'),
			'content'		=> $this->input->post('content'),
			'related_news'	=> ($this->input->post('related')) ? serialize($this->input->post('related')):NULL,
			'prefik'		=> $this->input->post('prefik'),
			'category_id'	=> $this->input->post('category_id'),
			'editor_id'		=> ($this->input->post('editor')) ? $this->input->post('editor'):NULL,
			'author_id'		=> $this->input->post('author'),
			'publisher_id'	=> ($this->input->post('publisher') ? $this->input->post('publisher'):1),
			'topic_id'		=> ($this->input->post('topic')) ? $this->input->post('topic'):0,
			'published_at'	=> ($this->input->post('schedule')) ? date('Y-m-d H:i:s',strtotime($this->input->post('schedule'))):date('Y-m-d H:i:s'),
			'channel'		=> ($this->input->post('channel')) ? $this->input->post('channel'):1,
			'status'		=> $status,
			'hash'			=> random_string('alnum', 12),
		);
		# preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $dataPost['content'], $image);
		// echo $image[1];

		$saveNews = $this->NewsModel->saveNews($dataPost,$tags);
		if ($saveNews === FALSE) {
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'news/create/add');
		}else{
			$this->session->set_flashdata('success','News has been '.$this->input->post('status'));
			redirect(backend.'news/create/edit/'.$saveNews);
		}

	}

	public function edit()
	{
		if (!$this->access_menu->access('news-create-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Edit News';
		$this->output->set_common_meta('Edit News');

		$newsId = $this->uri->segment(5);
		$dataNews = $this->NewsModel->getNewsById($newsId);
		if ($dataNews === FALSE) {
			redirect(backend.'dashboard');
		}
		$data['news'] = $dataNews->row();
		$data['tags'] = $this->TagsModel->getNewsTags($newsId,'news');

		$topic = $this->TopicModel->getTopicById($data['news']->news_topic_id);

		$data['topic'] = ($topic === FALSE ? array():$topic->result_array());
		$data['related'] = $this->NewsModel->related_news(unserialize($data['news']->news_related_news))->result_array();

		// $this->load->section('js', 'backend/posts/news/js/edit_js',$data);
		$this->load->section('js', 'backend/posts/news/js/add_js',$data);
		$this->load->view('backend/posts/news/v-edit', $data);
	}

	public function update()
	{
		$this->output->unset_template();
		if (!$this->access_menu->access('news-create-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('category_id','category_id','trim|required');
		$this->form_validation->set_rules('author','author','trim|required');
		$this->form_validation->set_rules('category_id','category_id','trim|required');

		$newsId = $this->input->post('newsId');

		if (!$this->access_menu->access('news-create-update',$this->session->userdata('id'))) {
			$this->session->set_flashdata('error','Not Authorized.!');
			redirect(backend.'dashboard');
		}

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Error Try again.!');
			redirect(backend.'news/create/edit/'.$newsId);
		}

		$tags = $this->input->post('tags');

		$status = $this->input->post('status');
		if ($status == 'Publish') {
			$status = 'published';
		}else{
			$status = 'draft';
		}

		$dataPost = array(
			'title'			=> $this->input->post('title'),
			'slug'			=> url_title($this->input->post('title'), 'dash', true),
			'excerpt'		=> $this->input->post('excerpt'),
			'content'		=> $this->input->post('content'),
			'related_news'	=> ($this->input->post('related')) ? serialize($this->input->post('related')):NULL,
			'prefik'		=> $this->input->post('prefik'),
			'category_id'	=> $this->input->post('category_id'),
			'editor_id'		=> ($this->input->post('editor')) ? $this->input->post('editor'):NULL,
			'author_id'		=> $this->input->post('author'),
			'publisher_id'	=> ($this->input->post('publisher') ? $this->input->post('publisher'):1),
			'topic_id'		=> ($this->input->post('topic')) ? $this->input->post('topic'):0,
			'published_at'	=> ($this->input->post('schedule')) ? date('Y-m-d H:i:s',strtotime($this->input->post('schedule'))):date('Y-m-d H:i:s'),
			'channel'		=> ($this->input->post('channel')) ? $this->input->post('channel'):1,
			'status'		=> $status,
			'hash'			=> $this->input->post('hash'),
		);

		$updateNews = $this->NewsModel->update($dataPost,$tags,$newsId);
		if ($updateNews === FALSE) {
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'news/create/add');
		}else{
			$this->session->set_flashdata('success','News has been Updated.!');
			redirect(backend.'news/create/edit/'.$newsId);
		}
	}
	
	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('newsId','newsId','required');
		if (!$this->access_menu->access('news-create-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$newsId = $this->input->post('newsId');
		if (!$this->NewsModel->newsDelete($newsId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'News deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}
	
}
