<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['TagsModel','MenusModel']);
		$this->load->helper(['date']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');
		
		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('settings-menus-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Menu Frontend';
		$this->output->set_common_meta('Menu Frontend');
		$data['menus'] = $this->MenusModel->getMenus();

		// nestable
		$this->load->css(assets.'backend/plugins/nestable/jquery.nestable.css');
		$this->load->js(assets.'backend/plugins/nestable/jquery.nestable.js');

		$this->load->section('js', 'backend/settings/menus/js/index_js',$data);
		$this->load->view('backend/settings/menus/v-index', $data);
	}

	public function save(){
		$this->output->unset_template();
		$this->form_validation->set_rules('name','name','trim|required');
		$this->form_validation->set_rules('url','url','trim|required');
		$this->form_validation->set_rules('position','position','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','complete all form.!');
			redirect(backend.'settings/menus/view');
		}

		$data['name'] = $this->input->post('name');
		$data['parent_id'] = ($this->input->post('parent')) ? $this->input->post('parent'):0;
		$data['position'] = $this->input->post('position');
		$data['url'] = ($this->input->post('url')) ? $this->input->post('url'):'#';
		$data['class'] = ($this->input->post('class')) ? $this->input->post('class'):NULL;
		$data['order'] = ($this->input->post('order')) ? $this->input->post('order'):0;
		$data['status'] = ($this->input->post('status')) ? $this->input->post('status'):1;
		$data['channel'] = ($this->input->post('channel')) ? $this->input->post('channel'):1;
		$data['created_at'] = date('Y-m-d H:i:s');

		if ($this->MenusModel->save($data)) {
			$this->session->set_flashdata('success','Menu Added.!');
			redirect(backend.'settings/menus/view');
		}else{
			$this->session->set_flashdata('error','complete all form.!');
			redirect(backend.'settings/menus/view');
		}
	}

	public function edit(){
		if (!$this->access_menu->access('settings-menus-update',$this->session->userdata('user_id'))) {
			$this->session->set_flashdata('error','No Access .!');
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Edit Menu');
		$data['title'] = 'Edit Menu';

		$menuId = $this->uri->segment(5);
		$dataMenu = $this->MenusModel->getMenuById($menuId);
		if ($dataMenu === FALSE) {
			redirect(backend.'settings/menus/edit/'.$menuId);
		}

		$data['dataMenu'] = $dataMenu->row();
		$data['dataParent'] = $this->MenusModel->getMenuById($data['dataMenu']->menu_parent_id);
		$this->load->section('js', 'backend/settings/menus/js/edit_js',$data);
		$this->load->view('backend/settings/menus/v-edit', $data);
	}

	public function update(){
		$this->output->unset_template();
		$this->form_validation->set_rules('name','name','trim|required');
		$this->form_validation->set_rules('url','url','trim|required');
		$this->form_validation->set_rules('position','position','trim|required');
		$this->form_validation->set_rules('menuId','menuId','trim|required');
		$this->form_validation->set_rules('channel','channel','trim|required');

		$menuId = $this->input->post('menuId');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','complete all form.!');
			redirect(backend.'settings/menus/edit/'.$menuId);
		}

		$data['name'] = $this->input->post('name');
		$data['parent_id'] = ($this->input->post('parent')) ? $this->input->post('parent'):0;
		$data['position'] = $this->input->post('position');
		$data['url'] = ($this->input->post('url')) ? $this->input->post('url'):'#';
		$data['class'] = ($this->input->post('class')) ? $this->input->post('class'):NULL;
		$data['order'] = ($this->input->post('order')) ? $this->input->post('order'):0;
		$data['status'] = ($this->input->post('status') == 0) ? 0:1;
		$data['channel'] = ($this->input->post('channel')) ? $this->input->post('channel'):1;
		$data['updated_at'] = date('Y-m-d H:i:s');

		if ($this->MenusModel->update($menuId,$data)) {
			$this->session->set_flashdata('success','Menu Updated.!');
			redirect(backend.'settings/menus/edit/'.$menuId);
		}else{
			$this->session->set_flashdata('error','Error Try again.!');
			redirect(backend.'settings/menus/edit/'.$menuId);
		}
	}
	
	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('menuId','menuId','required');
		if (!$this->access_menu->access('settings-menus-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$menuId = $this->input->post('menuId');
		if (!$this->MenusModel->delete($menuId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'News deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}
}
