<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rubrik extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['GroupsModel','NewsModel','CategoriesModel']);
		$this->load->helper(['date']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');

		// nestable
		$this->load->css(assets.'backend/plugins/nestable/jquery.nestable.css');
		$this->load->js(assets.'backend/plugins/nestable/jquery.nestable.js');

		// datetime picker
		$this->load->css(assets.'backend/plugins/bootstrap4-timepicker/css/tempusdominus-bootstrap-4.min.css');
		$this->load->js(assets.'backend/plugins/bootstrap4-timepicker/js/tempusdominus-bootstrap-4.min.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('settings-rubrik-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Rubrik Lists';
		$this->output->set_common_meta('Rubrik Lists');
		$keyword = ($this->input->get('search')) ? $this->input->get('search'):'';
		$status = ($this->input->get('status')) ? $this->input->get('status'):1;

		$data['rubrik'] = $this->CategoriesModel->getCategories(1,$status,$keyword);

		$this->load->section('js', 'backend/settings/rubrik/js/index_js',$data);
		$this->load->view('backend/settings/rubrik/v-index', $data);
	}

	public function save()
	{
		if (!$this->access_menu->access('settings-rubrik-create',$this->session->userdata('user_id'))) {
			$this->session->set_flashdata('error','Not Authorized.!');
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('name','name','trim|required');
		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form.!');
			redirect(backend.'settings/rubrik/view');
		}

		$data['name'] = $this->input->post('name');
		$data['slug'] = url_title($this->input->post('name'), 'dash', true);
		$data['status'] = ($this->input->post('status') != '' ? $this->input->post('status'):1);
		$data['picture'] = ($this->input->post('rubrik_picture') != '' ? $this->input->post('rubrik_picture'):NULL);
		$data['description'] = $this->input->post('description');
		$data['keywords'] = $this->input->post('keywords');
		$data['channel'] = ($this->input->post('channel')) ? $this->input->post('channel'):1;
		$data['created_at'] = date('Y-m-d H:i:s');

		if ($this->CategoriesModel->save($data)) {
			$this->session->set_flashdata('success','Rubrik saved.!');
			redirect(backend.'settings/rubrik/view');
		}else{
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'settings/rubrik/view');
		}
	}

	public function edit(){
		if (!$this->access_menu->access('settings-rubrik-update',$this->session->userdata('user_id'))) {
			$this->session->set_flashdata('error','No Access .!');
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Edit Rubrik');
		$data['title'] = 'Edit Rubrik';

		$rubrikId = $this->input->post('rubrikId');

		$rubrikId = $this->uri->segment(5);
		$dataRubrik = $this->CategoriesModel->getCategoryById($rubrikId);
		if ($dataRubrik === FALSE) {
			redirect(backend.'settings/rubrik/edit/'.$rubrikId);
		}

		$data['dataRubrik'] = $dataRubrik->row();
		$this->load->section('js', 'backend/settings/rubrik/js/edit_js',$data);
		$this->load->view('backend/settings/rubrik/v-edit', $data);
	}

	public function update()
	{
		if (!$this->access_menu->access('settings-rubrik-update',$this->session->userdata('user_id'))) {
			$this->session->set_flashdata('error','Not Authorized.!');
			redirect(backend.'dashboard');
		}

		$rubrikId = $this->input->post('rubrikId');

		$this->form_validation->set_rules('name','name','trim|required');
		$this->form_validation->set_rules('rubrikId','rubrikId','trim|required');
		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form.!');
			redirect(backend.'settings/rubrik/edit/'.$rubrikId);
		}

		$data['name'] = $this->input->post('name');
		$data['slug'] = url_title($this->input->post('name'), 'dash', true);
		$data['status'] = ($this->input->post('status') != '' ? $this->input->post('status'):1);
		$data['picture'] = ($this->input->post('rubrik_picture') != '' ? $this->input->post('rubrik_picture'):NULL);
		$data['description'] = $this->input->post('description');
		$data['keywords'] = $this->input->post('keywords');
		$data['channel'] = ($this->input->post('channel')) ? $this->input->post('channel'):1;
		$data['updated_at'] = date('Y-m-d H:i:s');

		if ($this->CategoriesModel->update($rubrikId,$data)) {
			$this->session->set_flashdata('success','Rubrik updated.!');
			redirect(backend.'settings/rubrik/view');
		}else{
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'settings/rubrik/view');
		}
	}

	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('catId','catId','required');
		if (!$this->access_menu->access('settings-rubrik-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$catId = $this->input->post('catId');
		if (!$this->CategoriesModel->delete($catId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'News deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}
	
}
