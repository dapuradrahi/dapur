<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['TagsModel']);
		$this->load->helper(['date']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');

		// datetime picker
		$this->load->css(assets.'backend/plugins/bootstrap4-timepicker/css/tempusdominus-bootstrap-4.min.css');
		$this->load->js(assets.'backend/plugins/bootstrap4-timepicker/js/tempusdominus-bootstrap-4.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('settings-tags-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Tags Lists';
		$this->output->set_common_meta('Tags Lists');

		$this->load->section('js', 'backend/settings/tags/js/index_js',$data);
		$this->load->view('backend/settings/tags/v-index', $data);
	}

	public function lists()
	{
		$this->output->unset_template();

		if (!$this->access_menu->access('settings-tags-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->input->post('search')) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$memData = $this->TagsModel->getRows($_POST);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $v->tag_name;
			$row[] = date('d-m-Y H:i:s', strtotime($v->tag_created_at));
			$row[] = $v->tag_count;
			$row[] = '<div class="btn-group">
			'.($this->access_menu->access('settings-tags-update',$this->session->userdata('user_id')) ? '<a href="'.backend.'settings/tags/edit/'.$v->tag_id.'"><button type="button" class="btn btn-flat btn-xs btn-success" title="Edit"><i class="fas fa-edit"></i></button></a>':'<button type="button" class="btn btn-flat btn-xs btn-success disabled" title="Edit"><i class="fas fa-edit"></i></button></div>').'
			'.($this->access_menu->access('settings-tags-delete',$this->session->userdata('user_id')) ? '<button type="button" data-id="'.$v->tag_id.'" class="btn btn-flat btn-xs btn-danger tagDelete" title="Delete"><i class="fas fa-trash"></i></button></div>':'<button type="button" class="btn btn-flat btn-xs btn-danger disabled" title="Delete"><i class="fas fa-trash"></i></button></div>').'';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->TagsModel->countAll(),
			"recordsFiltered" => $this->TagsModel->countFiltered($_POST),
			"data" => $data,
		);

		return $this->output->set_output(json_encode($output));
	}

	public function save(){
		$this->output->unset_template();
		$this->form_validation->set_rules('newTag','newTag','trim|required|is_unique[tags.name]');
		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Empty or alerdy exist.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->access_menu->access('settings-tags-create',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}
		$data['name'] = $this->input->post('newTag');
		$data['slug'] = url_title($this->input->post('newTag'), 'dash', true);
		$data['created_at'] = date('Y-m-d H:i:s');

		if ($this->TagsModel->save($data)) {
			$output = array(
				'status' => TRUE,
				'message' => 'Tag Saved.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}
	}

	public function edit(){
		if (!$this->access_menu->access('settings-tags-update',$this->session->userdata('user_id'))) {
			$this->session->set_flashdata('error','No Access .!');
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Edit Tag');
		$data['title'] = 'Edit Tag';

		$tagId = $this->uri->segment(5);
		$dataTag = $this->TagsModel->getTagById($tagId);
		if ($dataTag === FALSE) {
			$this->session->set_flashdata('error','Not found tag .!');
			redirect(backend.'settings/tags/edit/'.$tagId);
		}

		$data['dataTag'] = $dataTag->row();
		// $this->load->section('js', 'backend/settings/tags/js/edit_js',$data);
		$this->load->view('backend/settings/tags/v-edit', $data);
	}

	public function update(){
		$this->output->unset_template();

		$this->form_validation->set_rules('newTag','newTag','trim|required');
		$this->form_validation->set_rules('tagId','tagId','trim|required');
		
		$tagId = $this->input->post('tagId');

		if (!$this->access_menu->access('settings-tags-update',$this->session->userdata('user_id'))) {
			$this->session->set_flashdata('error','No access.!');
			redirect(backend.'settings/tags/edit/'.$tagId);

		}

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form.!');
			redirect(backend.'settings/tags/edit/'.$tagId);
		}

		$data['name'] = $this->input->post('newTag');
		$data['slug'] = url_title($this->input->post('newTag'), 'dash', true);
		$data['updated_at'] = date('Y-m-d H:i:s');

		if ($this->TagsModel->update($tagId,$data)) {
			$this->session->set_flashdata('success','Tag Updated.!');
			redirect(backend.'settings/tags/view');
		}else{
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'settings/tags/edit/'.$tagId);
		}
	}

	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('tagId','tagId','required');
		if (!$this->access_menu->access('settings-tags-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$id = $this->input->post('tagId');
		if (!$this->TagsModel->delete($id)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'News deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}
	
}
