<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['GroupsModel']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('settings-groups-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Groups';
		$this->output->set_common_meta('Groups');

		$groups = $this->ion_auth->groups()->result_array();
		foreach ($groups as $k => $v) {
			$group[] = ['id'=>$v['id'],'text'=>$v['name']];
		}
		$data['groups'] = json_encode($group);
		$data['module_access'] = $this->access_menu->module_access();

		$this->load->section('js', 'backend/settings/groups/js/groups_js',$data);
		$this->load->view('backend/settings/groups/v-index', $data);
	}

	public function lists()
	{
		$this->output->unset_template();

		if (!$this->input->post('search')) {
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->GroupsModel->countAll(),
				"recordsFiltered" => $this->GroupsModel->countFiltered($_POST),
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}
		$memData = $this->GroupsModel->getRows($_POST);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $v->name;
			$row[] = $v->description;
			$row[] = '<div class="btn-group"><a href="'.backend.'settings/groups/edit/'.$v->id.'"><button type="button" class="btn btn-flat btn-xs btn-info" title="Edit"><i class="fas fa-user-edit"></i></button></a><button type="button" data-id="'.$v->id.'" class="btn btn-flat btn-xs btn-danger groupDelete" title="Delete"><i class="fas fa-trash"></i></button></div>';

			$data[] = $row;
		}

		if (!$this->access_menu->access('settings-groups-read',$this->session->userdata('id'))) {
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->GroupsModel->countAll(),
				"recordsFiltered" => $this->GroupsModel->countFiltered($_POST),
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->GroupsModel->countAll(),
				"recordsFiltered" => $this->GroupsModel->countFiltered($_POST),
				"data" => $data,
			);

			return $this->output->set_output(json_encode($output));
		}
	}
	
	public function add()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('name','name','trim|required');
		$this->form_validation->set_rules('module_access[]','module_access','trim|required');

		if (!$this->access_menu->access('settings-groups-create',$this->session->userdata('id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Not Authorized.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash()
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Complete all form.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash()
			);

			return $this->output->set_output(json_encode($output));
		}

		$postModuleAccess = $this->input->post('module_access');
		$module_access = implode('|', $this->input->post('module_access'));

		$groupName = $this->input->post('name');
		$groupDesc = $this->input->post('desc');

		$group = $this->ion_auth->create_group($groupName,$groupDesc);
		if(!$group){

			$output = array(
				'status' => FALSE,
				'message' => 'Error try again.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$dataAccess = array(
			'group_id'=> $group,
			'access' => $module_access,
		);

		if (!$this->GroupsModel->addAccessGroup($dataAccess)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error try again.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'Group added.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}
	}

	public function edit()
	{
		if (!$this->access_menu->access('settings-groups-update',$this->session->userdata('id'))) {
			redirect(backend.'dashboard');
		}

		$this->output->set_common_meta('Edit Group');

		$groupId = $this->uri->segment(5);

		$data['groupInfo'] = $this->GroupsModel->getGroup($groupId)->result_array()[0];
		$data['title'] = 'Edit Group - '.$data['groupInfo']['name'];

		$data['accessGroup'] = $this->access_menu->access_group($groupId);
		$data['module_access'] = $this->access_menu->module_access();


		$this->load->section('js', 'backend/settings/groups/js/edit_js',$data);
		$this->load->view('backend/settings/groups/v-edit', $data);
	}

	public function update()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('name','name','trim|required');
		$this->form_validation->set_rules('module_access[]','module_access','trim|required');
		$this->form_validation->set_rules('groupId','groupId','trim|required');

		if (!$this->access_menu->access('settings-groups-update',$this->session->userdata('user_id'))) {
			$this->session->set_flashdata('error','Not Authorized.!');
			redirect(backend.'dashboard');
		}

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Error Try again.!');
			redirect(backend.'settings/groups/edit/'.$this->input->post('groupId'));
		}else{
			$postModuleAccess = $this->input->post('module_access');
			$module_access = implode('|', $this->input->post('module_access'));

			$groupName = $this->input->post('name');
			$groupDesc = $this->input->post('desc');
			$groupId = $this->input->post('groupId');

			$additional_data = array(
				'description' => $groupDesc,
			);
			$groupUpdate = $this->ion_auth->update_group($groupId, $groupName, $additional_data);
			if(!$groupUpdate){
				$this->session->set_flashdata('error','Error Try again.!');
				redirect(backend.'settings/groups/edit/'.$this->input->post('groupId'));
			}else{
				$dataAccess = array(
					'access' => $module_access,
					'updated_at' => date('Y-m-d H:i:s'),
				);
				if (!$this->GroupsModel->updateAccessGroup($groupId,$dataAccess)) {
					$this->session->set_flashdata('error','Error Try again.!');
					redirect(backend.'settings/groups/edit/'.$this->input->post('groupId'));
				}else{
					$this->session->set_flashdata('success','Group updated.!');
					redirect(backend.'settings/groups/edit/'.$this->input->post('groupId'));
				}
			}
		}
	}
	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('groupId','groupId','required');
		if (!$this->access_menu->access('settings-groups-delete',$this->session->userdata('id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$groupId = $this->input->post('groupId');
		if (!$this->GroupsModel->deleteAccessGroup($groupId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$this->ion_auth->delete_group($groupId);
			$output = array(
				'status' => TRUE,
				'message' => 'Group deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}
}
