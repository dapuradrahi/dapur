<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->helper(['file','form','date','string']);
		$this->load->model(['UsersModel','NewsModelUser','NewsPicturesModelUser','NewsVideosModelUser']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// datetime picker
		$this->load->css(assets.'backend/plugins/daterangepicker/daterangepicker.css');
		$this->load->js(assets.'backend/plugins/daterangepicker/daterangepicker.js');

		$this->load->css(assets.'backend/plugins/bootstrap4-timepicker/css/tempusdominus-bootstrap-4.min.css');
		$this->load->js(assets.'backend/plugins/bootstrap4-timepicker/js/tempusdominus-bootstrap-4.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('settings-users-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Users';
		$this->output->set_common_meta('Users');

		$groups = $this->ion_auth->groups()->result_array();
		foreach ($groups as $k => $v) {
			$group[] = ['id'=>$v['id'],'text'=>$v['name']];
		}
		$data['groups'] = json_encode($group);

		$this->load->section('js', 'backend/settings/users/js/users_js',$data);
		$this->load->view('backend/settings/users/v-index', $data);
	}

	public function lists()
	{
		$this->output->unset_template();
		if (!$this->input->post('search')) {
			return $this->output->set_output('Not allowed');
		}
		$memData = $this->UsersModel->getRows($_POST);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = '<a data-fancybox="gallery" href="'.storage.'profile/'.($v->picture == NULL ? 'default.png':$v->picture).'"><img src="'.storage.'profile/thumb/'.($v->picture == NULL ? 'default.png':$v->picture).'" class="img-circle img-size-32 mr-2"></a>';
			$row[] = $v->first_name.' '.$v->last_name;
			$row[] = $v->email;
			$row[] = $v->phone;
			$row[] = $v->company;
			$row[] = '<span class="badge badge-'.strtolower($v->group_name).'">'.$v->group_name.'</span>';
			$row[] = ($v->active == 1 ? '<span class="badge badge-info">Active</span>':'<span class="badge badge-danger">Inactive</span>');
			$row[] = '<div class="btn-group"><a href="'.backend.'settings/users/view/'.$v->id.'"><button type="button" data-id="" class="btn btn-flat bg-purple btn-xs" title="View"><i class="fas fa-eye"></i></button></a><a href="'.backend.'settings/users/edit/'.$v->id.'"><button type="button" class="btn btn-flat btn-xs btn-info userEdit" title="Edit"><i class="fas fa-user-edit"></i></button></a><button type="button" data-id="'.$v->id.'" class="btn btn-flat btn-xs btn-danger userDelete" title="Delete"><i class="fas fa-trash"></i></button></div>';

			$data[] = $row;
		}

		if (!$this->access_menu->access('settings-users-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->UsersModel->countAll(),
				"recordsFiltered" => $this->UsersModel->countFiltered($_POST),
				"data" => array(),
			);
			$this->output->set_output(json_encode($output));
		}else{
			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->UsersModel->countAll(),
				"recordsFiltered" => $this->UsersModel->countFiltered($_POST),
				"data" => $data,
			);

			$this->output->set_output(json_encode($output));
		}
	}
	
	public function add()
	{
		$this->output->unset_template();

		if (!$this->access_menu->access('settings-users-create',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Not Authorized.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$this->form_validation->set_rules('username','username','trim|required');
		$this->form_validation->set_rules('email','email','trim|required');
		$this->form_validation->set_rules('password','password','trim|required');
		$this->form_validation->set_rules('first_name','first_name','trim|required');
		$this->form_validation->set_rules('gender','gender','trim|required');
		$this->form_validation->set_rules('phone','phone','trim|required');
		$this->form_validation->set_rules('active','active','trim|required');
		$this->form_validation->set_rules('userLevel','userLevel','trim|required');

		if (empty($_FILES['picture']['name'])) {
			$dataUpload['file_name'] = 'default.png';
		}else{
			$config['upload_path']      = storagePath.'/profile/';
			$config['allowed_types']    = 'gif|jpg|png|jpeg';
			$config['max_size']         = 3000;
			$config['file_name']		= time();
			$config['max_width']            = 1080;
			$config['max_height']           = 1350;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('picture')){
				$error = $this->upload->display_errors();
				$output = array(
					'status' => FALSE,
					'message' => $error,
					'data' => '',
					'token' => $this->security->get_csrf_hash(),
				);
				return $this->output->set_output(json_encode($output));
			}else{
				$dataUpload = $this->upload->data();
				$this->apps->resizeImage($dataUpload['file_name'],250,250,'/profile/','/profile/thumb/');
			}
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Please complete all form.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$email = $this->input->post('email');

		$group[] = $this->input->post('userLevel'); 

		$additional_data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'display_name' => $this->input->post('display_name'),
			'gender' => $this->input->post('gender'),
			'phone' => $this->input->post('phone'),
			'company' => $this->input->post('company'),
			'active' => $this->input->post('active'),
			'picture' => $dataUpload['file_name'],
		);
		$register = $this->ion_auth->register($username, $password, $email, $additional_data, $group);

		if ($register) {
			// register channel
			$channel = ($this->input->post('channel_id') ? $this->input->post('channel_id'):1);
			$this->db->insert('users_channel',['user_id'=>$register,'channel_id'=>$channel]);

			$output = array(
				'status' => TRUE,
				'message' => 'User Added.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => FALSE,
				'message' => 'Error, Try again.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}

	}

	public function edit(){
		if (!$this->access_menu->access('settings-users-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		if ($id = $this->uri->segment(5) == '') {
			redirect(backend.'dashboard');
		}
		$id = $this->uri->segment(5);
		$userdata = $this->ion_auth->user($id)->result_array();
		if (count($userdata) < 0) {
			redirect(backend.'dashboard');
		}

		$data['userdata'] = $userdata[0];

		$data['title'] = 'Edit User';
		$this->output->set_common_meta('Edit User');

		$groups = $this->ion_auth->groups()->result_array();
		foreach ($groups as $k => $v) {
			$group[] = ['id'=>$v['id'],'text'=>$v['name']];
		}
		$data['groups'] = json_encode($group);

		$this->load->section('js', 'backend/settings/users/js/edit_js',$data);
		$this->load->view('backend/settings/users/v-edit', $data);
	}

	public function detail(){
		if (!$this->access_menu->access('settings-users-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Detail user';
		$this->output->set_common_meta('Users user');

		$id = $this->uri->segment(5);
		$userdata = $this->ion_auth->user($id)->result_array();
		if (count($userdata) < 0) {
			redirect(backend.'dashboard');
		}

		$data['userdata'] = $userdata[0];

		$groups = $this->ion_auth->groups()->result_array();
		foreach ($groups as $k => $v) {
			$group[] = ['id'=>$v['id'],'text'=>$v['name']];
		}
		$data['groups'] = json_encode($group);

		$this->load->section('js', 'backend/settings/users/js/detail_js',$data);
		$this->load->view('backend/settings/users/v-detail', $data);
	}

	public function update(){
		$this->output->unset_template();

		if (!$this->access_menu->access('settings-users-update',$this->session->userdata('user_id'))) {
			$this->session->set_flashdata('error','Not Authorized.!');
			redirect(backend.'settings/users/edit/'.$this->input->post('uid'));
		}

		$this->form_validation->set_rules('username','username','trim|required');
		$this->form_validation->set_rules('email','email','trim|required');
		$this->form_validation->set_rules('first_name','first_name','trim|required');
		$this->form_validation->set_rules('gender','gender','trim|required');
		$this->form_validation->set_rules('phone','phone','trim|required');
		$this->form_validation->set_rules('active','active','trim|required');
		$this->form_validation->set_rules('userLevel','userLevel','trim|required');
		$this->form_validation->set_rules('uid','uid','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form.!');
			redirect(backend.'settings/users/edit/'.$this->input->post('uid'));
		}

		if ($this->input->post('password') != '') {
			$dataUpdate['password'] = $this->input->post('password');
		}

		if (empty($_FILES['picture']['name'])) {
			$dataUpdate['picture'] = $this->input->post('oldPicture');
		}else{
			$config['upload_path']      = storagePath.'/profile/';
			$config['allowed_types']    = 'jpg|png|jpeg';
			$config['max_size']         = 3000;
			$config['file_name']		= time();
			$config['max_width']            = 1080;
			$config['max_height']           = 1350;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('picture')){
				$error = $this->upload->display_errors();
				$this->session->set_flashdata('error',$error);
				redirect(backend.'settings/users/edit/'.$this->input->post('uid'));
			}else{
				if ($this->input->post('oldPicture') != 'default.png') {
					unlink(storagePath.'/profile/'.$this->input->post('oldPicture'));
					unlink(storagePath.'/profile/thumb/'.$this->input->post('oldPicture'));
				}
				$dataUpload = $this->upload->data();
				$this->apps->resizeImage($dataUpload['file_name'],250,250,'/profile/','/profile/thumb/');
				$dataUpdate['picture'] = $dataUpload['file_name'];
			}
		}

		$id = $this->input->post('uid');

		$group = $this->input->post('userLevel');
		if ($this->ion_auth->remove_from_group(NULL,$id)) {
			$this->ion_auth->add_to_group($group, $id);
		}

		$dataUpdate['username'] = $this->input->post('username');
		$dataUpdate['email'] = $this->input->post('email');

		$dataUpdate['first_name'] = $this->input->post('first_name');
		$dataUpdate['last_name'] = $this->input->post('last_name');
		$dataUpdate['display_name'] = $this->input->post('display_name');
		$dataUpdate['gender'] = $this->input->post('gender');
		$dataUpdate['phone'] = $this->input->post('phone');
		$dataUpdate['company'] = $this->input->post('company');
		$dataUpdate['active'] = $this->input->post('active');

		if ($this->ion_auth->update($id, $dataUpdate)) {
			$this->session->set_flashdata('success','User Updated');
			redirect(backend.'settings/users/edit/'.$this->input->post('uid'));
		}else{
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'settings/users/edit/'.$this->input->post('uid'));
		}

	}

	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('uid','uid','required');

		if (!$this->access_menu->access('settings-users-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Not Authorized.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}


		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'User id not found.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$uid = $this->input->post('uid');
		$user = $this->ion_auth->user($uid)->row();

		if ($user->picture != 'default.png') {
			unlink(storagePath.'/profile/'.$user->picture);
			unlink(storagePath.'/profile/thumb/'.$user->picture);
		}

		if (!$this->ion_auth->delete_user($user->id)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error try again.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'User deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);
			return $this->output->set_output(json_encode($output));
		}
	}

	public function newsList(){

		$this->output->unset_template();

		if (!$this->access_menu->access('news-published-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" => 0,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->input->post('search')) {
			$output = array(
				"draw" => 0,
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$status = ($this->input->post('status') ? $this->input->post('status'):'published');
		$user_id = $_POST['uid'];
		$channel = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		$filterDate = explode('-', $_POST['filterDate']);

		$memData = $this->NewsModelUser->getRows($_POST,$status,$user_id,$filterDate,$channel);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = '<a href="'.backend.'news/create/edit/'.$v->news_id.'">'.$v->news_title;
			$row[] = '<span class="badge badge-success">'.$v->category_name.'</span>';
			$row[] = $this->ion_auth->user($v->author_id)->row()->first_name.' '.$this->ion_auth->user($v->author_id)->row()->last_name;
			$row[] = ($v->editor_id == NULL ? '<span class="text-red">Unknown</span>':$this->ion_auth->user($v->editor_id)->row()->first_name.' '.$this->ion_auth->user($v->editor_id)->row()->last_name);
			$row[] = '<span class="badge badge-info">'.date('d-M-Y H:i:s',strtotime($v->news_published_at)).'</span>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->NewsModelUser->countAll(),
			"recordsFiltered" => $this->NewsModelUser->countFiltered($_POST,$status,$user_id,$filterDate,$channel),
			"data" => $data,
		);

		return $this->output->set_output(json_encode($output));

	}

	public function newsPhoto(){
		$this->output->unset_template();

		if (!$this->access_menu->access('galleries-photos-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->input->post('search')) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$status = ($this->input->post('status')) ? $this->input->post('status'):'';
		$user_id = $this->input->post('uid');
		$filterDate = explode('-', $this->input->post('filterDate'));
		$channel = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		$memData = $this->NewsPicturesModelUser->getRows($_POST,$status,$user_id,$filterDate,$channel);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $v->news_pictures_title;
			$row[] = $this->ion_auth->user($v->news_pictures_author_id)->row()->first_name;
			$row[] = ($v->news_pictures_editor_id == NULL ? '<span class="text-red">Unknown</span>':$this->ion_auth->user($v->news_pictures_editor_id)->row()->first_name);
			$row[] = date('d-M-Y H:i:s',strtotime($v->news_pictures_published_at));

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->NewsPicturesModelUser->countAll(),
			"recordsFiltered" => $this->NewsPicturesModelUser->countFiltered($_POST,$status,$user_id,$filterDate,$channel),
			"data" => $data,
		);

		return $this->output->set_output(json_encode($output));
	}

	public function newsVideo()
	{
		$this->output->unset_template();

		if (!$this->access_menu->access('galleries-videos-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->input->post('search')) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$status = ($this->input->post('status')) ? $this->input->post('status'):'published';
		$user_id = $_POST['uid'];
		$filterDate = explode('-', $_POST['filterDate']);
		$channel = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		$memData = $this->NewsVideosModelUser->getRows($_POST,$status,$user_id,$filterDate,$channel);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){

			if ($v->news_videos_published_at > date('Y-m-d H:i:s')) {
				$status = 'Scheduled';
			}else{
				$status = $v->news_videos_status;
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $v->news_videos_title;
			$row[] = $this->ion_auth->user($v->news_videos_author_id)->row()->first_name;
			$row[] = ($v->news_videos_editor_id == NULL ? '<span class="text-red">Unknown</span>':$this->ion_auth->user($v->news_videos_editor_id)->row()->first_name);
			$row[] = '<span class="badge badge-'.strtolower($status).'">'.$status.'</span>';
			$row[] = date('d-M-Y H:i:s',strtotime($v->news_videos_published_at));
			$row[] = '<div class="btn-group">
			'.($this->access_menu->access('galleries-videos-update',$this->session->userdata('user_id')) ? '<a href="'.backend.'galleries/videos/edit/'.$v->news_videos_id.'"><button type="button" class="btn btn-flat btn-xs btn-success" title="Edit"><i class="fas fa-edit"></i></button></a>':'<button type="button" class="btn btn-flat btn-xs btn-success disabled" title="Edit"><i class="fas fa-edit"></i></button></div>').'
			'.($this->access_menu->access('galleries-videos-delete',$this->session->userdata('user_id')) ? '<button type="button" data-id="'.$v->news_videos_id.'" class="btn btn-flat btn-xs btn-danger '.($status == 'deleted' ? 'deletePermanen':'newsVideosDelete').'" title="Delete"><i class="fas fa-trash"></i></button></div>':'<button type="button" class="btn btn-flat btn-xs btn-danger disabled" title="Delete"><i class="fas fa-trash"></i></button></div>').'';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->NewsVideosModelUser->countAll(),
			"recordsFiltered" => $this->NewsVideosModelUser->countFiltered($_POST,$status,$user_id,$filterDate,$channel),
			"data" => $data,
		);

		return $this->output->set_output(json_encode($output));
	}

}
