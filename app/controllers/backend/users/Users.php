<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth']);
		$this->load->model(['UsersModel']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar');
		$this->load->section('sidebar', 'layouts/backend/sidebar');

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		$this->output->set_common_meta('Users');

		$groups = $this->ion_auth->groups()->result_array();
		foreach ($groups as $k => $v) {
			$group[] = ['id'=>$v['id'],'text'=>$v['name']];
		}
		$data['groups'] = json_encode($group);

		$this->load->section('js', 'backend/users/users_js',$data);
		$this->load->view('backend/users/index', $data);
	}

	public function lists()
	{
		$this->output->unset_template();
		if (!$this->input->post('search')) {
			return $this->output->set_output('Not allowed');
		}
		$memData = $this->UsersModel->getRows($_POST);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = '<a data-fancybox="gallery" href="'.assets.'backend/img/'.$v->picture.'"><img src="'.assets.'backend/img/'.$v->picture.'" class="img-circle img-size-32 mr-2"></a>';
			$row[] = $v->first_name.' '.$v->last_name;
			$row[] = $v->email;
			$row[] = $v->phone;
			$row[] = '<span class="badge badge-'.$v->group_name.'">'.$v->group_name.'</span>';
			$row[] = ($v->active == 1 ? '<span class="badge badge-info">Active</span>':'<span class="badge badge-danger">Inactive</span>');
			$row[] = '<div class="btn-group"><a href=""><button type="button" data-id="" class="btn btn-flat bg-purple btn-xs" title="View"><i class="fas fa-eye"></i></button></a><a href=""><button type="button" data-id="'.$v->id.'" class="btn btn-flat btn-xs btn-info userEdit" title="Edit"><i class="fas fa-user-edit"></i></button></a><button type="button" data-id="'.$v->id.'" class="btn btn-flat btn-xs btn-danger userDelete" title="Delete"><i class="fas fa-trash"></i></button></div>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->UsersModel->countAll(),
			"recordsFiltered" => $this->UsersModel->countFiltered($_POST),
			"data" => $data,
		);
		$this->output->set_output(json_encode($output));
	}
	public function form()
	{}
	public function add()
	{}
	public function update()
	{}
	public function delete()
	{
		
	}

	public function groups()
	{
		$this->output->unset_template();
		$fields = $this->db->list_fields('users');

		foreach ($fields as $v)
		{
			$field[] = $v;
		}
		print_r($fields);
	}
}
