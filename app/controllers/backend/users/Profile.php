<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->helper(['file','form','date','string']);
		$this->load->model(['UsersModel']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		$this->load->css(assets.'backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function detail(){
		$data['title'] = 'Profile user';
		$this->output->set_common_meta('Profile user');

		$id = $this->uri->segment(3);
		if ($id != $this->session->userdata('user_id')) {
			redirect(backend.'dashboard');
		}
		$userdata = $this->ion_auth->user($id)->result_array();

		if (empty($userdata)) {
			redirect(backend.'dashboard');
		}

		$data['userdata'] = $userdata[0];

		$groups = $this->ion_auth->groups()->result_array();
		foreach ($groups as $k => $v) {
			$group[] = ['id'=>$v['id'],'text'=>$v['name']];
		}
		$data['groups'] = json_encode($group);

		$this->load->section('js', 'backend/settings/users/js/detail_js',$data);
		$this->load->view('backend/users/v-detail', $data);
	}

	public function update(){

		$this->output->unset_template();

		$this->form_validation->set_rules('username','username','trim|required');
		$this->form_validation->set_rules('first_name','first_name','trim|required');
		$this->form_validation->set_rules('gender','gender','trim|required');
		$this->form_validation->set_rules('phone','phone','trim|required');
		$this->form_validation->set_rules('uid','uid','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form.!');
			redirect(backend.'users/'.$this->input->post('uid'));
		}

		if ($this->input->post('password') != '') {
			$dataUpdate['password'] = $this->input->post('password');
		}

		if (empty($_FILES['picture']['name'])) {
			$dataUpdate['picture'] = $this->input->post('oldPicture');
		}else{
			$config['upload_path']      = storagePath.'/profile/';
			$config['allowed_types']    = 'jpg|png|jpeg';
			$config['max_size']         = 3000;
			$config['file_name']		= time();
			$config['max_width']            = 1080;
			$config['max_height']           = 1350;

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('picture')){
				$error = $this->upload->display_errors();
				$this->session->set_flashdata('error',$error);
				redirect(backend.'settings/users/edit/'.$this->input->post('uid'));
			}else{
				if ($this->input->post('oldPicture') != 'default.png') {
					unlink(storagePath.'/profile/'.$this->input->post('oldPicture'));
					unlink(storagePath.'/profile/thumb/'.$this->input->post('oldPicture'));
				}
				$dataUpload = $this->upload->data();
				$this->apps->resizeImage($dataUpload['file_name'],250,250,'/profile/','/profile/thumb/');
				$dataUpdate['picture'] = $dataUpload['file_name'];
			}
		}

		$id = $this->input->post('uid');

		$dataUpdate['username'] = $this->input->post('username');

		$dataUpdate['first_name'] = $this->input->post('first_name');
		$dataUpdate['last_name'] = $this->input->post('last_name');
		$dataUpdate['display_name'] = $this->input->post('display_name');
		$dataUpdate['gender'] = $this->input->post('gender');
		$dataUpdate['phone'] = $this->input->post('phone');

		if ($this->ion_auth->update($id, $dataUpdate)) {
			$this->session->set_flashdata('success','User Updated');
			redirect(backend.'users/'.$this->input->post('uid'));
		}else{
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'users/'.$this->input->post('uid'));
		}

	}

}
