<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photos extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['NewsModel','CategoriesModel','NewsPicturesModel','TagsModel']);
		$this->load->helper(['date','string']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		// datetime picker
		$this->load->css(assets.'backend/plugins/bootstrap4-timepicker/css/tempusdominus-bootstrap-4.min.css');
		$this->load->js(assets.'backend/plugins/bootstrap4-timepicker/js/tempusdominus-bootstrap-4.min.js');

		// tynimce
		$this->load->js(assets.'plugins/tinymce/tinymce.min.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('galleries-photos-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'News Photo';
		$this->output->set_common_meta('News Photo');

		$this->load->section('js', 'backend/galleries/photos/js/index_js',$data);
		$this->load->view('backend/galleries/photos/v-index', $data);
	}

	public function lists()
	{
		$this->output->unset_template();

		if (!$this->access_menu->access('galleries-photos-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->input->post('search')) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$status = ($this->input->post('status')) ? $this->input->post('status'):'';
		$user_id = $this->session->userdata('user_id');
		$group = $this->ion_auth->get_users_groups($user_id)->row()->id;
		$channel = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		$memData = $this->NewsPicturesModel->getRows($_POST,$status,$user_id,$group,$channel);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){

			$picture = unserialize($v->news_pictures_pictures);

			if ($v->news_pictures_published_at > date('Y-m-d H:i:s')) {
				$status = 'Scheduled';
			}else{
				$status = $v->news_pictures_status;
			}

			$no++;
			$row = array();
			$row[] = $no;
			// $row[] = $picture[0]['file_name'];
			$row[] = '<a data-fancybox="gallery" href="'.storage.'picture/high/'.$picture[0]['file_name'].'" data-caption="'.$picture[0]['caption'].' | '.$picture[0]['source'].'"><img src="'.storage.'picture/low/'.$picture[0]['file_name'].'" class="img-size-50 p-0"></a>';
			$row[] = $v->news_pictures_title;
			$row[] = $this->ion_auth->user($v->news_pictures_author_id)->row()->first_name;
			$row[] = ($v->news_pictures_editor_id == NULL ? '<span class="text-red">Unknown</span>':$this->ion_auth->user($v->news_pictures_editor_id)->row()->first_name);
			$row[] = '<span class="badge badge-'.strtolower($status).'">'.$status.'</span>';
			$row[] = date('d-M-Y H:i:s',strtotime($v->news_pictures_published_at));
			$row[] = '<div class="btn-group">
			'.($this->access_menu->access('galleries-photos-update',$this->session->userdata('user_id')) ? '<a href="'.backend.'galleries/photos/edit/'.$v->news_picture_id.'"><button type="button" class="btn btn-flat btn-xs btn-success" title="Edit"><i class="fas fa-edit"></i></button></a>':'<button type="button" class="btn btn-flat btn-xs btn-success disabled" title="Edit"><i class="fas fa-edit"></i></button></div>').'
			'.($this->access_menu->access('galleries-photos-delete',$this->session->userdata('user_id')) ? '<button type="button" data-id="'.$v->news_picture_id.'" class="btn btn-flat btn-xs btn-danger '.($status == 'deleted' ? 'deletePermanen':'newsPhotoDelete').'" title="Delete"><i class="fas fa-trash"></i></button></div>':'<button type="button" class="btn btn-flat btn-xs btn-danger disabled" title="Delete"><i class="fas fa-trash"></i></button></div>').'';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->NewsPicturesModel->countAll(),
			"recordsFiltered" => $this->NewsPicturesModel->countFiltered($_POST,$status,$user_id,$group,$channel),
			"data" => $data,
		);

		return $this->output->set_output(json_encode($output));
	}

	public function add()
	{
		if (!$this->access_menu->access('galleries-photos-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Add News Photo');
		$data['title'] = 'Add News Photo';

		$this->load->section('js', 'backend/galleries/photos/js/add_js',$data);
		$this->load->view('backend/galleries/photos/v-add', $data);
	}

	public function save(){
		$this->output->unset_template();
		if (!$this->access_menu->access('galleries-photos-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('author','author','trim|required');
		$this->form_validation->set_rules('imageId[]','imageId','trim|required');
		$this->form_validation->set_rules('imageFilename[]','imageFilename','trim|required');
		$this->form_validation->set_rules('imageAuthor[]','author','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form, Min 1 Image');
			redirect(backend.'galleries/photos/add');
		}

		$imageId = $this->input->post('imageId');
		$imageFilename = $this->input->post('imageFilename');
		$imageCaption = $this->input->post('imageCaption');
		$imageSource = $this->input->post('imageSource');
		$imageAuthor = $this->input->post('imageAuthor');

		for ($i=0; $i < count($imageId) ; $i++) {
			$iamgesCollect[] = ['image'=> $imageId[$i],'file_name'=>$imageFilename[$i],'author'=>$imageAuthor[$i],'caption'=>$imageCaption[$i],'source'=> $imageSource[$i]];
		}

		$status = $this->input->post('status');
		if ($status == 'publish') {
			$status = 'published';
		}else{
			$status = 'draft';
		}

		$tags = $this->input->post('tags');

		$data['editor_id'] = ($this->input->post('editor')) ? $this->input->post('editor'):NULL;
		$data['author_id'] = $this->input->post('author');
		$data['publisher_id'] = ($this->input->post('publisher') ? $this->input->post('publisher'):1);
		$data['title'] = $this->input->post('title');
		$data['slug'] = url_title($this->input->post('title'), 'dash', true);
		$data['excerpt'] = $this->input->post('excerpt');
		$data['pictures'] = serialize($iamgesCollect);
		$data['content'] = $this->input->post('content');
		$data['status'] = $status;
		$data['related_news'] = ($this->input->post('related')) ? serialize($this->input->post('related')):NULL;
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['hash'] = random_string('alnum', 12);
		$data['channel'] = ($this->input->post('channel')) ? serialize($this->input->post('channel')):1;
		$data['published_at'] = ($this->input->post('schedule')) ? date('Y-m-d H:i:s',strtotime($this->input->post('schedule'))):date('Y-m-d H:i:s');

		$saveNewsPicture = $this->NewsPicturesModel->saveNewsPicture($data,$tags);
		if ($saveNewsPicture === FALSE){
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'galleries/photos/add');
		}else{
			$this->session->set_flashdata('success','News Photo '.$this->input->post('status'). ' Success');
			redirect(backend.'galleries/photos/edit/'.$saveNewsPicture);
		}
	}

	public function edit(){
		if (!$this->access_menu->access('galleries-photos-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Edit News Photo');
		$data['title'] = 'Edit News Photo';

		$newsId = $this->uri->segment(5);
		$dataNews = $this->NewsPicturesModel->getNewsPictureById($newsId);
		if ($dataNews === FALSE) {
			redirect(backend.'dashboard');
		}

		$data['newsPicture'] = $dataNews->row();
		$data['tags'] = $this->TagsModel->getNewsTags($data['newsPicture']->news_picture_id,'photo');
		$data['related'] = $this->NewsModel->related_news(unserialize($data['newsPicture']->news_pictures_related_news))->result_array();
		$data['pictures'] = unserialize($data['newsPicture']->news_pictures_pictures);

		$this->load->section('js', 'backend/galleries/photos/js/add_js',$data);
		$this->load->view('backend/galleries/photos/v-edit', $data);
	}

	public function update(){
		$this->output->unset_template();
		if (!$this->access_menu->access('galleries-photos-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('author','author','trim|required');
		$this->form_validation->set_rules('hash','hash','trim|required');
		$this->form_validation->set_rules('newsPictureId','newsPictureId','trim|required');
		$this->form_validation->set_rules('imageId[]','imageId','trim|required');
		$this->form_validation->set_rules('imageFilename[]','imageFilename','trim|required');
		$this->form_validation->set_rules('imageAuthor[]','author','trim|required');

		$newsPictureId = $this->input->post('newsPictureId');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form, Min 1 Image');
			redirect(backend.'galleries/photos/edit/'.$newsPictureId);
		}

		$imageId = $this->input->post('imageId');
		$imageFilename = $this->input->post('imageFilename');
		$imageCaption = $this->input->post('imageCaption');
		$imageSource = $this->input->post('imageSource');
		$imageAuthor = $this->input->post('imageAuthor');

		for ($i=0; $i < count($imageId) ; $i++) {
			$iamgesCollect[] = ['image'=> $imageId[$i],'file_name'=>$imageFilename[$i],'author'=>$imageAuthor[$i],'caption'=>$imageCaption[$i],'source'=> $imageSource[$i]];
		}

		$status = $this->input->post('status');
		if ($status == 'publish') {
			$status = 'published';
		}else{
			$status = 'draft';
		}

		$tags = $this->input->post('tags');

		$data['editor_id'] = ($this->input->post('editor')) ? $this->input->post('editor'):NULL;
		$data['author_id'] = $this->input->post('author');
		$data['publisher_id'] = ($this->input->post('publisher') ? $this->input->post('publisher'):1);
		$data['title'] = $this->input->post('title');
		$data['slug'] = url_title($this->input->post('title'), 'dash', true);
		$data['excerpt'] = $this->input->post('excerpt');
		$data['pictures'] = serialize($iamgesCollect);
		$data['content'] = $this->input->post('content');
		$data['status'] = $status;
		$data['related_news'] = ($this->input->post('related')) ? serialize($this->input->post('related')):NULL;
		$data['hash'] = $this->input->post('hash');
		$data['channel'] = ($this->input->post('channel')) ? serialize($this->input->post('channel')):1;
		$data['published_at'] = ($this->input->post('schedule')) ? date('Y-m-d H:i:s',strtotime($this->input->post('schedule'))):date('Y-m-d H:i:s');

		$updateNewsPicture = $this->NewsPicturesModel->update($data,$tags,$newsPictureId);
		if ($updateNewsPicture === FALSE){
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'galleries/photos/edit/'.$newsPictureId);
		}else{
			$this->session->set_flashdata('success','News Photo Updated.!');
			redirect(backend.'galleries/photos/edit/'.$newsPictureId);
		}
	}

	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('newsPicuteId','newsPicuteId','required');
		if (!$this->access_menu->access('galleries-photos-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$newsPicuteId = $this->input->post('newsPicuteId');
		if (!$this->NewsPicturesModel->newsPicuteDelete($newsPicuteId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'News Photo deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}

	public function delete_permanen()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('newsPicuteId','newsPicuteId','required');
		if (!$this->access_menu->access('galleries-photos-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$newsPicuteId = $this->input->post('newsPicuteId');
		if (!$this->NewsPicturesModel->newsDeletePermanent($newsPicuteId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'News Photo deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}
}