<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['NewsModel','CategoriesModel','NewsVideosModel','TagsModel']);
		$this->load->helper(['date','string']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		// datetime picker
		$this->load->css(assets.'backend/plugins/bootstrap4-timepicker/css/tempusdominus-bootstrap-4.min.css');
		$this->load->js(assets.'backend/plugins/bootstrap4-timepicker/js/tempusdominus-bootstrap-4.min.js');

		// tynimce
		$this->load->js(assets.'plugins/tinymce/tinymce.min.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('galleries-videos-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'News Video';
		$this->output->set_common_meta('News Video');

		$this->load->section('js', 'backend/galleries/videos/js/index_js',$data);
		$this->load->view('backend/galleries/videos/v-index', $data);
	}

	public function lists()
	{
		$this->output->unset_template();

		if (!$this->access_menu->access('galleries-videos-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->input->post('search')) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$status = ($this->input->post('status')) ? $this->input->post('status'):'published';
		$user_id = $this->session->userdata('user_id');
		$group = $this->ion_auth->get_users_groups($user_id)->row()->id;
		$channel = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		$memData = $this->NewsVideosModel->getRows($_POST,$status,$user_id,$group,$channel);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){

			if ($v->news_videos_published_at > date('Y-m-d H:i:s')) {
				$status = 'Scheduled';
			}else{
				$status = $v->news_videos_status;
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $v->news_videos_title;
			$row[] = $this->ion_auth->user($v->news_videos_author_id)->row()->first_name;
			$row[] = ($v->news_videos_editor_id == NULL ? '<span class="text-red">Unknown</span>':$this->ion_auth->user($v->news_videos_editor_id)->row()->first_name);
			$row[] = '<span class="badge badge-'.strtolower($status).'">'.$status.'</span>';
			$row[] = date('d-M-Y H:i:s',strtotime($v->news_videos_published_at));
			$row[] = '<div class="btn-group">
			'.($this->access_menu->access('galleries-videos-update',$this->session->userdata('user_id')) ? '<a href="'.backend.'galleries/videos/edit/'.$v->news_videos_id.'"><button type="button" class="btn btn-flat btn-xs btn-success" title="Edit"><i class="fas fa-edit"></i></button></a>':'<button type="button" class="btn btn-flat btn-xs btn-success disabled" title="Edit"><i class="fas fa-edit"></i></button></div>').'
			'.($this->access_menu->access('galleries-videos-delete',$this->session->userdata('user_id')) ? '<button type="button" data-id="'.$v->news_videos_id.'" class="btn btn-flat btn-xs btn-danger '.($status == 'deleted' ? 'deletePermanen':'newsVideosDelete').'" title="Delete"><i class="fas fa-trash"></i></button></div>':'<button type="button" class="btn btn-flat btn-xs btn-danger disabled" title="Delete"><i class="fas fa-trash"></i></button></div>').'';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->NewsVideosModel->countAll(),
			"recordsFiltered" => $this->NewsVideosModel->countFiltered($_POST,$status,$user_id,$group,$channel),
			"data" => $data,
		);

		return $this->output->set_output(json_encode($output));
	}

	public function add()
	{
		if (!$this->access_menu->access('galleries-photos-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Add News Video');
		$data['title'] = 'Add News Video';

		$this->load->section('js', 'backend/galleries/videos/js/add_js',$data);
		$this->load->view('backend/galleries/videos/v-add', $data);
	}

	public function save(){
		$this->output->unset_template();
		if (!$this->access_menu->access('galleries-videos-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('author','author','trim|required');
		$this->form_validation->set_rules('video','video','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form, Min 1 Video');
			redirect(backend.'galleries/videos/add');
		}

		$status = $this->input->post('status');
		if ($status == 'publish') {
			$status = 'published';
		}else{
			$status = 'draft';
		}

		$tags = $this->input->post('tags');

		$data['editor_id'] = ($this->input->post('editor')) ? $this->input->post('editor'):NULL;
		$data['author_id'] = $this->input->post('author');
		$data['publisher_id'] = ($this->input->post('publisher') ? $this->input->post('publisher'):1);
		$data['title'] = $this->input->post('title');
		$data['slug'] = url_title($this->input->post('title'), 'dash', true);
		$data['excerpt'] = $this->input->post('excerpt');
		$data['videos'] =$this->input->post('video');
		$data['content'] = $this->input->post('content');
		$data['status'] = $status;
		$data['related_news'] = ($this->input->post('related')) ? serialize($this->input->post('related')):NULL;
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['hash'] = random_string('alnum', 12);
		$data['channel'] = ($this->input->post('channel')) ? serialize($this->input->post('channel')):1;
		$data['published_at'] = ($this->input->post('schedule')) ? date('Y-m-d H:i:s',strtotime($this->input->post('schedule'))):date('Y-m-d H:i:s');

		$saveNewsvideos = $this->NewsVideosModel->saveNewsvideos($data,$tags);
		if ($saveNewsvideos === FALSE){
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'galleries/videos/add');
		}else{
			$this->session->set_flashdata('success','News Video '.$this->input->post('status'). ' Success');
			redirect(backend.'galleries/videos/view');
		}
	}

	public function edit()
	{
		if (!$this->access_menu->access('galleries-videos-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$newsVideoId = $this->uri->segment(5);
		$dataNewsVideo = $this->NewsVideosModel->getNewsVideoById($newsVideoId);
		if ($dataNewsVideo === FALSE) {
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Add News Video');
		$data['title'] = 'Add News Video';

		$data['newsVideo'] = $dataNewsVideo->row();
		$data['tags'] = $this->TagsModel->getNewsTags($data['newsVideo']->news_videos_id,'video');
		$data['related'] = $this->NewsModel->related_news(unserialize($data['newsVideo']->news_videos_related_news))->result_array();

		$this->load->section('js', 'backend/galleries/videos/js/add_js',$data);
		$this->load->view('backend/galleries/videos/v-edit', $data);
	}

	public function update(){
		$this->output->unset_template();
		if (!$this->access_menu->access('galleries-videos-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('author','author','trim|required');
		$this->form_validation->set_rules('video','video','trim|required');
		$this->form_validation->set_rules('newsVideoId','newsVideoId','trim|required');

		$newsVideoId = $this->input->post('newsVideoId');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form, Min 1 Video');
			redirect(backend.'galleries/videos/edit/'.$newsVideoId);
		}

		$status = $this->input->post('status');
		if ($status == 'publish') {
			$status = 'published';
		}else{
			$status = 'draft';
		}

		$tags = $this->input->post('tags');

		$data['editor_id'] = ($this->input->post('editor')) ? $this->input->post('editor'):NULL;
		$data['author_id'] = $this->input->post('author');
		$data['publisher_id'] = ($this->input->post('publisher') ? $this->input->post('publisher'):1);
		$data['title'] = $this->input->post('title');
		$data['slug'] = url_title($this->input->post('title'), 'dash', true);
		$data['excerpt'] = $this->input->post('excerpt');
		$data['videos'] = $this->input->post('video');
		$data['content'] = $this->input->post('content');
		$data['status'] = $status;
		$data['related_news'] = ($this->input->post('related')) ? serialize($this->input->post('related')):NULL;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['channel'] = ($this->input->post('channel')) ? serialize($this->input->post('channel')):1;
		$data['published_at'] = ($this->input->post('schedule')) ? date('Y-m-d H:i:s',strtotime($this->input->post('schedule'))):date('Y-m-d H:i:s');

		$updateNewsvideos = $this->NewsVideosModel->update($data,$tags,$newsVideoId);
		if ($updateNewsvideos === FALSE){
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'galleries/videos/edit/'.$newsVideoId);
		}else{
			$this->session->set_flashdata('success','News Video Udated.!');
			redirect(backend.'galleries/videos/edit/'.$newsVideoId);
		}
	}

	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('newsVideosId','newsVideosId','required');
		if (!$this->access_menu->access('galleries-videos-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$newsVideosId = $this->input->post('newsVideosId');
		if (!$this->NewsVideosModel->newsVideosDelete($newsVideosId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'News Video deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}

	public function delete_permanen()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('newsVideosId','newsVideosId','required');
		if (!$this->access_menu->access('galleries-videos-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$newsVideosId = $this->input->post('newsVideosId');
		if (!$this->NewsVideosModel->newsVideoDeletePermanent($newsVideosId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'News Video deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}
}
