<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topic extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['TopicModel']);
		$this->load->helper(['date','string']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		// datetime picker
		$this->load->css(assets.'backend/plugins/bootstrap4-timepicker/css/tempusdominus-bootstrap-4.min.css');
		$this->load->js(assets.'backend/plugins/bootstrap4-timepicker/js/tempusdominus-bootstrap-4.min.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('managements-topic-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Topik Khusus';
		$this->output->set_common_meta('Topik Khusus');

		$this->load->section('js', 'backend/managements/topic/js/index_js',$data);
		$this->load->view('backend/managements/topic/v-index', $data);
	}

	public function lists()
	{
		$this->output->unset_template();

		if (!$this->access_menu->access('managements-topic-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->input->post('search')) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$channel = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		$memData = $this->TopicModel->getRows($_POST,$channel);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){
			if ($v->topic_picture == NULL) {
				$picture = '<a data-fancybox="gallery" href="'.storage.'no-image.png" data-caption="no image"><img src="'.storage.'no-image.png" class="img-size-50 p-0"></a>';
			}else{
				$picture = '<a data-fancybox="gallery" href="'.storage.'picture/high/'.$v->topic_picture.'" data-caption="'.$v->topic_name.'"><img src="'.storage.'picture/low/'.$v->topic_picture.'" class="img-size-50 p-0"></a>';
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $picture;
			$row[] = $v->topic_name;
			$row[] = $v->topic_description;
			$row[] = date('d-m-Y H-i:s', strtotime($v->topic_created_at));
			$row[] = $this->apps->format_k_number(0);
			$row[] = '<div class="btn-group">
			'.($this->access_menu->access('managements-topic-update',$this->session->userdata('user_id')) ? '<a href="'.backend.'managements/topic/edit/'.$v->topic_id.'"><button type="button" class="btn btn-flat btn-xs btn-success" title="Edit"><i class="fas fa-edit"></i></button></a>':'<button type="button" class="btn btn-flat btn-xs btn-success disabled" title="Edit"><i class="fas fa-user-edit"></i></button></div>').'
			'.($this->access_menu->access('managements-topic-delete',$this->session->userdata('user_id')) ? '<button type="button" data-id="'.$v->topic_id.'" class="btn btn-flat btn-xs btn-danger topicDelete" title="Delete"><i class="fas fa-trash"></i></button></div>':'<button type="button" class="btn btn-flat btn-xs btn-danger disabled" title="Delete"><i class="fas fa-trash"></i></button></div>').'';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->TopicModel->countAll(),
			"recordsFiltered" => $this->TopicModel->countFiltered($_POST,$channel),
			"data" => $data,
		);

		return $this->output->set_output(json_encode($output));
	}

	public function add()
	{
		if (!$this->access_menu->access('managements-topic-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Add new topic');
		$data['title'] = 'Add new topic';

		$this->load->section('js', 'backend/managements/topic/js/add_js',$data);
		$this->load->view('backend/managements/topic/v-add', $data);
	}

	public function save(){
		$this->output->unset_template();
		if (!$this->access_menu->access('managements-topic-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('name','name','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form');
			redirect(backend.'managements/topic/add');
		}

		$data['author_id'] = $this->session->userdata('user_id');
		$data['name'] = $this->input->post('name');
		$data['slug'] = url_title($this->input->post('name'), 'dash', true);
		$data['description'] = $this->input->post('description');
		$data['keywords'] = $this->input->post('keywords');
		$data['picture'] = ($this->input->post('topic_picture')) ? $this->input->post('topic_picture'):NULL;
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['channel'] = ($this->input->post('channel')) ? $this->input->post('channel'):1;
		$data['hash'] = random_string('alnum', 12);

		$saveTopic = $this->TopicModel->saveTopic($data);
		if ($saveTopic === FALSE){
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'managements/topic/add');
		}else{
			$this->session->set_flashdata('success','Topic added');
			redirect(backend.'managements/topic/view');
		}
	}

	public function edit(){
		if (!$this->access_menu->access('managements-topic-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Edit topic');
		$data['title'] = 'Edit topic';

		$topicId = $this->uri->segment(5);
		$dataTopic = $this->TopicModel->getTopicById($topicId);
		if ($dataTopic === FALSE) {
			redirect(backend.'dashboard');
		}

		$data['topic'] = $dataTopic->row();

		$this->load->section('js', 'backend/managements/topic/js/add_js',$data);
		$this->load->view('backend/managements/topic/v-edit', $data);
	}

	public function update(){
		$this->output->unset_template();
		if (!$this->access_menu->access('managements-topic-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('name','name','trim|required');
		$this->form_validation->set_rules('topicId','topicId','trim|required');
		$this->form_validation->set_rules('hash','hash','trim|required');

		$topicId = $this->input->post('topicId');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form');
			redirect(backend.'managements/topic/edit/'.$topicId);
		}

		$data['author_id'] = $this->input->post('topicAuthor');
		$data['name'] = $this->input->post('name');
		$data['slug'] = url_title($this->input->post('name'), 'dash', true);
		$data['description'] = $this->input->post('description');
		$data['keywords'] = $this->input->post('keywords');
		$data['picture'] = ($this->input->post('topic_picture')) ? $this->input->post('topic_picture'):NULL;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['channel'] = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		$updateTopic = $this->TopicModel->update($topicId,$data);
		if ($updateTopic === FALSE){
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'managements/topic/edit/'.$topicId);
		}else{
			$this->session->set_flashdata('success','Topic added');
			redirect(backend.'managements/topic/edit/'.$topicId);
		}
	}

	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('topicId','topicId','required');
		if (!$this->access_menu->access('managements-topic-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$topicId = $this->input->post('topicId');
		if (!$this->TopicModel->topicDelete($topicId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'Topic deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}
}
