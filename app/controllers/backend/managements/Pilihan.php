<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pilihan extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['TopicModel','PilihanModel']);
		$this->load->helper(['date','string']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('managements-pilihan-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Pilihan Editor';
		$this->output->set_common_meta('Pilihan Editor');

		$pilihan = $this->PilihanModel->pilihan(0,1);
		if ($pilihan->num_rows() > 0) {
			$pilihan_news = unserialize($pilihan->row()->pilihan_news_id);
			$pilihan_id = $pilihan->row()->pilihan_id;
		}else{
			$pilihan_news = [0];
			$pilihan_id = 0;
		}

		$news_pilihan = $this->PilihanModel->getNewsPilihan($pilihan_news);

		$data['pilihan'] = $news_pilihan;
		$data['pilihan_id'] = $pilihan_id;
		$data['max_headline'] = 4;

		$this->load->section('js', 'backend/managements/pilihan/js/index_js',$data);
		$this->load->view('backend/managements/pilihan/v-index', $data);
	}

	public function save(){
		$this->output->unset_template();
		if (!$this->access_menu->access('managements-pilihan-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('news_id[]','news_id','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form');
			redirect(backend.'managements/pilihan/view');
		}

		$pilihan_id = $this->input->post('pilihan_id');
		$data['news_id'] = serialize($this->input->post('news_id'));
		$data['type'] = ($this->input->post('type')) ? $this->input->post('type'):0;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['channel'] = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		if ($this->input->post('pilihan_id') == 0) {
			$savePilihan = $this->PilihanModel->save($data);
			if ($savePilihan === FALSE){
				$this->session->set_flashdata('error','Error try again.!');
				redirect(backend.'managements/pilihan/view');
			}else{
				$this->session->set_flashdata('success','Pilihan Editor Created.!');
				redirect(backend.'managements/pilihan/view');
			}
		}else{
			$updatePilihan = $this->PilihanModel->update($pilihan_id,$data);
			if ($updatePilihan === FALSE){
				$this->session->set_flashdata('error','Error try again.!');
				redirect(backend.'managements/pilihan/view');
			}else{
				$this->session->set_flashdata('success','Pilihan Editor Updated');
				redirect(backend.'managements/pilihan/view');
			}
		}
	}
}
