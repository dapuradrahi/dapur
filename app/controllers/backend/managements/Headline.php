<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Headline extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['TopicModel','HeadlineModel']);
		$this->load->helper(['date','string']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('managements-headline-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Headline News';
		$this->output->set_common_meta('Headline News');
		$headline = $this->HeadlineModel->headline(0,1);
		if ($headline->num_rows() > 0) {
			$headline_news = unserialize($headline->row()->headline_news_id);
			$headline_id = $headline->row()->headline_id;
		}else{
			$headline_news = [0];
			$headline_id = 0;
		}

		$news_headline = $this->HeadlineModel->getNewsHeadline($headline_news);

		$data['headline'] = $news_headline;
		$data['headline_id'] = $headline_id;
		$data['max_headline'] = 4;

		$this->load->section('js', 'backend/managements/headline/js/index_js',$data);
		$this->load->view('backend/managements/headline/v-index', $data);
	}

	public function save(){
		$this->output->unset_template();
		if (!$this->access_menu->access('managements-headline-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('news_id[]','news_id','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form');
			redirect(backend.'managements/headline/view');
		}

		$headline_id = $this->input->post('headline_id');
		$data['news_id'] = serialize($this->input->post('news_id'));
		$data['type'] = ($this->input->post('type')) ? $this->input->post('type'):0;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['channel'] = ($this->input->post('channel')) ? $this->input->post('channel'):1;

		if ($this->input->post('headline_id') == 0) {
			$saveHeadline = $this->HeadlineModel->save($data);
			if ($saveHeadline === FALSE){
				$this->session->set_flashdata('error','Error try again.!');
				redirect(backend.'managements/headline/view');
			}else{
				$this->session->set_flashdata('success','Headline Created.!');
				redirect(backend.'managements/headline/view');
			}
		}else{
			$updateHeadline = $this->HeadlineModel->update($headline_id,$data);
			if ($updateHeadline === FALSE){
				$this->session->set_flashdata('error','Error try again.!');
				redirect(backend.'managements/headline/view');
			}else{
				$this->session->set_flashdata('success','Headline Updated');
				redirect(backend.'managements/headline/view');
			}
		}
	}
}
