<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->_init();
		$this->load->library(['session','ion_auth','form_validation']);
		$this->load->model(['TopicModel','PagesModel']);
		$this->load->helper(['date','string']);
		if (!$this->ion_auth->logged_in()){
			redirect('auth/login');
		}
	}

	private function _init()
	{
		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];

		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		// Datatables
		$this->load->css(assets.'backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');
		$this->load->css(assets.'backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js(assets.'backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/dataTables.responsive.min.js');
		$this->load->js(assets.'backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');

		// datetime picker
		$this->load->css(assets.'backend/plugins/bootstrap4-timepicker/css/tempusdominus-bootstrap-4.min.css');
		$this->load->js(assets.'backend/plugins/bootstrap4-timepicker/js/tempusdominus-bootstrap-4.min.js');

		$this->load->js(assets.'plugins/tinymce/tinymce.min.js');

		// fancybox
		$this->load->css(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.css');
		$this->load->js(assets.'backend/plugins/jquery-fancybox/dist/jquery.fancybox.min.js');

		// select2
		$this->load->css(assets.'backend/plugins/select2/css/select2.min.css');
		$this->load->css(assets.'backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');
		$this->load->js(assets.'backend/plugins/select2/js/select2.full.min.js');
	}

	public function index()
	{
		if (!$this->access_menu->access('managements-pages-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		$data['title'] = 'Pages';
		$this->output->set_common_meta('Pages');

		$this->load->section('js', 'backend/managements/pages/js/index_js',$data);
		$this->load->view('backend/managements/pages/v-index', $data);
	}

	public function lists()
	{
		$this->output->unset_template();

		if (!$this->access_menu->access('managements-pages-read',$this->session->userdata('user_id'))) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		if (!$this->input->post('search')) {
			$output = array(
				"draw" => '',
				"recordsTotal" => 0,
				"recordsFiltered" => 0,
				"data" => array(),
			);
			return $this->output->set_output(json_encode($output));
		}

		$channel = ($this->input->post('channel')) ? $this->input->post('channel'):1;
		$status = ($this->input->post('status')) ? $this->input->post('status'):'published';

		$memData = $this->PagesModel->getRows($_POST,$status,$channel);
		$data = array();

		$no = ($_POST['start'] == ''? 0:0);
		foreach($memData as $v){
			$author = $this->ion_auth->user($v->author_id)->row();

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $v->page_title;
			$row[] = $author->first_name.' '.$author->last_name;
			$row[] = date('d-m-Y H-i:s', strtotime($v->page_created_at));
			$row[] = '<span class="badge badge-'.$v->page_status.'">'.$v->page_status.'</span>';
			$row[] = '<div class="btn-group">
			'.($this->access_menu->access('managements-pages-update',$this->session->userdata('user_id')) ? '<a href="'.backend.'managements/pages/edit/'.$v->page_id.'"><button type="button" class="btn btn-flat btn-xs btn-success" title="Edit"><i class="fas fa-edit"></i></button></a>':'<button type="button" class="btn btn-flat btn-xs btn-success disabled" title="Edit"><i class="fas fa-edit"></i></button></div>').'
			'.($this->access_menu->access('managements-pages-delete',$this->session->userdata('user_id')) ? '<button type="button" data-id="'.$v->page_id.'" class="btn btn-flat btn-xs btn-danger '.($v->page_status == 'deleted' ? 'deletePermanen':'pageDelete').'" title="Delete"><i class="fas fa-trash"></i></button></div>':'<button type="button" class="btn btn-flat btn-xs btn-danger disabled" title="Delete"><i class="fas fa-trash"></i></button></div>').'';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->PagesModel->countAll(),
			"recordsFiltered" => $this->PagesModel->countFiltered($_POST,$status,$channel),
			"data" => $data,
		);

		return $this->output->set_output(json_encode($output));
	}

	public function add()
	{
		if (!$this->access_menu->access('managements-pages-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}
		
		$this->output->set_common_meta('Add new page');
		$data['title'] = 'Add new page';

		$this->load->section('js', 'backend/managements/pages/js/add_js',$data);
		$this->load->view('backend/managements/pages/v-add', $data);
	}

	public function save(){
		$this->output->unset_template();
		if (!$this->access_menu->access('managements-pages-create',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('title','title','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form');
			redirect(backend.'managements/pages/add');
		}

		$data['author_id'] = $this->session->userdata('user_id');
		// $data['prefik'] = $this->input->post('prefix');
		$data['title'] = $this->input->post('title');
		$data['slug'] = url_title($this->input->post('title'), 'dash', true);
		$data['excerpt'] = $this->input->post('excerpt');
		$data['content'] = $this->input->post('content');
		$data['status'] = $this->input->post('status');
		$data['channel'] = ($this->input->post('channel')) ? $this->input->post('channel'):1;
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['published_at'] = date('Y-m-d H:i:s');
		$data['hash'] = random_string('alnum', 12);

		$save = $this->PagesModel->save($data);
		if ($save === FALSE){
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'managements/pages/add');
		}else{
			$this->session->set_flashdata('success','Page added');
			redirect(backend.'managements/pages/view');
		}
	}

	public function edit()
	{
		if (!$this->access_menu->access('managements-pages-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$pageId = $this->uri->segment(5);
		$page = $this->PagesModel->getPageById($pageId);
		if ($page === FALSE) {
			redirect(backend.'dashboard');
		}

		$data['page'] = $page->row();
		
		$this->output->set_common_meta('Edit new page');
		$data['title'] = 'Edit new page';

		$this->load->section('js', 'backend/managements/pages/js/add_js',$data);
		$this->load->view('backend/managements/pages/v-edit', $data);
	}

	public function update(){
		$this->output->unset_template();
		if (!$this->access_menu->access('managements-pages-update',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$this->form_validation->set_rules('title','title','trim|required');
		$this->form_validation->set_rules('status','status','trim|required');
		$this->form_validation->set_rules('pageId','pageId','trim|required');

		if (!$this->form_validation->run()) {
			$this->session->set_flashdata('error','Complete all form');
			redirect(backend.'managements/pages/edit/'.$this->input->post('pageId'));
		}

		$pageId = $this->input->post('pageId');

		$data['title'] = $this->input->post('title');
		$data['slug'] = url_title($this->input->post('title'), 'dash', true);
		$data['excerpt'] = $this->input->post('excerpt');
		$data['content'] = $this->input->post('content');
		$data['status'] = $this->input->post('status');
		$data['channel'] = ($this->input->post('channel')) ? $this->input->post('channel'):1;
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['published_at'] = date('Y-m-d H:i:s');

		$update = $this->PagesModel->update($pageId,$data);
		if ($update === FALSE){
			$this->session->set_flashdata('error','Error try again.!');
			redirect(backend.'managements/pages/edit/'.$pageId);
		}else{
			$this->session->set_flashdata('success','Page updated');
			redirect(backend.'managements/pages/edit/'.$pageId);
		}
	}

	public function delete()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('pageId','pageId','required');
		if (!$this->access_menu->access('managements-pages-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$pageId = $this->input->post('pageId');
		if (!$this->PagesModel->pageDelete($pageId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'Page deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}

	public function delete_permanen()
	{
		$this->output->unset_template();
		$this->form_validation->set_rules('pageId','pageId','required');
		if (!$this->access_menu->access('managements-pages-delete',$this->session->userdata('user_id'))) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		if (!$this->form_validation->run()) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}

		$pageId = $this->input->post('pageId');
		if (!$this->PagesModel->pageDeletePermanent($pageId)) {
			$output = array(
				'status' => FALSE,
				'message' => 'Error.!!!!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}else{
			$output = array(
				'status' => TRUE,
				'message' => 'Page deleted.!',
				'data' => '',
				'token' => $this->security->get_csrf_hash(),
			);

			return $this->output->set_output(json_encode($output));
		}
	}
}
