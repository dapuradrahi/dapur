<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->_init();
		$this->load->library(['ion_auth','session','form_validation']);
		$this->load->helper(['form']);
	}

	private function _init()
	{
		$this->output->set_template('auth/auth');
	}

	public function index()
	{
		$this->output->set_common_meta('Login');
		$this->load->section('js', 'layouts/auth/js_login');
		$this->load->view('auth/v-login');
	}

	public function proccess()
	{
		$this->form_validation->set_rules('identity','identity','required|trim');
		$this->form_validation->set_rules('password','password','required|trim');
		if ($this->form_validation->run()) {
			$identity = $this->input->post('identity');
			$password = $this->input->post('password');
			$remember = ($this->input->post('remember') == 1 ? TRUE:FALSE);
			if ($this->ion_auth->login($identity, $password, $remember)){
				redirect('root-cms/dashboard');
			}else{
				$this->session->set_flashdata('error',$this->ion_auth->errors());
				redirect('auth/login');
			}
		}
	}

	public function logout()
	{
		
	}
}
