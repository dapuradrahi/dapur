<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->_init();
		$this->load->library(['ion_auth','session','form_validation']);
		$this->load->helper(['form']);
	}

	private function _init()
	{
		$this->output->set_template('auth/auth');
	}

	public function index()
	{
		$this->output->set_common_meta('Register');
		$this->load->view('auth/v-register');
	}

	public function proccess()
	{
		$this->form_validation->set_rules('identity','identity','required|trim');
		$this->form_validation->set_rules('password','password','required|trim');
		if ($this->form_validation->run()) {
			$identity = $this->input->post('identity');
			$password = $this->input->post('password');
			$remember = $this->input->post('remember');
			if ($this->ion_auth->login($identity, $password, $remember)) {
			
			}else{

			}
		}
	}

	public function success()
	{
		$this->output->set_common_meta('Register success');
	}
}
