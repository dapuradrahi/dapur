<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library(['session','form_validation']);
		$this->load->model(['GroupsModel','NewsModel','CategoriesModel']);
		$this->load->helper(['date','string','security']);
	}

	public function index()
	{
		// $this->output->set_template('new/index');
		redirect(base_url('auth/login'));
	}

	public function read(){
		$slug = explode('-',$this->uri->segment(2));
		$hashNews = end($slug);

		$query = $this->db->get_where('news', array('hash' => $hashNews))->result_array();
		print_r($query);
	}

	public function error(){
		if (!$this->access_menu->access('dashboard-read',$this->session->userdata('user_id'))) {
			redirect(backend.'dashboard');
		}

		$data['menu'] = $this->access_menu->access_menu($this->session->userdata('user_id'));
		$data['userLogin'] = $this->ion_auth->user($this->session->userdata('user_id'))->result_array()[0];
		$this->output->set_template('backend/app');
		$this->load->section('navbar', 'layouts/backend/navbar',$data);
		$this->load->section('sidebar', 'layouts/backend/sidebar',$data);

		$data['title'] = '';
		$this->output->set_common_meta('Dashboard');
		$this->load->view('backend/errors/404',$data);
	}
}
