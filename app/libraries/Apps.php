<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apps {
	protected $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library(['ion_auth']);
		$this->CI->load->helper(['tgl_indo']);
	}
	
	public function resizeImage($filename,$x,$y,$path,$pathThumb)
	{
		$source_path = storagePath.$path. $filename;
		$target_path = storagePath.$pathThumb;
		$config_manip = array(
			'image_library' => 'gd2',
			'source_image' => $source_path,
			'new_image' => $target_path,
			'maintain_ratio' => TRUE,
			'create_thumb' => TRUE,
			'thumb_marker' => '',
			'width' => $x,
			'height' => $y,
		);
		$this->CI->load->library('image_lib', $config_manip);
		$resize = $this->CI->image_lib->resize();
		if (!$resize) {
			return false;
		}else{
			return true;
		}
		$this->image_lib->clear();
	}

	public function create_thumbs($file_name){
		$config = array(
			array(
				'image_library' => 'gd2',
				'source_image' => storagePath.'/picture/origin/'.$file_name,
				'new_image' => storagePath.'/picture/low/'.$file_name,
				'maintain_ratio' => TRUE,
				'create_thumb' => TRUE,
				'thumb_marker' => '',
				'width' => 320,
				'height' => 180,
			),
			array(
				'image_library' => 'gd2',
				'source_image' => storagePath.'/picture/origin/'.$file_name,
				'new_image' => storagePath.'/picture/medium/'.$file_name,
				'maintain_ratio' => TRUE,
				'create_thumb' => TRUE,
				'thumb_marker' => '',
				'width' => 480,
				'height' => 360,
			),
			array(
				'image_library' => 'gd2',
				'source_image' => storagePath.'/picture/origin/'.$file_name,
				'new_image' => storagePath.'/picture/high/'.$file_name,
				'maintain_ratio' => TRUE,
				'create_thumb' => TRUE,
				'thumb_marker' => '',
				'width' => 1280,
				'height' => 720,
			),
		);

		$this->CI->load->library('image_lib', $config[0]);
		foreach ($config as $item){
			$this->CI->image_lib->initialize($item);
			if(!$this->CI->image_lib->resize()){
				return false;
			}
			$this->CI->image_lib->clear();
		}
	}

	public function format_k_number($number) {
		if($number >= 1000) {
			return $number/1000 . "k";
		}
		else {
			return $number;
		}
	}

	public function thumbnailYoutube($youTubeLink='',$thumbNamilQuality=''){
		$videoIdExploded = explode('?v=', $youTubeLink);   

		if (sizeof($videoIdExploded) == 1) 
		{
			$videoIdExploded = explode('&v=', $youTubeLink);

			$videoIdEnd = end($videoIdExploded);

			$removeOtherInVideoIdExploded = explode('&',$videoIdEnd);

			$youTubeVideoId = current($removeOtherInVideoIdExploded);
		}else{
			$videoIdExploded = explode('?v=', $youTubeLink);

			$videoIdEnd = end($videoIdExploded);

			$removeOtherInVideoIdExploded = explode('&',$videoIdEnd);

			$youTubeVideoId = current($removeOtherInVideoIdExploded);
		}

		switch ($thumbNamilQuality) 
		{
			case 'LOW':
			$imageUrl = 'http://img.youtube.com/vi/'.$youTubeVideoId.'/mqdefault.jpg';
			break;

			case 'MEDIUM':
			$imageUrl = 'http://img.youtube.com/vi/'.$youTubeVideoId.'/hqdefault.jpg';
			break;

			case 'HIGH':
			$imageUrl = 'http://img.youtube.com/vi/'.$youTubeVideoId.'/maxresdefault.jpg';
			break;

			default:
			return  'Choose The Quality Between [ LOW (or) MEDIUM  (or) HIGH  (or)  MAXIMUM]';
			break;
		}
		return $imageUrl;
	}

	public function count_news($channel='',$status='')
	{
		$uid = $this->CI->session->userdata('user_id');
		$group = $this->CI->ion_auth->get_users_groups($uid)->row()->id;

		if ($group == 1 || $group == 2 || $group == 3) {

		}else{
			$this->CI->db->where('author_id',$uid);
		}

		if (!empty($channel)) {
			$this->CI->db->where('channel',$channel);
		}

		if ($status == 'published') {
			$this->CI->db->where('news.status','published');
			$this->CI->db->where('news.published_at <=',date('Y-m-d H:i:s'));
		}elseif ($status =='scheduled') {
			$this->CI->db->where('news.status','published');
			$this->CI->db->where('news.published_at >',date('Y-m-d H:i:s'));
		}else{
			$this->CI->db->where('status',$status);
		}

		$this->CI->db->from('news');
		return $this->CI->db->count_all_results();
	}

	public function count_news_graph_day($channel)
	{
		$dateNow = date('Y-m-d H:i:s');

		// for get only this week
		$sabtuMingguIni = date("Y-m-d H:i:s", strtotime('saturday this week'));
		$startDate = date('Y-m-d H:i:s', strtotime('-6 days',strtotime($sabtuMingguIni)));
		$endDate = $sabtuMingguIni;

		$data['draft'] = $this->CI->db->query('SELECT DAYOFWEEK(DATE(published_at)), COUNT(published_at) FROM news WHERE status = "draft" AND published_at BETWEEN "'.$startDate.'" AND "'.$endDate.'" GROUP BY DAYOFWEEK(DATE(published_at))')->result_array();

		$data['published'] = $this->CI->db->query('SELECT DAYOFWEEK(DATE(published_at)), COUNT(published_at) FROM news WHERE status = "published" AND published_at <= "'.$dateNow.'" AND published_at BETWEEN "'.$startDate.'" AND "'.$endDate.'" GROUP BY DAYOFWEEK(DATE(published_at))')->result_array();
		
		$data['scheduled'] = $this->CI->db->query('SELECT DAYOFWEEK(DATE(published_at)), COUNT(published_at) FROM news WHERE status = "published" AND published_at > "'.$dateNow.'" AND published_at BETWEEN "'.$startDate.'" AND "'.$endDate.'" GROUP BY DAYOFWEEK(DATE(published_at))')->result_array();

		$data['newsPhoto'] = $this->CI->db->query('SELECT DAYOFWEEK(DATE(published_at)), COUNT(published_at) FROM news_pictures WHERE status = "published" AND published_at <= "'.$dateNow.'" AND published_at BETWEEN "'.$startDate.'" AND "'.$endDate.'" GROUP BY DAYOFWEEK(DATE(published_at))')->result_array();
		$data['newsVideos'] = $this->CI->db->query('SELECT DAYOFWEEK(DATE(published_at)), COUNT(published_at) FROM news_videos WHERE status = "published" AND published_at <= "'.$dateNow.'" AND published_at BETWEEN "'.$startDate.'" AND "'.$endDate.'" GROUP BY DAYOFWEEK(DATE(published_at))')->result_array();

		$draft = [];
		$published = [];
		$scheduled = [];

		$newsPhoto = [];
		$newsVideos = [];

		$d = 1;
		for ($i=0; $i < 7; $i++) { 
			$hari[$d++] = 0;
		}

		foreach ($data['draft'] as $v) {
			$draft[$v['DAYOFWEEK(DATE(published_at))']] = (int)$v['COUNT(published_at)'];
		}

		foreach ($data['published'] as $v) {
			$published[$v['DAYOFWEEK(DATE(published_at))']] = (int)$v['COUNT(published_at)'];
		}

		foreach ($data['scheduled'] as $v) {
			$scheduled[$v['DAYOFWEEK(DATE(published_at))']] = (int)$v['COUNT(published_at)'];
		}

		foreach ($data['newsPhoto'] as $v) {
			$newsPhoto[$v['DAYOFWEEK(DATE(published_at))']] = (int)$v['COUNT(published_at)'];
		}

		foreach ($data['newsVideos'] as $v) {
			$newsVideos[$v['DAYOFWEEK(DATE(published_at))']] = (int)$v['COUNT(published_at)'];
		}

		$dp = array_replace_recursive($hari, $published);
		$ds = array_replace_recursive($hari, $scheduled);
		$dd = array_replace_recursive($hari, $draft);

		$dnp = array_replace_recursive($hari, $newsPhoto);
		$dnv = array_replace_recursive($hari, $newsVideos);

		return ['dd'=>$dd,'dp'=>$dp,'ds'=>$ds,'dnp'=>$dnp,'dnv'=>$dnv];
	}

	public function count_news_graph_week($channel)
	{
		$dateNow = date('Y-m-d H:i:s');

		// for get only this week
		$firstDay = date('Y-m-01');
		$lastDay = date('Y-m-t');

		$draft = [];
		$published = [];
		$scheduled = [];

		$newsPhoto = [];
		$newsVideos = [];

		$data['draft'] = $this->CI->db->query('SELECT count("published_at") as total, DATE_FORMAT(published_at, "%Y-%m-%d") as `date` from news WHERE status = "draft" AND published_at BETWEEN "'.$firstDay.'" AND "'.$lastDay.'" group by yearweek(published_at)')->result_array();
		$data['published'] = $this->CI->db->query('SELECT count("published_at") as total, DATE_FORMAT(published_at, "%Y-%m-%d") as `date` from news WHERE status = "published" AND published_at <= "'.$dateNow.'" AND published_at BETWEEN "'.$firstDay.'" AND "'.$lastDay.'" group by yearweek(published_at)')->result_array();
		$data['scheduled'] = $this->CI->db->query('SELECT count("published_at") as total, DATE_FORMAT(published_at, "%Y-%m-%d") as `date` from news WHERE status = "published" AND published_at > "'.$dateNow.'" AND published_at BETWEEN "'.$firstDay.'" AND "'.$lastDay.'" group by yearweek(published_at)')->result_array();
		$data['newsPhoto'] = $this->CI->db->query('SELECT count("published_at") as total, DATE_FORMAT(published_at, "%Y-%m-%d") as `date` from news_pictures WHERE status = "published" AND published_at <= "'.$dateNow.'" AND published_at BETWEEN "'.$firstDay.'" AND "'.$lastDay.'" group by yearweek(published_at)')->result_array();
		$data['newsVideos'] = $this->CI->db->query('SELECT count("published_at") as total, DATE_FORMAT(published_at, "%Y-%m-%d") as `date` from news_videos WHERE status = "published" AND published_at <= "'.$dateNow.'" AND published_at BETWEEN "'.$firstDay.'" AND "'.$lastDay.'" group by yearweek(published_at)')->result_array();

		foreach ($data['draft'] as $dd) {
			$draft[$this->weekNumberOfMonth($dd['date'])] = $dd['total'];
		}
		foreach ($data['published'] as $dp) {
			$published[$this->weekNumberOfMonth($dp['date'])] = $dp['total'];
		}
		foreach ($data['scheduled'] as $ds) {
			$scheduled[$this->weekNumberOfMonth($ds['date'])] = $ds['total'];
		}
		foreach ($data['newsPhoto'] as $dnp) {
			$newsPhoto[$this->weekNumberOfMonth($dnp['date'])] = $dnp['total'];
		}
		foreach ($data['newsVideos'] as $dnv) {
			$newsVideos[$this->weekNumberOfMonth($dnv['date'])] = $dnv['total'];
		}

		$d = 1;
		for ($i=0; $i < 5; $i++) { 
			$minggu[$d++] = 0;
		}

		$wd = array_replace_recursive($minggu, $draft);
		$wp = array_replace_recursive($minggu, $published);
		$ws = array_replace_recursive($minggu, $scheduled);

		$wnp = array_replace_recursive($minggu, $newsPhoto);
		$wnv = array_replace_recursive($minggu, $newsVideos);

		// return $firstDay;
		return ['wd'=>$wd,'wp'=>$wp,'ws'=>$ws,'wnp'=>$wnp,'wnv'=>$wnv];
	}

	public function count_news_graph_month($channel)
	{
		$dateNow = date('Y-m-d H:i:s');

		// for get only this Month
		$firstYear = date('Y-01-01');
		$endYear = date('Y-12-t');

		$draft = [];
		$published = [];
		$scheduled = [];

		$newsPhoto = [];
		$newsVideos = [];

		$data['draft'] = $this->CI->db->query('SELECT count(published_at) as jumlah, DATE_FORMAT(published_at, "%Y-%m-01") as bulan from news WHERE status = "draft" AND published_at BETWEEN "'.$firstYear.'" AND "'.$endYear.'" group by DATE_FORMAT(published_at, "%Y-%m-01")')->result_array();
		$data['published'] = $this->CI->db->query('SELECT count(published_at) as jumlah, DATE_FORMAT(published_at, "%Y-%m-01") as bulan from news WHERE status = "published" AND published_at <= "'.$dateNow.'" AND published_at BETWEEN "'.$firstYear.'" AND "'.$endYear.'" group by DATE_FORMAT(published_at, "%Y-%m-01")')->result_array();
		$data['scheduled'] = $this->CI->db->query('SELECT count(published_at) as jumlah, DATE_FORMAT(published_at, "%Y-%m-01") as bulan from news WHERE status = "published" AND published_at > "'.$dateNow.'" AND published_at BETWEEN "'.$firstYear.'" AND "'.$endYear.'" group by DATE_FORMAT(published_at, "%Y-%m-01")')->result_array();
		$data['newsPhoto'] = $this->CI->db->query('SELECT count(published_at) as jumlah, DATE_FORMAT(published_at, "%Y-%m-01") as bulan from news_pictures WHERE status = "published" AND published_at <= "'.$dateNow.'" AND published_at BETWEEN "'.$firstYear.'" AND "'.$endYear.'" group by DATE_FORMAT(published_at, "%Y-%m-01")')->result_array();
		$data['newsVideos'] = $this->CI->db->query('SELECT count(published_at) as jumlah, DATE_FORMAT(published_at, "%Y-%m-01") as bulan from news_videos WHERE status = "published" AND published_at <= "'.$dateNow.'" AND published_at BETWEEN "'.$firstYear.'" AND "'.$endYear.'" group by DATE_FORMAT(published_at, "%Y-%m-01")')->result_array();

		foreach ($data['draft'] as $md) {
			$draft[date('m',strtotime($md['bulan']))] = $md['jumlah'];
		}

		foreach ($data['published'] as $mp) {
			$published[date('m',strtotime($mp['bulan']))] = $mp['jumlah'];
		}

		foreach ($data['scheduled'] as $ms) {
			$scheduled[date('m',strtotime($ms['bulan']))] = $ms['jumlah'];
		}

		foreach ($data['newsPhoto'] as $mnp) {
			$newsPhoto[date('m',strtotime($mnp['bulan']))] = $mnp['jumlah'];
		}
		foreach ($data['newsVideos'] as $mnv) {
			$newsVideos[date('m',strtotime($mnv['bulan']))] = $mnv['jumlah'];
		}

		$d = 01;
		for ($i=0; $i < 12; $i++) { 
			$month[str_pad($d++, 2, '0', STR_PAD_LEFT)]  = 0;
		}

		$md = array_replace_recursive($month, $draft);
		$mp = array_replace_recursive($month, $published);
		$ms = array_replace_recursive($month, $scheduled);

		$mnp = array_replace_recursive($month, $newsPhoto);
		$mnv = array_replace_recursive($month, $newsVideos);

		// return $firstDay;
		return ['md'=>$md,'mp'=>$mp,'ms'=>$ms,'mnp'=>$mnp,'mnv'=>$mnv];
	}

	public function weekNumberOfMonth($date) {
		$tgl=date_parse($date);
		$tanggal =  $tgl['day'];
		$bulan   =  $tgl['month'];
		$tahun   =  $tgl['year'];
		$tanggalAwalBulan = mktime(0, 0, 0, $bulan, 1, $tahun);
		$mingguAwalBulan = (int) date('W', $tanggalAwalBulan);
		$tanggalYangDicari = mktime(0, 0, 0, $bulan, $tanggal, $tahun);
		$mingguTanggalYangDicari = (int) date('W', $tanggalYangDicari);
		$mingguKe = $mingguTanggalYangDicari - $mingguAwalBulan + 1;
		return $mingguKe;

	}

}