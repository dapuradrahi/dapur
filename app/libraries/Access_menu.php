<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access_menu {
	protected $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library(['ion_auth']);
	}
	
	private function menu()
	{
		$menu = array(
			array(
				'name' => 'dashboard',
				'alias' => 'Dashboard',
				'url' => backendUrl.'dashboard',
				'class' => '',
				'icon' => 'nav-icon fas fa-tachometer-alt',
				'access' => ['read'],
			),
			array(
				'name' => 'news',
				'alias' => 'News',
				'url' => backendUrl.'posts/news',
				'class' => '',
				'icon' => 'nav-icon fas fa-newspaper',
				'access' => ['view'],
				'children' => array(
					array(
						'name' => 'create',
						'alias' => 'Create Article',
						'url' => backendUrl.'news/create/add',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
					array(
						'name' => 'draft',
						'alias' => 'Draft',
						'url' => backendUrl.'news/draft/view',
						'class' => '',
						'icon' => '',
						'access' => ['read','update','delete'],
					),
					array(
						'name' => 'published',
						'alias' => 'Published',
						'url' => backendUrl.'news/published/view',
						'class' => '',
						'icon' => '',
						'access' => ['read','update','delete'],
					),
					array(
						'name' => 'scheduled',
						'alias' => 'Scheduled',
						'url' => backendUrl.'news/scheduled/view',
						'class' => '',
						'icon' => '',
						'access' => ['read','update','delete'],
					),
					array(
						'name' => 'trash',
						'alias' => 'Trash',
						'url' => backendUrl.'news/trash/view',
						'class' => '',
						'icon' => '',
						'access' => ['read','update','delete'],
					),
				),
			),
			array(
				'name' => 'galleries',
				'alias' => 'Galleries',
				'url' => backendUrl.'galleries/photos',
				'class' => '',
				'icon' => 'nav-icon fas fa-file-image',
				'access' => ['read'],
				'children' => array(
					array(
						'name' => 'photos',
						'alias' => 'Photos',
						'url' => backendUrl.'galleries/photos/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
					array(
						'name' => 'videos',
						'alias' => 'Videos',
						'url' => backendUrl.'galleries/videos/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					)
				),
			),
			array(
				'name' => 'managements',
				'alias' => 'Managements',
				'url' => '',
				'class' => '',
				'icon' => 'nav-icon fas fa-toolbox',
				'access' => ['view'],
				'children' => array(
					array(
						'name' => 'headline',
						'alias' => 'Headline',
						'url' => backend.'managements/headline/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
					array(
						'name' => 'pilihan',
						'alias' => 'Pilihan Editor',
						'url' => backend.'managements/pilihan/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
					array(
						'name' => 'topic',
						'alias' => 'Topik Khusus',
						'url' => backend.'managements/topic/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
					array(
						'name' => 'pages',
						'alias' => 'Static Page',
						'url' => backend.'managements/pages/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
				),
			),
			array(
				'name' => 'filemanager',
				'alias' => 'Filemanager',
				'url' => '',
				'class' => '',
				'icon' => 'nav-icon fas fa-folder-open',
				'access' => ['view'],
				'children' => array(
					array(
						'name' => 'filemanagerPhotos',
						'alias' => 'Photos',
						'url' => backendUrl.'filemanager/filemanagerPhotos/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
					array(
						'name' => 'filemanagerVideos',
						'alias' => 'Videos',
						'url' => backendUrl.'filemanager/filemanagerVideos/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
				),
			),
			array(
				'name' => 'report',
				'alias' => 'Report',
				'url' => backendUrl.'report/view',
				'class' =>'',
				'icon' => 'nav-icon fas fa-book-open',
				'access' => ['create','read','update','delete'],
			),
			array(
				'name' => 'settings',
				'alias' => 'Settings',
				'url' => backendUrl.'settings/users',
				'class' => '',
				'icon' => 'nav-icon fas fa-cogs',
				'access' => ['create','read','update','delete'],
				'children' => array(
					array(
						'name' => 'users',
						'alias' => 'Users',
						'url' => backendUrl.'settings/users',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
					array(
						'name' => 'groups',
						'alias' => 'Groups',
						'url' => backendUrl.'settings/groups',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
					array(
						'name' => 'rubrik',
						'alias' => 'Rubrik',
						'url' => backendUrl.'settings/rubrik/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
					array(
						'name' => 'tags',
						'alias' => 'Tags',
						'url' => backendUrl.'settings/tags/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
					array(
						'name' => 'menus',
						'alias' => 'Menu Frontend',
						'url' => backendUrl.'settings/menus/view',
						'class' => '',
						'icon' => '',
						'access' => ['create','read','update','delete'],
					),
				),
			),
		);

		return $menu;
	}

	public function module_access()
	{
		$menu = $this->menu();
		foreach ($menu as $k => $v) {
			if (isset($v['children'])) {
				foreach ($v['children'] as $child => $vChild) {
					$module[] = array('parent'=>$v['name'],'name'=>$vChild['name'],'access'=>$vChild['access']);
					$parent[] = $v['name'];
				}
			}else{
				$module[] = array('name'=>$v['name'],'access'=>$v['access']);
			}
		}
		return array('module'=>$module,'parent'=>array_unique($parent));
	}

	public function access_menu($uid = '')
	{
		$user_group = $this->CI->ion_auth->get_users_groups($uid)->result_array();
		$q = $this->CI->db->get_where('access', array('group_id' => $user_group[0]['id']));

		$menu = $this->menu();

		if ($uid == '') {
			return array('menu'=>$menu,'mainMenu'=>array(),'subMenu'=>array());
		}

		if ($q->num_rows() < 0) {
			return array('menu'=>$menu,'mainMenu'=>array(),'subMenu'=>array());
		}

		$q = $q->result_array();
		$access = explode('|', $q[0]['access']);

		foreach ($access as $k => $v) {
			$main[] = explode('-', $v);
		}

		$s=array();
		$m=array();

		foreach ($main as $key => $value) {
			if (isset($value[2])) {
				$s[] = $value[1];
			}
			$m[] = $value[0];
		}

		$mainMenu = array_unique($m);
		$subMenu = array_unique($s);

		return array('menu'=>$menu,'mainMenu'=>$mainMenu,'subMenu'=>$subMenu);
	}

	public function access_group($groupId){
		$ress = $this->CI->db->get_where('access', array('group_id' => $groupId));

		$menu = $this->menu();

		if ($ress->num_rows() < 0) {
			return array();
		}

		$q = $ress->result_array();
		$access = explode('|', $q[0]['access']);
		foreach ($access as $k => $v) {
			$a[] = explode('-', $v);
		}

		foreach ($a as $key => $value) {
			if (isset($value[2])) {
				$m[] = array('name'=>$value[0],'children'=>array('name'=>$value[1],'access'=>$value[2]));
				$acc[] = array($value[1]=>$value[2]);
			}else{
				$acc[] = array($value[0]=>$value[1]);
			}
		}

		$collection = array_merge_recursive(...$acc);
		return $collection;
	}

	public function access($mAccess,$uid){
		$user_group = $this->CI->ion_auth->get_users_groups($uid)->result_array();
		if (!$user_group) {
			return FALSE;
		}
		$q = $this->CI->db->get_where('access', array('group_id' => $user_group[0]['id']));
		if ($q->num_rows() < 0) {
			return FALSE;
		}else{
			$q = $q->result_array();
			$access = explode('|', $q[0]['access']);
		}

		if (in_array($mAccess, $access)) {
			return TRUE;
		}else{
			return FALSE;
		}
		// return $coll;
	}
}