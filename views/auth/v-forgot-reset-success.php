<div class="login-box">
	<div class="card shadow">
		<div class="card-header text-info" style="background: url('<?= assets;?>/img/login-bg.png') center center;">
			<h3 class="card-title"><b>Success</b></h3>
		</div>
		<div class="card-body login-card-body">
			<p>Your password has been changed successfully</p>
		</div>
		<div class="card-footer text-center">
			<a href="<?= base_url('auth/login');?>" class="text-info">Login</a>
		</div>
	</div>
</div>
