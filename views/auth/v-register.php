<div class="login-box">
	<div class="card shadow">
		<div class="card-header text-info" style="background: url('<?= assets;?>/img/login-bg.png') center center;">
			<h3 class="card-title"><b>Register</b></h3>
		</div>
		<div class="card-body login-card-body">
			<?= form_open('auth/proccess-login','role="form" id="loginForm"');?>
			<div class="input-group mb-3">
				<input type="text" name="identity" value="<?= set_value('identity');?>" class="form-control form-control-sm" placeholder="Username" autocomplete="off" autofocus>
				<div class="input-group-append">
					<div class="input-group-text">
						<span class="fas fa-envelope"></span>
					</div>
				</div>
			</div>
			<div class="input-group mb-3">
				<input type="password" name="password" class="form-control form-control-sm" placeholder="Password">
				<div class="input-group-append">
					<div class="input-group-text">
						<span class="fas fa-lock"></span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-8">
					<div class="icheck-primary">
						<input type="checkbox" id="remember" name="remember" value="1">
						<label for="remember">
							Remember Me
						</label>
					</div>
				</div>
				<div class="col-4">
					<button id="btnLoginForm" class="btn btn-root btn-block btn-flat btn-sm">Login</button>
				</div>
			</div>
			<?= form_close();?>
		</div>
		<div class="card-footer text-center">
			<a href="<?= base_url('forgot');?>" class="text-info">Forgot password</a>
		</div>
	</div>
</div>