<div class="login-box">
	<div class="card shadow">
		<div class="card-header text-info" style="background: url('<?= assets;?>/img/login-bg.png') center center;">
			<h3 class="card-title"><b>Forgot</b></h3>
		</div>
		<div class="card-body login-card-body">
			<?= form_open('auth/proccess-login','role="form" id="loginForm"');?>
			<div class="input-group mb-3">
				<input type="email" name="email" value="<?= set_value('email');?>" class="form-control form-control-sm" placeholder="Email" autocomplete="off" autofocus>
				<div class="input-group-append">
					<div class="input-group-text">
						<span class="fas fa-envelope"></span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-7">
					
				</div>
				<div class="col-5">
					<button id="btnLoginForm" class="btn btn-root btn-block btn-flat btn-sm">Reset Password</button>
				</div>
			</div>
			<?= form_close();?>
		</div>
		<div class="card-footer text-center">
			<a href="<?= base_url('auth/login');?>" class="text-info">Login</a>
		</div>
	</div>
</div>
