<div class="row">
	<div class="col-lg-3 col-6">
		<div class="small-box bg-info">
			<div class="inner">
				<h3><?= $this->apps->count_news(1,'draft');?></h3>
				<p>Draft</p>
			</div>
			<div class="icon">
				<i class="fas fa-clone"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-6">
		<div class="small-box bg-warning">
			<div class="inner">
				<h3><?= $this->apps->count_news(1,'scheduled');?></h3>
				<p>Scheduled</p>
			</div>
			<div class="icon">
				<i class="fas fa-calendar"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-6">
		<div class="small-box bg-success">
			<div class="inner">
				<h3><?= $this->apps->count_news(1,'published');?></h3>
				<p>Published</p>
			</div>
			<div class="icon">
				<i class="fas fa-check"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<div class="col-lg-3 col-6">
		<div class="small-box bg-danger">
			<div class="inner">
				<h3><?= $this->apps->count_news(1,'deleted');?></h3>
				<p>Trash</p>
			</div>
			<div class="icon">
				<i class="fas fa-trash"></i>
			</div>
			<a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
		</div>
	</div>
</div>
<div class="card card-primary card-outline">
	<div class="card-body p-2">
		<canvas id="canvas"></canvas>
	</div>
	<div class="card-footer">
	</div>
</div>