<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD-MM-YYYY, HH:mm:ss',
		});
	});

	$("#general-modal-iframe").on('load',function () {
		LoadingContent('loadingContent','','stop');
		$(this).contents().find(".InsertToEditor").click(function(){
			$('#generalModal').modal('hide');
			var videoId = $(this).attr('data-id');
			var title = $(this).attr('data-title');
			var desc = $(this).attr('data-desc');
			var link = $(this).attr('data-link');
			$('#title').val(title);
			$('#description').val(desc);
			$('#video').val(link);
			addCols('1',link);
			console.log(link);
		});
	});

	function getIdYoutube(url) {
		const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
		const match = url.match(regExp);
		return (match && match[2].length === 11) ? match[2] : null;
	}

	var addCols = function (num,link){
		const videoId = getIdYoutube(link);
		for (var i=1;i<=num;i++) {
			var myCol = $('<div class="col-sm-12"><div class="card iframe_6_9"><iframe class="iframe_resposive" src="https://www.youtube.com/embed/'+videoId+'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div>');
			$('#iframeVideo').html(myCol);
		}
	};

	<?php if (!empty($newsVideo->news_videos_videos)) {?>
		addCols('1','<?= $newsVideo->news_videos_videos;?>');
	<?php }?>

	$('#btnAddFoto').on('click', function(){

		$('#generalModal').modal({backdrop: 'static', keyboard: false});
		$('#generalModal').find('.modal-title').text('Insert Video');
		var url = '<?= backend.'IframeContent/videos';?>';
		$('#general-modal-iframe').attr('src', url);
		LoadingContent('loadingContent','','start');
		return false;
	});

	$('.related').select2({
		allowClear: true,
		placeholder: "**Select a related**",
		minimumInputLength: 2,
		ajax: {
			url: '<?= backend.'JSON_select/related';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term
				}
				return query;
			}
		}
	});

	$('.tags').select2({
		allowClear: true,
		placeholder: "**Type tags**",
		minimumInputLength: 2,
		ajax: {
			url: '<?= backend.'JSON_select/tags';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term
				}
				return query;
			}
		}
	});

	$('#addNewTag').on('click',function(){
		$('#addNewTagsModal').modal({backdrop: 'static', keyboard: false});
	});

	$('#newTagForm').submit(function(e) {
		e.preventDefault();
		LoadingBtn('btnSaveTag','Save','start');
		var data = new FormData(this);
		request = $.ajax({
			type: 'POST',
			url: $('#newTagForm').attr('action'),
			data: data,
			dataType: 'JSON',
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(){
			},
		});
		request.done(function (response, textStatus, jqXHR){
			if (response.status) {
				toastr.success(response.message);
				$("#newTagForm")[0].reset();
				$('#addNewTagsModal').modal('hide');
			}else{
				toastr.error(response.message);
			}
			$('input[name=sypo]').val(response.token);
		});
		request.fail(function (jqXHR, textStatus, errorThrown){
			location.reload();
		});
		request.always(function () {
			LoadingBtn('btnSaveTag','Save','Stop');
		});
	});

	$('.author').select2({
		allowClear: true,
		placeholder: "**Select author**",
		ajax: {
			url: '<?= backend.'JSON_select/author';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'author'
				}
				return query;
			}
		}
	});

	$('.editor').select2({
		allowClear: true,
		placeholder: "**Select editor**",
		ajax: {
			url: '<?= backend.'JSON_select/author';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'editor'
				}
				return query;
			}
		}
	});

	$('.publisher').select2({
		allowClear: true,
		placeholder: "**Select publisher**",
		ajax: {
			url: '<?= backend.'JSON_select/author';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'publisher'
				}
				return query;
			}
		}
	});
</script>