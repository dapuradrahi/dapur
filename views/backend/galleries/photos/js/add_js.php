<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD-MM-YYYY, HH:mm:ss',
		});
	});

	$("#general-modal-iframe").on('load',function () {
		LoadingContent('loadingContent','','stop');
		$(this).contents().find(".InsertToEditor").click(function(){
			$('#generalModal').modal('hide');
			var imageId = $(this).attr('data-id');
			var imageFilename = $(this).attr('data-filename');
			var author = $(this).attr('data-author');
			var source = $(this).attr('data-source');
			var caption = $(this).attr('data-caption');
			addCols('1',imageId,imageFilename,author,source,caption);
		});
	});

	$('#title').keyup(function(){
		$("#title").attr('maxlength','100');
		var str=$('#title').val();
		var left = 100-str.length
		$('#titleLeft').html(left);
	});

	$('#excerpt').keyup(function(){
		$("#excerpt").attr('maxlength','140');
		var str=$('#excerpt').val();
		$('#rangVal').val(str.length);
	});

	var addCols = function (num,imageId,imageFilename,author,source,caption){
		for (var i=1;i<=num;i++) {
			var myCol = $('<div class="col-sm-3"></div>');
			var myPanel = $('<div class="card"><a data-fancybox="gallery" href="<?= storage;?>picture/high/'+imageFilename+'" data-caption="'+caption+'"><img src="<?= storage;?>picture/low/'+imageFilename+'" class="card-img-top"></a><div class="card-header p-2"><h3 class="card-title text-xs text-info"><i class="fa fa-user"></i> '+author.substr(0,13)+'</h3><div class="card-tools"><button type="button" class="btn btn-tool editImageSelected" data-id="'+imageId+'"><i class="fas fa-edit text-info"></i></button><button type="button" class="btn btn-tool deleteImageSelected" data-id="'+imageId+'"><i class="fas fa-trash text-danger"></i></button></div><input type="hidden" name="imageId[]" value="'+imageId+'"><input type="hidden" name="imageFilename[]" value="'+imageFilename+'"><input type="hidden" name="imageCaption[]" value="'+caption+'"><input type="hidden" name="imageSource[]" value="'+source+'"><input type="hidden" name="imageAuthor[]" value="'+author+'"></div>');
			myPanel.appendTo(myCol);
			myCol.appendTo('#ImageSelected');
		}

		$('.deleteImageSelected').on('click', function(e){
			e.stopPropagation();  
			var $target = $(this).parents('.col-sm-3');
			$target.hide('slow', function(){ $target.remove(); });
		});
	};
	$('.deleteImageSelected').on('click', function(e){
		e.stopPropagation();  
		var $target = $(this).parents('.col-sm-3');
		$target.hide('slow', function(){ $target.remove(); });
	});

	$('#btnAddFoto').on('click', function(){

		$('#generalModal').modal({backdrop: 'static', keyboard: false});
		$('#generalModal').find('.modal-title').text('Insert image');
		var url = '<?= backend.'IframeContent/images';?>';
		$('#general-modal-iframe').attr('src', url);
		LoadingContent('loadingContent','','start');
		return false;
	});

	tinymce.init({
		selector: '#mytextarea',
		height : 200,
		toolbar: 'styleselect bold italic alignleft aligncenter alignright | bullist numlist outdent indent link code',
		plugins: 'code lists advlist link image autolink wordcount',
		image_advtab: true ,

		theme: 'modern',
		// mobile: { theme: 'mobile' },
		menubar: false,
		convert_urls: false,

		valid_elements : '+*[*]',

		setup: function (editor) {
			editor.on('init', function (args) {
				editor_id = args.target.id;
			});
		}
	});

	$('.related').select2({
		allowClear: true,
		placeholder: "**Select a related**",
		minimumInputLength: 2,
		ajax: {
			url: '<?= backend.'JSON_select/related';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term
				}
				return query;
			}
		}
	});

	$('.tags').select2({
		allowClear: true,
		placeholder: "**Type tags**",
		minimumInputLength: 2,
		ajax: {
			url: '<?= backend.'JSON_select/tags';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term
				}
				return query;
			}
		}
	});

	$('#addNewTag').on('click',function(){
		$('#addNewTagsModal').modal({backdrop: 'static', keyboard: false});
	});
	$('#newTagForm').submit(function(e) {
		e.preventDefault();
		LoadingBtn('btnSaveTag','Save','start');
		var data = new FormData(this);
		request = $.ajax({
			type: 'POST',
			url: $('#newTagForm').attr('action'),
			data: data,
			dataType: 'JSON',
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(){
			},
		});
		request.done(function (response, textStatus, jqXHR){
			if (response.status) {
				toastr.success(response.message);
				$("#newTagForm")[0].reset();
				$('#addNewTagsModal').modal('hide');
			}else{
				toastr.error(response.message);
			}
			$('input[name=sypo]').val(response.token);
		});
		request.fail(function (jqXHR, textStatus, errorThrown){
			location.reload();
		});
		request.always(function () {
			LoadingBtn('btnSaveTag','Save','Stop');
		});
	});

	$('.author').select2({
		allowClear: true,
		placeholder: "**Select author**",
		ajax: {
			url: '<?= backend.'JSON_select/author';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'author'
				}
				return query;
			}
		}
	});

	$('.editor').select2({
		allowClear: true,
		placeholder: "**Select editor**",
		ajax: {
			url: '<?= backend.'JSON_select/author';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'editor'
				}
				return query;
			}
		}
	});

</script>