<?= form_open(backend.'galleries/photos/update');?>
<div class="card card-warning card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<a href="<?= backend.'galleries/photos/view';?>"><button type="button" class="btn btn-tool btn-danger">Back</button></a>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<div class="input-group input-group">
						<input type="text" class="form-control form-control-sm" value="<?= $newsPicture->news_pictures_title; ?>" id="title" name="title" placeholder="Title" autocomplete="off">
						<span class="input-group-append">
							<button type="button" class="btn btn-info btn-sm btn-flat" id="titleLeft">100</button>
						</span>
					</div>
				</div>
				<div class="form-group text-right">
					<button type="button" class="btn btn-success btn-sm btn-flat" id="btnAddFoto">Add Foto</button>
				</div>
				<div class="form-group">
					<div class="row" id="ImageSelected">
						<?php if (!empty($pictures)) {?>
							<?php foreach ($pictures as $img) {?>
								<div class="col-sm-3">
									<div class="card">
										<a data-fancybox="gallery" href="<?= storage;?>picture/high/<?= $img['file_name'];?>" data-caption="<?= $img['caption'];?>">
											<img src="<?= storage;?>picture/low/<?= $img['file_name'];?>" class="card-img-top">
										</a>
										<div class="card-header p-2">
											<h3 class="card-title text-xs text-info"><i class="fa fa-user"></i> <?= $img['author'];?></h3>
											<div class="card-tools">
												<button type="button" class="btn btn-tool editImageSelected" data-id="<?= $img['image'];?>"><i class="fas fa-edit text-info"></i></button>
												<button type="button" class="btn btn-tool deleteImageSelected" data-id="<?= $img['image'];?>"><i class="fas fa-trash text-danger"></i></button>
											</div>
											<input type="hidden" name="imageId[]" value="<?= $img['image'];?>">
											<input type="hidden" name="imageFilename[]" value="<?= $img['file_name'];?>">
											<input type="hidden" name="imageCaption[]" value="<?= $img['caption'];?>">
											<input type="hidden" name="imageSource[]" value="<?= $img['source'];?>">
											<input type="hidden" name="imageAuthor[]" value="<?= $img['author'];?>">
										</div>
									</div>
								</div>
							<?php }?>
						<?php }?>
					</div>
				</div>
				<div class="form-group">
					<textarea id="mytextarea" name="content"><?= $newsPicture->news_pictures_content; ?></textarea>
				</div>
				<div class="form-group">
					<label>Related</label>
					<select class="form-control form-control related" name="related[]" multiple="multiple">
						<?php foreach ($related as $t => $vt) {?>
							<option value="<?= $vt['news_id'];?>" selected><?= $vt['news_title'];?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<textarea class="form-control" name="excerpt" id="excerpt" placeholder="Deskripsi : 140 characters left" style="height: 90px"><?= $newsPicture->news_pictures_excerpt; ?></textarea>
					<input type="range" min="0" max="140" id="rangVal" value="0" style="width: 100%;" class="progress-bar bg-danger">
				</div>
				<div class="form-group">
					<label>Tags</label>
					<div class="input-group input-group">
						<select class="form-control form-control tags" name="tags[]" multiple="multiple">
							<?php if (!empty($tags)) {
								foreach ($tags->result_array() as $t) {
									echo '<option selected value="'.$t['news_tag_tag_id'].'">'.$t['tag_name'].'</option>';
								}
							}?>
						</select>
						<span class="input-group-append">
							<button type="button" class="btn btn-info btn-sm btn-flat" id="addNewTag">Add</button>
						</span>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="form-group row mb-1">
						<label class="col-sm-3 col-form-label">Editor</label>
						<div class="col-sm-9">
							<select class="form-control form-control editor" name="editor">
								<?php if ($newsPicture->news_pictures_editor_id != NULL) {?>
								<option value="<?= $newsPicture->news_pictures_editor_id;?>" selected><?= $this->ion_auth->user($newsPicture->news_pictures_editor_id)->row()->first_name.' '.$this->ion_auth->user($newsPicture->news_pictures_editor_id)->row()->last_name;?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="form-group row mb-1">
						<label class="col-sm-3 col-form-label">Author</label>
						<div class="col-sm-9">
							<select class="form-control form-control author" name="author">
								<option value="<?= $userLogin['id'];?>" selected><?= $userLogin['first_name'].' '.$userLogin['last_name'];?></option>
							</select>
						</div>
					</div>
					<div class="form-group row mb-1">
						<label class="col-sm-3 col-form-label">Publisher</label>
						<div class="col-sm-9">
							<select class="form-control form-control publisher" name="publisher">
								<option value="<?= $userLogin['id'];?>" selected><?= $userLogin['first_name'].' '.$userLogin['last_name'];?></option>
							</select>
						</div>
					</div>
				</div>
				<br>
				<div class="form-group">
					<label>Schedule</label>
					<div class="form-group">
						<div class="input-group date" id="datetimepicker1" data-target-input="nearest">
							<input type="text" name="schedule" class="form-control form-control-sm datetimepicker-input" data-target="#datetimepicker1" value="<?= date('d-m-Y H:i:s',strtotime($newsPicture->news_pictures_published_at)); ?>">
							<div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
								<div class="input-group-text"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer d-flex justify-content-center p-1">
		<input type="hidden" name="newsPictureId" value="<?= $newsPicture->news_picture_id;?>">
		<input type="hidden" name="hash" value="<?= $newsPicture->news_pictures_hash;?>">
		<input type="submit" value="publish" name="status" class="btn btn-success btn-flat btn-sm">
		<input type="submit" value="save" name="status" id="btnAddUser" class="btn btn-info btn-flat btn-sm">
		<a href="<?= backend.'';?>"><button type="button" class="btn btn-danger btn-flat btn-sm">cancel</button></a>
	</div>
</div>
<?= form_close();?>

<div class="modal fade" id="generalModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-scrollable" role="document" style="height: 900px">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Baca Juga</h6>
			</div>
			<div class="modal-body p-0">
				<div class="form-group">
					<div class="fileIframe">
						<section class="contentIframe">
							<div id="loadingContent"></div>
							<iframe id="general-modal-iframe" src="" allowfullscreen><div class="d-flex justify-content-center">
								<div class="spinner-grow" id="loader1" style="width: 5rem; height: 5rem;" role="status">
									<span class="sr-only">Loading...</span>
								</div>
							</div></iframe>
						</section>
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="addNewTagsModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Add new tag</h6>
			</div>
			<?= form_open(backend.'tags/create/save','id="newTagForm"');?>
			<div class="modal-body">
				<div class="form-group">
					<input type="text" name="newTag" class="form-control form-control-sm" placeholder="Type new tag">
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Close</button>
				<button type="submit" class="btn btn-info btn-flat btn-sm" id="btnSaveTag">save</button>
			</div>
			<?= form_close();?>
		</div>
	</div>
</div>