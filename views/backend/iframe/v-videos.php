<div class="card card-info card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?> | <span class="badge badge-info"><?= $total_rows;?></span> Images</h3>
		<div class="card-tools">
			<a href="" title="Reload"><button type="button" class="btn btn-tool"><i class="fas fa-sync-alt"></i></button></a>
			<button type="button" id="addVideos" class="btn btn-tool btn-info" <?= ($this->access_menu->access('filemanager-filemanagerVideos-create',$this->session->userdata('user_id')) ? '':'disabled');?>>Add Video</button>
		</div>
	</div>
	<div class="card-body">
		<?= form_open('','method="GET"');?>
		<div class="row">
			<div class="col-sm-2">
				<div class="form-group">
					<select class="form-control form-control-sm" name="status">
						<option value="">Status</option>
						<option value="">All</option>
						<option value="1" <?=($this->input->get('status') == '1' ? 'selected':'');?>>Publish</option>
						<option value="0" <?=($this->input->get('status') == '0' ? 'selected':'');?>>Hide</option>
					</select>
				</div>
			</div>
			<div class="col-sm-1">
				<div class="form-group">
					<div class="input-group">
						<input type="text" class="form-control form-control-sm" id="dateRang" name="dateRange" placeholder="Date" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<div class="input-group input-group">
						<input type="text" class="form-control form-control-sm" id="keywords" value="<?=$this->input->get('keywords');?>" name="keywords" placeholder="Keywords">
						<span class="input-group-append">
							<button type="submit" class="btn btn-info btn-sm btn-flat">Search</button>
						</span>
						<?= (count($this->input->get()) > 1 ? '<a href="'.backend.'IframeContent/videos'.'"><span class="input-group-append"><button type="button" class="btn btn-warning btn-sm btn-flat">Clear</button></span></a>':'');?>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<?php echo $pagination; ?>
			</div>
		</div>
		<?= form_close();?>
		<div class="row">
			<?php foreach ($videos->result() as $row) {?>
				<div class="col-sm-3">
					<div class="card">
						<a data-fancybox="gallery" href="<?= $row->link;?>" data-caption="<?= 'By : '.$row->first_name. ' '.$row->last_name. ' | Title : '.$row->title;?>"><img src="<?= $this->apps->thumbnailYoutube($row->link,'LOW');?>" class="card-img-top"></a>
						<div class="card-header p-2">
							<h3 class="card-title text-sm text-info"><i class="fa fa-user"></i> <?= substr($row->first_name.' '.$row->last_name, 0,17);?></h3>
							<div class="card-tools">
								<button type="button" title="Insert to News" class="btn btn-tool InsertToEditor" data-title="<?= $row->title ;?>" data-desc="<?= $row->description ;?>" data-id="<?= $row->videos_id ;?>" data-link="<?= $row->link ;?>" <?= ($this->access_menu->access('filemanager-filemanagerVideos-read',$this->session->userdata('user_id')) ? '':'disabled') ;?>><i class="fas fa-check-circle text-info"></i></button>
								<button type="button" class="btn btn-tool editVideo" data-id="<?= $row->videos_id ;?>" <?= ($this->access_menu->access('filemanager-filemanagerVideos-update',$this->session->userdata('user_id')) ? '':'disabled') ;?>><i class="fas fa-edit text-info"></i></button>
								<button type="button" class="btn btn-tool deleteVideo" data-id="<?= $row->videos_id ;?>" <?= ($this->access_menu->access('filemanager-filemanagerVideos-delete',$this->session->userdata('user_id')) ? '':'disabled') ;?>><i class="fas fa-trash text-red"></i></button>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<div class="modal fade" id="addVideosModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Add Videos</h6>
			</div>
			<?= form_open_multipart('root-cms/filemanager/filemanagerVideos/save','id="addVideosForm" method="post"');?>
			<div class="modal-body">
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Link Youtube</label>
					<div class="col-sm-9">
						<input type="text" name="link" id="link" class="form-control form-control-sm" placeholder="Link Youtube">
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Title</label>
					<div class="col-sm-9">
						<input type="text" name="title" class="form-control form-control-sm" placeholder="Title">
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Description</label>
					<div class="col-sm-9">
						<textarea class="form-control" name="description"></textarea>
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Source</label>
					<div class="col-sm-9">
						<input type="text" name="source" class="form-control form-control-sm" placeholder="Source">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Status</label>
					<div class="col-sm-9 mt-1">
						<div class="icheck-primary d-inline">
							<input type="radio" id="statusActive" value="1" name="status" checked>
							<label for="statusActive">Publish</label>
						</div>
						<div class="icheck-danger d-inline">
							<input type="radio" id="statusInActive" value="0" name="status">
							<label for="statusInActive">Hide</label>
						</div>
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Thumbnail</label>
					<div class="col-sm-9" id="previewThumb">
						<img src="<?= assets.'img/youtubeThumb.png';?>" class="img-fluid" alt="Responsive image">
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Cancel</button>
				<button type="submit" id="btnAddVideo" class="btn btn-info btn-flat btn-sm">Save</button>
			</div>
			<?= form_close();?>
		</div>
	</div>
</div>