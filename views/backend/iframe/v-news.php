<div class="card card-info card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?> | <span class="badge badge-info"><?= $total_rows;?></span> Images</h3>
		<div class="card-tools">
			<a href="" title="Reload"><button type="button" class="btn btn-tool"><i class="fas fa-sync-alt"></i></button></a>
		</div>
	</div>
	<div class="card-body">
		<?= form_open('','method="GET"');?>
		<div class="row">
			<div class="col-sm-2">
				<div class="form-group">
					<select class="form-control form-control-sm" name="status">
						<option value="">Status</option>
						<option value="">All</option>
						<option value="1" <?=($this->input->get('status') == '1' ? 'selected':'');?>>Publish</option>
						<option value="0" <?=($this->input->get('status') == '0' ? 'selected':'');?>>Hide</option>
					</select>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<div class="input-group">
						<input type="text" class="form-control form-control-sm" id="dateRang" name="dateRange" placeholder="Date" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<div class="input-group input-group">
						<input type="text" class="form-control form-control-sm" id="keywords" value="<?=$this->input->get('keywords');?>" name="keywords" placeholder="Keywords">
						<span class="input-group-append">
							<button type="submit" class="btn btn-info btn-sm btn-flat">Search</button>
						</span>
						<?= (count($this->input->get()) > 1 ? '<a href="'.backend.'IframeContent/newsList'.'"><span class="input-group-append"><button type="button" class="btn btn-warning btn-sm btn-flat">Clear</button></span></a>':'');?>
					</div>
				</div>
			</div>
			<div class="col-sm-5">
				<?php echo $pagination; ?>
			</div>
		</div>
		<?= form_close();?>

		<table class="table-sm table-striped" style="width:100%">
			<thead>
				<tr class="text-center text-white" style="background-color: #343a40">
					<th data-priority="1">Title</th>
					<th>Rubrik</th>
					<th><i class="fa fa-calendar text-info"></i></th>
					<th style="width: 15px">Action</th> 
				</tr>
			</thead>
			<tbody>
				<?php foreach ($newsList->result() as $k => $v) {?>
					<?php preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $v->news_content, $image);?>
					<tr>
						<td><?= $v->news_title ;?> <a href="<?= frontPage.$v->category_slug.'/'.$v->news_slug.'-'.$v->hash;?>" target="_blink" type="button" class="btn btn-info btn-xs"><i class="fas fa-angle-double-right" title="Preview"></i></a></td>
						<td><?= $v->category_name ;?></td>
						<td style="text-align: center;"><?= date('d-m-Y H:i:s', strtotime($v->news_published_at));?></td>
						<td>
							<button type="button" class="btn btn-sm btn-success btn-flat insertNews" data-id="<?= $v->news_id;?>" data-title="<?= $v->news_title;?>" data-image="<?= (!empty($image[1])) ? $image[1]: storage.'no-image.png';?>" data-author="<?= $v->author_id;?>" data-link="<?= $v->category_slug.'/'.$v->news_slug.'-'.$v->hash;?>">Insert</button>
						</td>
					</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</div>