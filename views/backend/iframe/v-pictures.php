<div class="card card-info card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?> | <span class="badge badge-info"><?= $total_rows;?></span> Images</h3>
		<div class="card-tools">
			<a href="" title="Reload"><button type="button" class="btn btn-tool"><i class="fas fa-sync-alt"></i></button></a>
			<button type="button" id="addPictures" class="btn btn-tool btn-info" <?= ($this->access_menu->access('filemanager-filemanagerPhotos-create',$this->session->userdata('user_id')) ? '':'disabled');?>>Add Image</button>
		</div>
	</div>
	<div class="card-body">
		<?= form_open('','method="GET"');?>
		<div class="row">
			<!-- <div class="col-sm-2">
				<div class="form-group">
					<select class="form-control form-control-sm" name="channel">
						<option value="" disabled selected>Channel</option>
						<option value="">All</option>
						<option value="1" <?=($this->input->get('channel') == 1 ? 'selected':'');?>></option>
					</select>
				</div>
			</div> -->
			<div class="col-sm-2">
				<div class="form-group">
					<select class="form-control form-control-sm" name="type">
						<option value="" disabled selected>Type</option>
						<option value="">All</option>
						<option value="ilustration" <?=($this->input->get('type') == 'ilustration' ? 'selected':'');?>>Ilustration</option>
						<option value="graphic-design" <?=($this->input->get('type') == 'graphic-design' ? 'selected':'');?>>Graphic design</option>
						<option value="photography" <?=($this->input->get('type') == 'photography' ? 'selected':'');?>>Photography</option>
					</select>
				</div>
			</div>
			<div class="col-sm-1">
				<div class="form-group">
					<div class="input-group">
						<input type="text" class="form-control form-control-sm" id="dateRang" name="dateRange" placeholder="Date" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<div class="input-group input-group">
						<input type="text" class="form-control form-control-sm" id="keywords" value="<?=$this->input->get('keywords');?>" name="keywords" placeholder="Keywords">
						<span class="input-group-append">
							<button type="submit" class="btn btn-info btn-sm btn-flat">Search</button>
						</span>
						<?= (count($this->input->get()) > 1 ? '<a href="'.backend.'IframeContent/images'.'"><span class="input-group-append"><button type="button" class="btn btn-warning btn-sm btn-flat">Clear</button></span></a>':'');?>
					</div>
				</div>
			</div>
			<div class="col-sm-4 float-right">
				<?php echo $pagination; ?>
			</div>
		</div>
		<?= form_close();?>
		<div class="row" id="imageSelected">
			<?php foreach ($pictures->result() as $row) {?>
				<div class="col-sm-3">
					<div class="card">
						<a data-fancybox="gallery" href="<?= storage.'picture/high/'.$row->file_name;?>" data-caption="<?= 'By : '.$row->first_name. ' '.$row->last_name. ' | Title : '.$row->title.' | Type : '.$row->type;?>"><img src="<?= storage.'picture/low/'.$row->file_name;?>" class="card-img-top"></a>
						<div class="card-header p-2">
							<h3 class="card-title text-sm text-info"><i class="fa fa-user"></i> <?= substr($row->first_name, 0,20);?></h3>
							<div class="card-tools">
								<button type="button" title="Insert to News" class="btn btn-tool InsertToEditor" data-author="<?= $row->first_name.' '.$row->last_name;?>" data-source="<?= $row->source;?>" data-caption="<?= $row->caption;?>" data-id="<?= $row->picture_id;?>" data-filename="<?= $row->file_name;?>" data-title="<?= $row->title;?>" <?= ($this->access_menu->access('filemanager-filemanagerPhotos-read',$this->session->userdata('user_id')) ? '':'disabled') ;?>><i class="fas fa-check-circle text-info"></i></button>
								<a href="<?= backend.'IframeContent/images/edit/'.$row->picture_id;?>">
									<button type="button" class="btn btn-tool" data-id="<?= $row->picture_id ;?>" <?= ($this->access_menu->access('filemanager-filemanagerPhotos-update',$this->session->userdata('user_id')) ? '':'disabled') ;?>><i class="fas fa-edit text-info"></i></button>
								</a>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<div class="modal fade" id="addPicturesModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Add Images</h6>
			</div>
			<?= form_open_multipart('root-cms/filemanager/filemanagerPhotos/save','id="addPictureForm" method="post"');?>
			<div class="modal-body">
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Images</label>
					<div class="col-sm-9">
						<div class="input-group">
							<div class="custom-file">
								<input type="file" name="picture" class="custom-file-input" id="PictureInputFile">
								<label class="custom-file-label" for="PictureInputFile">Choose file</label>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Title</label>
					<div class="col-sm-9">
						<input type="text" name="title" class="form-control form-control-sm" placeholder="Title">
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Caption</label>
					<div class="col-sm-9">
						<textarea class="form-control" name="caption"></textarea>
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Source</label>
					<div class="col-sm-9">
						<input type="text" name="source" class="form-control form-control-sm" placeholder="Source">
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Type</label>
					<div class="col-sm-9">
						<select class="form-control form-control-sm" name="type">
							<option value="ilustration">Ilustration</option>
							<option value="graphic-design">Graphic Design</option>
							<option value="photography" selected>Photography</option>
						</select>
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Status</label>
					<div class="col-sm-9 mt-1">
						<div class="icheck-primary d-inline">
							<input type="radio" id="statusActive" value="1" name="status" checked>
							<label for="statusActive">Publish</label>
						</div>
						<div class="icheck-danger d-inline">
							<input type="radio" id="statusInActive" value="0" name="status">
							<label for="statusInActive">Hide</label>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Cancel</button>
				<button type="submit" id="btnAddUser" class="btn btn-info btn-flat btn-sm">Save</button>
			</div>
			<?= form_close();?>
		</div>
	</div>
</div>

<div class="modal fade" id="editPicturesModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Update Images</h6>
			</div>
			<?= form_open_multipart('root-cms/filemanager/filemanagerPhotos/update','id="editPictureForm" method="post"');?>
			<div class="modal-body" id="dataUpdate">
				<div class="d-flex justify-content-center">
					<div class="spinner-grow" id="loader1" style="width: 5rem; height: 5rem;" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Cancel</button>
				<button type="submit" id="btnAddUser" class="btn btn-info btn-flat btn-sm">Save</button>
			</div>
			<?= form_close();?>
		</div>
	</div>
</div>