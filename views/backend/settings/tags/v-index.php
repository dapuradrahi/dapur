<input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>">
<div class="card card-info card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<button type="button" id="reload" class="btn btn-tool"><i class="fas fa-sync-alt"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
			<button type="button" class="btn btn-tool btn-info" id="addNewTag" <?= ($this->access_menu->access('settings-tags-create',$this->session->userdata('user_id')) ? '':'disabled');?>>Add Tag</button>
		</div>
	</div>
	<div class="card-body table-responsive p-0" style="margin-bottom: ">
		<table id="table" class="table-sm table-striped" style="width:100%">
			<thead>
				<tr class="text-center text-white" style="background-color: #343a40">
					<th>No</th>
					<th data-priority="1">Name</th>
					<th>Cretaed at</th>
					<th><i class="fas fa-eye text-info"></i></th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="addNewTagsModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Add new tag</h6>
			</div>
			<?= form_open(backend.'settings/tags/save','id="newTagForm"');?>
			<div class="modal-body">
				<div class="form-group">
					<input type="text" name="newTag" class="form-control form-control-sm" placeholder="Type new tag">
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Close</button>
				<button type="submit" class="btn btn-info btn-flat btn-sm" id="btnSaveTag">save</button>
			</div>
			<?= form_close();?>
		</div>
	</div>
</div>