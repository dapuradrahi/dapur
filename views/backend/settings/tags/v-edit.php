<?= form_open(backend.'settings/tags/update');?>
<div class="card card-warning card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<a href="<?= backend.'settings/tags/view';?>"><button type="button" class="btn btn-tool btn-danger">Back</button></a>
		</div>
	</div>
	<div class="card-body">
		<div class="form-group">
			<label>Tag name</label>
			<input type="text" name="newTag" class="form-control form-control-sm" value="<?= $dataTag->tag_name;?>" placeholder="Type new tag">
		</div>
	</div>
	<div class="modal-footer d-flex justify-content-center p-1">
		<input type="hidden" name="tagId" value="<?= $dataTag->tag_id;?>">
		<button type="submit" class="btn btn-flat btn-sm btn-info" id="savePicture">Save</button>
	</div>
</div>
<?= form_close();?>