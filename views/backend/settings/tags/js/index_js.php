<script type="text/javascript">
	$(document).ready(function() {
		table = $('#table').DataTable({
			processing: true, 
			serverSide: true, 
			responsive: true,
			order: [], 
			dom: "'<'row pt-2 pr-2 pl-2'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-10'f>>'"+"<'row'<'col-sm-12'tr>>"+"<'row p-2'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
			ajax: {
				url: '<?= backendUrl.'settings/tags/lists';?>',
				type: "POST",
				data: function (data) {
					data.status = $('#filter').val();
				}
			},

			columnDefs: [
			{ responsivePriority: 1, targets: 1 },
			{ responsivePriority: 1, targets: -1 },
			{ width: "50%", "targets": [1] },
			{ width: "20%", "targets": [2,3] },
			{ 
				targets: [1,2,3,4], 
				orderable: false,
			},
			{ 
				targets: [0,3], 
				className: 'text-center',
			}],
		});
		$('#reload').on('click',function() {
			table.ajax.reload();
		});
		
		table.on('click', '.tagDelete', function () {
			var tagId = $(this).attr('data-id');
			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this.!",
				cancelButtonColor: '#d33',
				cancelButtonText: 'Cancel!',
				confirmButtonText: 'Yes, Delete it',
				showCancelButton: true,
				position: 'top',
				width:'20rem',
				showLoaderOnConfirm: true,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						request = $.ajax({
							type: 'POST',
							url: '<?= backendUrl.'settings/tags/delete';?>',
							data: {'tagId':tagId,'sypo':$('input[name=sypo]').val()},
							dataType: 'JSON',
							beforeSend: function(){
							},
						});
						request.done(function (response, textStatus, jqXHR){
							if (response.status) {
								resolve();
							}else{
								// location.reload();
							}
							$('input[name=sypo]').val(response.token);
						});
						request.fail(function (jqXHR, textStatus, errorThrown){
							// location.reload();
						});
						request.always(function () {
						});
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				if (result.value) {
					toastr.success('Topic deleted.!');
					table.ajax.reload();
					swal.close();
				}
			});
		});

		$('#addNewTag').on('click',function(){
			$('#addNewTagsModal').modal({backdrop: 'static', keyboard: false});
		});
		$('#newTagForm').submit(function(e) {
			e.preventDefault();
			LoadingBtn('btnSaveTag','Save','start');
			var data = new FormData(this);
			request = $.ajax({
				type: 'POST',
				url: $('#newTagForm').attr('action'),
				data: data,
				dataType: 'JSON',
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function(){
				},
			});
			request.done(function (response, textStatus, jqXHR){
				if (response.status) {
					toastr.success(response.message);
					$("#newTagForm")[0].reset();
					$('#addNewTagsModal').modal('hide');
				}else{
					toastr.error(response.message);
				}
				$('input[name=sypo]').val(response.token);
			});
			request.fail(function (jqXHR, textStatus, errorThrown){
				location.reload();
			});
			request.always(function () {
				table.ajax.reload();
				LoadingBtn('btnSaveTag','Save','Stop');
			});
		});
	});

</script>