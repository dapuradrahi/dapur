<script type="text/javascript">
	$(document).ready(function() {
		var span = 1;
		var prevTD = "";
		var prevTDVal = "";
		$("#myTable tr td:first-child").each(function() {
			var $this = $(this);
			if ($this.text() == prevTDVal) {
				span++;
				if (prevTD != "") {
					prevTD.attr("rowspan", span);
					$this.remove();
				}
			}else{
				prevTD     = $this;
				prevTDVal  = $this.text();
				span       = 1;
			}
		});
	});
	$(document).ready(function() {

		table = $('#table').DataTable({
			processing: true, 
			serverSide: true, 
			responsive: true,
			order: [], 
			dom: "'<'row pt-2 pr-2 pl-2'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-10'f>>'"+"<'row'<'col-sm-12'tr>>"+"<'row p-2'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
			ajax: {
				url: '<?= backendUrl.'settings/groups/lists';?>',
				type: "POST",
				data: function (data) {
				}
			},

			columnDefs: [
			{ responsivePriority: 1, targets: 1 },
			{ responsivePriority: 1, targets: -1 },
			{ width: "7%", "targets": [0,3] },
			{ 
				targets: [1,2,3], 
				orderable: false,
			},
			{ 
				targets: [0,3], 
				className: 'text-center',
			}],
			drawCallback: function(dt) {
			}
		});
		$('#reload').on('click',function() {
			table.ajax.reload();
		});
		
		$('#addGroup').on('click',function() {
			$('#addGroupModal').modal({backdrop: 'static', keyboard: false});
		});
		$('#addGroupForm').submit(function(e) {
			e.preventDefault();
			var data = $('#addGroupForm');
			request = $.ajax({
				type: 'POST',
				url: data.attr('action'),
				data: data.serialize(),
				dataType: 'JSON',
				beforeSend: function(){
				},
			});
			request.done(function (response, textStatus, jqXHR){
				if (response.status) {
					toastr.success(response.message);
					$("#addGroupForm")[0].reset();
					$('#addGroupModal').modal('hide');
					table.ajax.reload();
				}else{
					location.reload();
				}
				$('input[name=sypo]').val(response.token);
			});
			request.fail(function (jqXHR, textStatus, errorThrown){
				location.reload();
			});
			request.always(function () {
			});
		});

		table.on('click', '.groupDelete', function () {
			var groupId = $(this).attr('data-id');
			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this.!",
				cancelButtonColor: '#d33',
				cancelButtonText: 'Cancel!',
				confirmButtonText: 'Yes, Delete it',
				showCancelButton: true,
				position: 'top',
				width:'20rem',
				showLoaderOnConfirm: true,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						request = $.ajax({
							type: 'POST',
							url: '<?= backendUrl.'settings/groups/delete';?>',
							data: {'groupId':groupId,'sypo':$('input[name=sypo]').val()},
							dataType: 'JSON',
							beforeSend: function(){
							},
						});
						request.done(function (response, textStatus, jqXHR){
							if (response.status) {
								resolve();
							}else{
								location.reload();
							}
							$('input[name=sypo]').val(response.token);
						});
						request.fail(function (jqXHR, textStatus, errorThrown){
							// location.reload();
						});
						request.always(function () {
						});
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				if (result.value) {
					toastr.success('Group deleted.!');
					table.ajax.reload();
					swal.close();
				}
			});
		})
	});

</script>