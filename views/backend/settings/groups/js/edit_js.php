<script type="text/javascript">
	$(document).ready(function() {
		var span = 1;
		var prevTD = "";
		var prevTDVal = "";
		$("#myTable tr td:first-child").each(function() {
			var $this = $(this);
			if ($this.text() == prevTDVal) {
				span++;
				if (prevTD != "") {
					prevTD.attr("rowspan", span);
					$this.remove();
				}
			}else{
				prevTD     = $this;
				prevTDVal  = $this.text();
				span       = 1;
			}
		});
	});
</script>