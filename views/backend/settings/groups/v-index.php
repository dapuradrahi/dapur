<div class="card card-info card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<button type="button" id="reload" class="btn btn-tool"><i class="fas fa-sync-alt"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
			<button type="button" class="btn btn-tool btn-info" <?= ($this->access_menu->access('settings-groups-create',$this->session->userdata('user_id')) ? '':'disabled');?> id="addGroup">Add Group</button>
		</div>
	</div>
	<div class="card-body table-responsive p-0">
		<table id="table" class="table-sm table-striped" style="width:100%">
			<thead>
				<tr class="text-center text-white" style="background-color: #343a40">
					<th>No</th>
					<th data-priority="1">Name</th>
					<th>Description</th>
					<th>Action</th> 
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="addGroupModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Add new group</h6>
			</div>
			<?= form_open('root-cms/settings/groups/add','id="addGroupForm"');?>
			<div class="modal-body">
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Name</label>
					<div class="col-sm-9">
						<input type="text" name="name" class="form-control form-control-sm" placeholder="Name" autocomplete="off" required autofocus>
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Description</label>
					<div class="col-sm-9">
						<input type="text" name="desc" class="form-control form-control-sm" placeholder="Description" autocomplete="off">
					</div>
				</div>
				<table class="table table-sm mt-2" id="myTable">
					<thead>
						<tr class="text-white" style="background-color: #343a40">
							<th style="width: 25%;text-align: center;">Module name</th>
							<th style="width: 25px; text-align: center;">Sub Module</th>
							<th style="text-align: center">Access</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = -1; foreach ($module_access['module'] as $k => $v) {?>
							<?php if (isset($v['parent'])) {?>
								<tr>
									<td><b><?= $v['parent'];?></b></td>
									<td><b><?= $v['name'];?></b></td>
									<td><div class="form-group clearfix float-right"><?php foreach ($v['access'] as $childAcc) {?>
										<div class="icheck-primary d-inline">
											<input type="checkbox" id="<?= $v['parent'].'-'.$v['name'].'-'.$childAcc ;?>" value="<?= $v['parent'].'-'.$v['name'].'-'.$childAcc ;?>" name="module_access[]">
											<label for="<?= $v['parent'].'-'.$v['name'].'-'.$childAcc ;?>"><?= $childAcc;?></label>
										</div>
									<?php }?></div></td>
								</tr>
							<?php }else{?>
								<tr>
									<td><b><?= $v['name'];?></b></td>
									<td>-</td>
									<td><div class="form-group clearfix float-right"><?php foreach ($v['access'] as $mainAcc) {?>
										<div class="icheck-primary d-inline">
											<input type="checkbox" id="<?= $v['name'].'-'.$mainAcc ;?>" value="<?= $v['name'].'-'.$mainAcc ;?>" name="module_access[]">
											<label for="<?= $v['name'].'-'.$mainAcc ;?>"><?= $mainAcc;?></label>
										</div>
									<?php }?></div></td>
								</tr>
							<?php }?>
						<?php $no++; }?>
				</tbody>
				</table>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Cancel</button>
				<button type="submit" id="btnAddUser" class="btn btn-info btn-flat btn-sm">Save</button>
			</div>
			<?= form_close();?>
		</div>
	</div>
</div>