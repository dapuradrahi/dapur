<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<button type="button" class="btn btn-tool btn-danger" onclick="window.location.href='<?= backend.'settings/groups';?>'">Back</button>
		</div>
	</div>
	<?= form_open('root-cms/settings/groups/update','id="addGroupForm"');?>
	<div class="card-body table-responsive">
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Name</label>
			<div class="col-sm-9">
				<input type="text" name="name" class="form-control form-control-sm" placeholder="Name" autocomplete="off" required value="<?= $groupInfo['name'];?>">
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Description</label>
			<div class="col-sm-9">
				<input type="text" name="desc" class="form-control form-control-sm" placeholder="Description" autocomplete="off" value="<?= $groupInfo['description'];?>">
				<input type="hidden" name="groupId" value="<?= $groupInfo['groupId'];?>">
			</div>
		</div>
		<table class="table mt-2" id="myTable">
			<thead>
				<tr class="text-white" style="background-color: #343a40">
					<th style="width: 25%;text-align: center;">Module name</th>
					<th style="width: 25px; text-align: center;">Sub Module</th>
					<th style="text-align: center">Access</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = -1; foreach ($module_access['module'] as $k => $v) {?>
					<?php if (isset($v['parent'])) {?>
						<tr>
							<td><b><?= $v['parent'];?></b></td>
							<td><b><?= $v['name'];?></b></td>
							<td><div class="form-group clearfix float-right"><?php foreach ($v['access'] as $childAcc) {?>
								<div class="icheck-primary d-inline">
									<input type="checkbox" id="<?= $v['parent'].'-'.$v['name'].'-'.$childAcc ;?>" value="<?= $v['parent'].'-'.$v['name'].'-'.$childAcc ;?>" name="module_access[]" <?php if(isset($accessGroup[$v['name']])) {
										if (is_array($accessGroup[$v['name']])) {
											if (in_array($childAcc, $accessGroup[$v['name']])) {
												echo 'checked';
											}elseif($childAcc == $accessGroup[$v['name']]){
												echo 'checked';
											}
										}elseif ($accessGroup[$v['name']] == $childAcc) {
											echo 'checked';
										}
									};?>>
									<label for="<?= $v['parent'].'-'.$v['name'].'-'.$childAcc ;?>"><?= $childAcc;?></label>
								</div>
							<?php }?>
						</div></td>
					</tr>
				<?php }else{?>
					<tr>
						<td><b><?= $v['name'];?></b></td>
						<td>-</td>
						<td><div class="form-group clearfix float-right"><?php foreach ($v['access'] as $mainAcc) {?>
							<div class="icheck-primary d-inline">
								<input type="checkbox" id="<?= $v['name'].'-'.$mainAcc ;?>" value="<?= $v['name'].'-'.$mainAcc ;?>" name="module_access[]" <?php if (isset($accessGroup[$v['name']])) {
									if (is_array($accessGroup[$v['name']])) {
										if (in_array($mainAcc, $accessGroup[$v['name']])) {
											echo 'checked';
										}elseif ($mainAcc == $accessGroup[$v['name']]) {
											echo 'checked';
										}
									}elseif($accessGroup[$v['name']] == $mainAcc){
										echo 'checked';
									}
								};?>>
								<label for="<?= $v['name'].'-'.$mainAcc ;?>"><?= $mainAcc;?></label>
								</div><?php }?>
							</div>
						</td>
					</tr>
				<?php }?>
				<?php $no++; }?>
			</tbody>
		</table>
	</div>
	<div class="card-footer text-center">
		<button type="button" class="btn btn-danger btn-flat" onclick="window.location.href='<?= backend.'settings/groups';?>'">Back</button>
		<button type="submit" class="btn btn-info btn-flat">Update</button>
	</div>
	<?= form_close();?>
</div>