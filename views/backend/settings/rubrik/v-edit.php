<?= form_open_multipart(backend.'settings/rubrik/update');?>
<div class="card card-warning card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<a href="<?= backend.'settings/rubrik/view';?>"><button type="button" class="btn btn-tool btn-danger">Back</button></a>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-4">
				<a data-fancybox="gallery" id="rubrik_picture_a_src" href="<?= storage.'picture/high/'.$dataRubrik->category_picture;?>" data-caption="<?= 'Title : '.$dataRubrik->category_name;?>">
					<img id="rubrik_picture_src" src="<?= storage.'picture/high/'.$dataRubrik->category_picture;?>" class="img-fluid">
				</a>
				<input type="hidden" name="rubrik_picture" id="rubrik_picture" value="">
			</div>
			<div class="col-md-8">
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control form-control-sm" name="name" placeholder="Name" autocomplete="off" value="<?= $dataRubrik->category_name;?>">
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea class="form-control form-control-sm" name="description" placeholder="Description"><?= $dataRubrik->category_desc;?></textarea>
				</div>
				<div class="form-group">
					<label>Keywords</label>
					<textarea class="form-control form-control-sm" name="keywords" placeholder="Ex : info bandung,bandung"><?= $dataRubrik->category_keywords;?></textarea>
				</div>
				<div class="form-group">
					<label>Add image</label>
					<button type="button" class="btn btn-info btn-sm btn-flat float-right" id="addImage">Add Image</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer d-flex justify-content-center p-1">
		<input type="hidden" name="rubrikId" value="<?= $dataRubrik->category_id;?>">
		<input type="hidden" name="channel" value="<?= $dataRubrik->category_channel;?>">
		<button type="submit" class="btn btn-flat btn-sm btn-info" id="savePicture">Save</button>
	</div>
</div>
<?= form_close();?>
<div class="modal fade" id="generalModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-scrollable" role="document" style="height: 900px">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Modal general</h6>
			</div>
			<div class="modal-body p-0">
				<div class="form-group">
					<div class="fileIframe">
						<section class="contentIframe">
							<div id="loadingContent"></div>
							<iframe id="general-modal-iframe" src="" allowfullscreen><div class="d-flex justify-content-center">
								<div class="spinner-grow" id="loader1" style="width: 5rem; height: 5rem;" role="status">
									<span class="sr-only">Loading...</span>
								</div>
							</div></iframe>
						</section>
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Close</button>
			</div>
		</div>
	</div>
</div>