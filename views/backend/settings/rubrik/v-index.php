<div class="row">
	<div class="col-md-6">
		<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title"><?= $title;?></h3>
			</div>
			<div class="card-body p-2">
				<div class="dd">
					<ol class="dd-list todo-list">
						<?php foreach ($rubrik as $key => $value) {?>
							<li class="dd-item" data-id="<?= $value['category_id'];?>">
								<div class="handle dd-handle" style="cursor: move;">
									<i class="fas fa-ellipsis-v"></i>
									<i class="fas fa-ellipsis-v"></i>
								</div>
								<span class="text"><?= ucwords(strtolower($value['category_name']));?></span>
								<div class="tools">
									<a href="<?= backend.'settings/rubrik/edit/'.$value['category_id'];?>"><i class="fas fa-edit text-info" data-id="<?= $value['category_id'];?>"></i></a>
									<i class="fas fa-trash deleteCategory" data-id="<?= $value['category_id'];?>"></i>
								</div>
							</li>
						<?php }?>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">Add new rubrik</h3>
			</div>
			<?= form_open(backend.'settings/rubrik/save');?>
			<div class="card-body">
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control form-control-sm" name="name" placeholder="Name" autocomplete="off">
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea class="form-control form-control-sm" name="description" placeholder="Description"></textarea>
				</div>
				<div class="form-group">
					<label>Keywords</label>
					<textarea class="form-control form-control-sm" name="keywords" placeholder="Ex : info bandung,bandung"></textarea>
				</div>
				<div class="form-group">
					<label>Add image</label>
					<button class="btn btn-info btn-sm btn-flat float-right" id="addImage">Add Image</button>
				</div>
				<div class="form-group">
					<div class="row d-flex justify-content-center">
						<div class="col-sm-8">
							<div class="card">
								<img src="<?= storage.'no-image.png';?>" id="rubrik_picture_src" class="img-fluid">
							</div>
							<input type="hidden" name="rubrik_picture" id="rubrik_picture" value="">
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer d-flex justify-content-center p-1">
				<button type="submit" class="btn btn-success btn-flat btn-sm">Save</button>
			</div>
			<?= form_close();?>
		</div>
	</div>
</div>
<div class="modal fade" id="generalModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-scrollable" role="document" style="height: 900px">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Modal general</h6>
			</div>
			<div class="modal-body p-0">
				<div class="form-group">
					<div class="fileIframe">
						<section class="contentIframe">
							<div id="loadingContent"></div>
							<iframe id="general-modal-iframe" src="" allowfullscreen><div class="d-flex justify-content-center">
								<div class="spinner-grow" id="loader1" style="width: 5rem; height: 5rem;" role="status">
									<span class="sr-only">Loading...</span>
								</div>
							</div></iframe>
						</section>
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Close</button>
			</div>
		</div>
	</div>
</div>