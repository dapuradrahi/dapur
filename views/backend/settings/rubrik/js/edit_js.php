<script type="text/javascript">
	$(document).ready(function() {
		$('.fancybox').fancybox({
			beforeShow : function(){
				this.title =  $(this.element).data("caption");
			}
		});
		$("#general-modal-iframe").on('load',function () {
			LoadingContent('loadingContent','','stop');
			$(this).contents().find(".InsertToEditor").click(function(){
				$('#generalModal').modal('hide');
				var imageId = $(this).attr('data-id');
				var imageFilename = $(this).attr('data-filename');
				document.getElementById("rubrik_picture_a_src").href = "<?= storage;?>picture/high/"+imageFilename;
				document.getElementById("rubrik_picture_src").src = "<?= storage;?>picture/low/"+imageFilename;
				$("#rubrik_picture").val(imageFilename);
			});
		});

		$('#addImage').on('click', function(){
			$('#generalModal').modal({backdrop: 'static', keyboard: false});
			$('#generalModal').find('.modal-title').text('Insert image');
			var url = '<?= backend.'IframeContent/images';?>';
			$('#general-modal-iframe').attr('src', url);
			LoadingContent('loadingContent','','start');
			return false;
		});
	});
</script>