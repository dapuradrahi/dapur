<script type="text/javascript">
	$(document).ready(function() {
		$('.dd').nestable({

		});
		$('.dd').on('change', function() {
			setOrder($('.dd').nestable('serialize'));
		});
		function setOrder(dd) {
			var serializedData = window.JSON.stringify(dd);
			console.log(serializedData);
		};



		$('.select2').select2({
			placeholder: "Filter Rubrik",
			allowClear: true,
			ajax: {
				url: '<?= backend.'JSON_select/rubrik_list';?>',
				dataType: 'json',
				data: function (params) {
					var query = {
						search: params.term,
						channel: '1'
					}
					return query;
				}
			}
		});

		$("#general-modal-iframe").on('load',function () {
			LoadingContent('loadingContent','','stop');
			$(this).contents().find(".InsertToEditor").click(function(){
				$('#generalModal').modal('hide');
				var imageId = $(this).attr('data-id');
				var imageFilename = $(this).attr('data-filename');
				document.getElementById("rubrik_picture_src").src = "<?= storage;?>picture/high/"+imageFilename;
				$("#rubrik_picture").val(imageFilename);
			});
		});

		$('#addImage').on('click', function(){
			$('#generalModal').modal({backdrop: 'static', keyboard: false});
			$('#generalModal').find('.modal-title').text('Insert image');
			var url = '<?= backend.'IframeContent/images';?>';
			$('#general-modal-iframe').attr('src', url);
			LoadingContent('loadingContent','','start');
			return false;
		});

		$('.deleteCategory').on('click',function () {
			var catId = $(this).attr('data-id');
			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this.!",
				cancelButtonColor: '#d33',
				cancelButtonText: 'Cancel!',
				confirmButtonText: 'Yes, Delete it',
				showCancelButton: true,
				position: 'top',
				width:'20rem',
				showLoaderOnConfirm: true,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						request = $.ajax({
							type: 'POST',
							url: '<?= backendUrl.'settings/rubrik/delete';?>',
							data: {'catId':catId,'sypo':$('input[name=sypo]').val()},
							dataType: 'JSON',
							beforeSend: function(){
							},
						});
						request.done(function (response, textStatus, jqXHR){
							if (response.status) {
								$('.dd').nestable('remove', catId);
								resolve();
							}else{
								// location.reload();
							}
							$('input[name=sypo]').val(response.token);
						});
						request.fail(function (jqXHR, textStatus, errorThrown){
							// location.reload();
						});
						request.always(function () {
						});
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				if (result.value) {
					toastr.success('Rubrik deleted.!');
					swal.close();
				}
			});
		})
	});
</script>