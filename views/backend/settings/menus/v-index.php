<div class="row">
	<div class="col-md-6">
		<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title"><?= $title;?></h3>
			</div>
			<div class="card-body p-2">
				<div class="dd">
					<?= $menus;?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card card-primary card-outline">
			<div class="card-header">
				<h3 class="card-title">Add new rubrik</h3>
			</div>
			<?= form_open(backend.'settings/menus/save');?>
			<div class="card-body">
				<div class="form-group">
					<input type="text" class="form-control form-control-sm" name="name" placeholder="Name" autocomplete="off">
				</div>
				<div class="form-group">
					<input type="text" class="form-control form-control-sm" name="url" placeholder="URL" autocomplete="off">
				</div>
				<div class="form-group">
					<input type="text" class="form-control form-control-sm" name="class" placeholder="Class" autocomplete="off">
				</div>
				<div class="form-group">
					<input type="number" class="form-control form-control-sm" name="order" placeholder="Order" autocomplete="off">
				</div>
				<div class="form-group">
					<select class="form-control form-control-sm parent" name="parent">
					</select>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-3 col-form-label">Position</label>
					<div class="col-sm-9 mt-1">
						<div class="icheck-primary d-inline">
							<input type="radio" id="header" value="header" name="position" checked>
							<label for="header">Header</label>
						</div>
						<div class="icheck-purple d-inline">
							<input type="radio" id="footer" value="footer" name="position">
							<label for="footer">Footer</label>
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer d-flex justify-content-center p-2">
				<button type="submit" class="btn btn-success btn-flat btn-sm">Save</button>
			</div>
			<?= form_close();?>
		</div>
	</div>
</div>