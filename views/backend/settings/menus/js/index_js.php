<script type="text/javascript">
	$('.dd').nestable({

	});
	$('.dd').on('change', function() {
		setOrder($('.dd').nestable('serialize'));
	});
	function setOrder(dd) {
		var serializedData = window.JSON.stringify(dd);
		console.log(serializedData);
	};

	$('.parent').select2({
		allowClear: true,
		placeholder: "**Select Parent Menu**",
		ajax: {
			url: '<?= backend.'JSON_select/menu_list';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					channel: '1'
				}
				return query;
			}
		}
	});

	$('.deleteMenu').on('click',function () {
			var menuId = $(this).attr('data-id');
			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this.!",
				cancelButtonColor: '#d33',
				cancelButtonText: 'Cancel!',
				confirmButtonText: 'Yes, Delete it',
				showCancelButton: true,
				position: 'top',
				width:'20rem',
				showLoaderOnConfirm: true,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						request = $.ajax({
							type: 'POST',
							url: '<?= backendUrl.'settings/menus/delete';?>',
							data: {'menuId':menuId,'sypo':$('input[name=sypo]').val()},
							dataType: 'JSON',
							beforeSend: function(){
							},
						});
						request.done(function (response, textStatus, jqXHR){
							if (response.status) {
								$('.dd').nestable('remove', menuId);
								resolve();
							}else{
								location.reload();
							}
							$('input[name=sypo]').val(response.token);
						});
						request.fail(function (jqXHR, textStatus, errorThrown){
							location.reload();
						});
						request.always(function () {
						});
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				if (result.value) {
					toastr.success('Menu deleted.!');
					swal.close();
				}
			});
		})
</script>