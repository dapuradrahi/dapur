<script type="text/javascript">
	$('.parent').select2({
		allowClear: true,
		placeholder: "**Select Parent Menu**",
		ajax: {
			url: '<?= backend.'JSON_select/menu_list';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					channel: '1'
				}
				return query;
			}
		}
	});
</script>