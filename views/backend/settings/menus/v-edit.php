<?= form_open(backend.'settings/menus/update');?>
<div class="card card-warning card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<a href="<?= backend.'settings/menus/view';?>"><button type="button" class="btn btn-tool btn-danger">Back</button></a>
		</div>
	</div>
	<div class="card-body">
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Last Name</label>
			<div class="col-sm-9">
				<input type="text" class="form-control form-control-sm" name="name" value="<?= $dataMenu->menu_name;?>" placeholder="Name" autocomplete="off">
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">URL</label>
			<div class="col-sm-9">
				<input type="text" class="form-control form-control-sm" value="<?= $dataMenu->menu_url;?>" name="url" placeholder="URL" autocomplete="off">
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Class</label>
			<div class="col-sm-9">
				<input type="text" class="form-control form-control-sm" value="<?= $dataMenu->menu_class;?>" name="class" placeholder="Class" autocomplete="off">
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Order</label>
			<div class="col-sm-9">
				<input type="number" class="form-control form-control-sm" value="<?= $dataMenu->menu_order;?>" name="order" placeholder="Order" autocomplete="off">
			</div>
		</div>
		<div class="form-group row">
			<label class="col-sm-2 col-form-label">Parent</label>
			<div class="col-sm-9">
				<select class="form-control form-control-sm parent" name="parent">
					<?php if ($dataParent != FALSE) {?>
						<option value="<?= $dataParent->row()->menu_parent_id;?>" selected><?= $dataParent->row()->menu_name;?></option>
					<?php }?>
				</select>
			</div>
		</div>
		<div class="form-group row mb-2">
			<label class="col-sm-3 col-form-label">Position</label>
			<div class="col-sm-9 mt-1">
				<div class="icheck-primary d-inline">
					<input type="radio" id="header" value="header" name="position" <?= ($dataMenu->menu_position == 'header') ? 'checked':'';?>>
					<label for="header">Header</label>
				</div>
				<div class="icheck-purple d-inline">
					<input type="radio" id="footer" value="footer" name="position" <?= ($dataMenu->menu_position == 'footer') ? 'checked':'';?>>
					<label for="footer">Footer</label>
				</div>
			</div>
		</div>
		<div class="form-group row mb-2">
			<label class="col-sm-3 col-form-label">Status</label>
			<div class="col-sm-9 mt-1">
				<div class="icheck-info d-inline">
					<input type="radio" id="showStatus" value="1" name="status" <?= ($dataMenu->menu_status == '1') ? 'checked':'';?>>
					<label for="showStatus">Show</label>
				</div>
				<div class="icheck-danger d-inline">
					<input type="radio" id="hideStatus" value="0" name="status" <?= ($dataMenu->menu_status == '0') ? 'checked':'';?>>
					<label for="hideStatus">Hide</label>
				</div>
			</div>
		</div>	
	</div>
	<div class="modal-footer d-flex justify-content-center p-1">
		<input type="hidden" name="menuId" value="<?= $dataMenu->menu_id;?>">
		<input type="hidden" name="channel" value="<?= $dataMenu->menu_channel;?>">
		<button type="submit" class="btn btn-flat btn-sm btn-info" id="savePicture">Save</button>
	</div>
</div>
<?= form_close();?>