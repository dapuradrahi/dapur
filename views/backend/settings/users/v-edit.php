<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?> - <?= $userdata['first_name'].' '.$userdata['last_name'];?></h3>
		<div class="card-tools">
			<button type="button" class="btn btn-tool btn-danger" onclick="window.location.href='<?= backend.'settings/users';?>'">Back</button>
		</div>
	</div>
	<?= form_open_multipart('root-cms/settings/users/update','id="addGroupForm"');?>
	<div class="card-body table-responsive">
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">First name</label>
			<div class="col-sm-8">
				<input type="text" name="first_name" value="<?= $userdata['first_name'];?>" class="form-control form-control-sm" placeholder="First name" autocomplete="off">
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Last Name</label>
			<div class="col-sm-8">
				<input type="text" name="last_name" value="<?= $userdata['last_name'];?>" class="form-control form-control-sm" placeholder="Last name" autocomplete="off">
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Display Name</label>
			<div class="col-sm-8">
				<input type="text" name="display_name" value="<?= $userdata['display_name'];?>" class="form-control form-control-sm" placeholder="Display name" autocomplete="off">
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Gender</label>
			<div class="col-sm-8 mt-1">
				<div class="icheck-primary d-inline">
					<input type="radio" id="genderMale" value="l" name="gender" <?= ($userdata['gender'] == 'l' ? 'checked':'');?>>
					<label for="genderMale">Male</label>
				</div>
				<div class="icheck-purple d-inline">
					<input type="radio" id="genderFemale" value="p" name="gender" <?= ($userdata['gender'] == 'p' ? 'checked':'');?>>
					<label for="genderFemale">Female</label>
				</div>
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Phone</label>
			<div class="col-sm-8">
				<input type="number" name="phone" value="<?= $userdata['phone'];?>" class="form-control form-control-sm" placeholder="Phone">
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Email</label>
			<div class="col-sm-8">
				<input type="email" name="email" value="<?= $userdata['email'];?>" class="form-control form-control-sm" placeholder="Email" autocomplete="off">
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Company</label>
			<div class="col-sm-8">
				<input type="text" name="company" value="<?= $userdata['company'];?>" class="form-control form-control-sm" placeholder="Company" autocomplete="off">
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Username</label>
			<div class="col-sm-8">
				<input type="text" name="username" value="<?= $userdata['username'];?>" class="form-control form-control-sm" placeholder="Username" autocomplete="off">
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">password</label>
			<div class="col-sm-8">
				<input type="password" name="password" class="form-control form-control-sm" placeholder="Empty if not change" autocomplete="off">
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Level User</label>
			<div class="col-sm-8">
				<select class="form-control form-control-sm select2" name="userLevel">
					<option selected disabled>Select Level user</option>
					<?php $groupss = json_decode($groups);
					foreach ($groupss as $k => $v) {
						echo '<option value="'.$v->id.'" '.($this->ion_auth->get_users_groups($userdata['id'])->row()->id == $v->id ? 'selected':'').'>'.$v->text.'</option>';
					}?>
				</select>
			</div>
		</div>
		<div class="form-group row mb-1">
			<label class="col-sm-3 col-form-label">Status</label>
			<div class="col-sm-8 mt-1">
				<div class="icheck-info d-inline">
					<input type="radio" id="statusActive" value="1" name="active" <?= ($userdata['active'] == 1 ? 'checked':'');?>>
					<label for="statusActive">Active</label>
				</div>
				<div class="icheck-danger d-inline">
					<input type="radio" id="statusInactive" value="0" name="active" <?= ($userdata['active'] == 0 ? 'checked':'');?>>
					<label for="statusInactive">Inactive</label>
				</div>
			</div>
		</div>
		<div class="form-group row pb-0 mb-0">
			<label class="col-sm-3 col-form-label">Picture</label>
			<div class="col-sm-8">
				<input type="file" name="picture" class="">
				<br>
				<span class="text-red">Empty if not change</span><br>
				<span class="text-red">max size 1080 x 1350 px & png | jpg | jpeg format</span>
			</div>
		</div>
	</div>
	<div class="card-footer text-center">
		<input type="hidden" name="uid" value="<?= $userdata['id'];?>">
		<input type="hidden" name="oldPicture" value="<?= $userdata['picture'];?>">
		<button type="button" class="btn btn-danger btn-flat" onclick="window.location.href='<?= backend.'settings/users';?>'">Back</button>
		<button type="submit" class="btn btn-info btn-flat">Update</button>
	</div>
	<?= form_close();?>
</div>