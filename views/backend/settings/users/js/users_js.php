<script type="text/javascript">
	$(document).ready(function() {
		$('.select2').select2();
		table = $('#table').DataTable({
			processing: true, 
			serverSide: true, 
			responsive: true,
			order: [], 
			dom: "'<'row pt-2 pr-2 pl-2'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-2 filter'><'col-sm-12 col-md-8'f>>'"+"<'row'<'col-sm-12'tr>>"+"<'row p-2'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
			ajax: {
				url: '<?= backendUrl.'settings/users/lists';?>',
				type: "POST",
				data: function (data) {
					data.filter = $('#filterGroup').val();
				}
			},

			columnDefs: [
			{ responsivePriority: 1, targets: 1 },
			{ responsivePriority: 1, targets: -1 },
			{ width: "7%", "targets": [0,1] },
			{ width: "10%", "targets": [6,7,8] },
			{ width: "10%", "targets": [4] },
			{ 
				targets: [1,2,3,4,5,6,7,8], 
				orderable: false,
			},
			{ 
				targets: [0,1,5,6,7], 
				className: 'text-center',
			}],
			drawCallback: function(dt) {
				$('.select2').select2({
					placeholder: "Filter Group",
					allowClear: true,
					data:<?= $groups;?>,
				});
				$('#filterGroup').on('change',function() {
					table.ajax.reload();
				});
			}
		});
		$("div.filter").html('<select class="form-control select2" style="width: 100%;" name="filterGroup" id="filterGroup"><option></option></select>');
		$('#reload').on('click',function() {
			table.ajax.reload();
		});

		$('#addUser').on('click',function() {
			$('#addUserModal').modal({backdrop: 'static', keyboard: false});
		});

		$('#addUserForm').submit(function(e) {
			e.preventDefault();
			var data = new FormData(this);
			request = $.ajax({
				type: 'POST',
				url: $('#addUserForm').attr('action'),
				data: data,
				dataType: 'JSON',
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function(){
				},
			});
			request.done(function (response, textStatus, jqXHR){
				if (response.status) {
					toastr.success(response.message);
					$("#addUserForm")[0].reset();
					$('#addUserModal').modal('hide');
					table.ajax.reload();
				}else{
					toastr.error(response.message);
					// location.reload();
				}
				$('input[name=sypo]').val(response.token);
			});
			request.fail(function (jqXHR, textStatus, errorThrown){
				// location.reload();
			});
			request.always(function () {
			});
		});
		
		table.on('click', '.userDelete', function () {
			var uid = $(this).attr('data-id');
			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this.!",
				cancelButtonColor: '#d33',
				cancelButtonText: 'Cancel!',
				confirmButtonText: 'Yes, Delete it',
				showCancelButton: true,
				position: 'top',
				width:'20rem',
				showLoaderOnConfirm: true,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						request = $.ajax({
							type: 'POST',
							url: '<?= backendUrl.'settings/users/delete';?>',
							data: {'uid':uid,'sypo':$('input[name=sypo]').val()},
							dataType: 'JSON',
							beforeSend: function(){
							},
						});
						request.done(function (response, textStatus, jqXHR){
							if (response.status) {
								resolve();
							}else{
								// location.reload();
							}
							$('input[name=sypo]').val(response.token);
						});
						request.fail(function (jqXHR, textStatus, errorThrown){
							// location.reload();
						});
						request.always(function () {
						});
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				if (result.value) {
					toastr.success('User deleted.!');
					table.ajax.reload();
					swal.close();
				}
			});
		})
	});

</script>