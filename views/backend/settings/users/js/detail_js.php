<script type="text/javascript">
	$(function(){
		var hash = window.location.hash;
		hash && $('ul.nav a[href="' + hash + '"]').tab('show');

		$('.nav-tabs a').click(function (e) {
			$(this).tab('show');
			var scrollmem = $('body').scrollTop();
			window.location.hash = this.hash;
			$('html,body').scrollTop(scrollmem);
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		table = $('#table').DataTable({
			processing: true, 
			serverSide: true, 
			responsive: true,
			order: [],
			dom: "'<'row pt-2 pr-2 pl-2'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-4 filter'><'col-sm-12 col-md-6'f>>'"+"<'row'<'col-sm-12'tr>>"+"<'row p-2'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
			ajax: {
				url:'<?= backendUrl.'settings/users/view/newsList';?>',
				type: "POST",
				data: function (data) {
					data.filter = $('#filter').val();
					data.filterDate = ($('#filterDate').val()) ? $('#filterDate').val() : '<?= date('d/m/Y 00:00:00', strtotime('-1 day'));?> - <?= date('d/m/Y H:i:s');?>';
					data.uid = '<?= $this->uri->segment(5);?>';
				},
			},

			columnDefs: [
			{ responsivePriority: 1, targets: 1 },
			{ responsivePriority: 1, targets: -1 },
			{ width: "50%", "targets": [1] },
			{ width: "6%", "targets": [0] },
			{ 
				targets: [1,2,3,4,5], 
				orderable: false,
			},
			{ 
				targets: [0,5], 
				className: 'text-center',
			}],
			drawCallback: function(dt) {
				$('#filterDate').daterangepicker({
					timePicker: true,
					timePickerIncrement: 30,
					locale: {
						format: 'DD/MM/YYYY HH:mm:ss'
					}
				});

				$('#filterDate').on('apply.daterangepicker', function(ev, picker) {
					table.ajax.reload();
				});

			}
		});
		$("div.filter").html('<input type="text" name="filterDate" value="<?= date('d-m-Y H:i:s') ;?>" id="filterDate" class="form-control form-control-sm float-right">');
		table.on('draw', function () {
			console.log('rows news:', table.rows( {search:'applied'} ).count());
			console.log('page news:', table.page.info().recordsDisplay);
		} );


		tableNewsPhoto = $('#tableNewsPhoto').DataTable({
			processing: true, 
			serverSide: true, 
			responsive: true,
			order: [],
			dom: "'<'row pt-2 pr-2 pl-2'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-4 filter2'><'col-sm-12 col-md-6'f>>'"+"<'row'<'col-sm-12'tr>>"+"<'row p-2'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
			ajax: {
				url:'<?= backendUrl.'settings/users/view/newsPhoto';?>',
				type: "POST",
				data: function (data) {
					data.filter = $('#filter').val();
					data.filterDate = ($('#filterDateFhoto').val()) ? $('#filterDateFhoto').val() : '<?= date('d/m/Y 00:00:00', strtotime('-1 day'));?> - <?= date('d/m/Y H:i:s');?>';
					data.uid = '<?= $this->uri->segment(5);?>';
				},
			},

			columnDefs: [
			{ responsivePriority: 1, targets: 1 },
			{ responsivePriority: 1, targets: -1 },
			{ width: "50%", "targets": [1] },
			{ width: "6%", "targets": [0] },
			{ 
				targets: [1,2,3,4], 
				orderable: false,
			},
			{ 
				targets: [0,4], 
				className: 'text-center',
			}],
			drawCallback: function(dt) {
				$('#filterDateFhoto').daterangepicker({
					timePicker: true,
					timePickerIncrement: 30,
					locale: {
						format: 'DD/MM/YYYY HH:mm:ss'
					}
				});

				$('#filterDateFhoto').on('apply.daterangepicker', function(ev, picker) {
					tableNewsPhoto.ajax.reload();
				});

			}
		});
		$("div.filter2").html('<input type="text" name="filterDateFhoto" id="filterDateFhoto" class="form-control form-control-sm float-right">');
		tableNewsPhoto.on('draw', function () {
			console.log('rows photo:', tableNewsPhoto.rows( {search:'applied'} ).count());
			console.log('page photo:', tableNewsPhoto.page.info().recordsDisplay);
		} );

		tableNewsVideo = $('#tableNewsVideo').DataTable({
			processing: true, 
			serverSide: true, 
			responsive: true,
			order: [],
			dom: "'<'row pt-2 pr-2 pl-2'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-4 filter3'><'col-sm-12 col-md-6'f>>'"+"<'row'<'col-sm-12'tr>>"+"<'row p-2'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
			ajax: {
				url:'<?= backendUrl.'settings/users/view/newsVideo';?>',
				type: "POST",
				data: function (data) {
					data.filterDate = ($('#filterDateVideo').val()) ? $('#filterDateVideo').val() : '<?= date('d/m/Y 00:00:00', strtotime('-1 day'));?> - <?= date('d/m/Y H:i:s');?>';
					data.uid = '<?= $this->uri->segment(5);?>';
				},
			},

			columnDefs: [
			{ responsivePriority: 1, targets: 1 },
			{ responsivePriority: 1, targets: -1 },
			{ width: "50%", "targets": [1] },
			{ width: "6%", "targets": [0] },
			{ 
				targets: [1,2,3,4], 
				orderable: false,
			},
			{ 
				targets: [0,4], 
				className: 'text-center',
			}],
			drawCallback: function(dt) {
				$('#filterDateVideo').daterangepicker({
					timePicker: true,
					timePickerIncrement: 30,
					locale: {
						format: 'DD/MM/YYYY HH:mm:ss'
					}
				});

				$('#filterDateVideo').on('apply.daterangepicker', function(ev, picker) {
					tableNewsVideo.ajax.reload();
				});

			}
		});
		$("div.filter3").html('<input type="text" name="filterDateVideo" id="filterDateVideo" class="form-control form-control-sm float-right">');
		tableNewsVideo.on('draw', function () {
			console.log('rows video:', tableNewsVideo.rows( {search:'applied'} ).count());
			console.log('page video:', tableNewsVideo.page.info().recordsDisplay);
		} );
	});

</script>