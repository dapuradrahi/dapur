<div class="card card-widget widget-user">
	<div class="widget-user-header text-white" style="background: url('<?= assets ;?>img/bg-profile.png') center center;">
		<h3 class="widget-user-username text-right"><?= $userdata['first_name'].' '.$userdata['last_name'];?></h3>
		<h5 class="widget-user-desc text-right"><?= $this->ion_auth->get_users_groups($userdata['id'])->row()->name;?></h5>
	</div>
	<div class="widget-user-image">
		<img class="img-circle" src="<?= storage.'profile/thumb/'.$userdata['picture'] ;?>" alt="User Avatar">
	</div>
	<div class="card-footer">
	</div>
</div>
<div class="card card-widget">
	<ul class="nav nav-tabs pt-2 pl-3" id="custom-content-below-tab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="profileTab" data-toggle="pill" href="#v-profile" role="tab" aria-controls="v-profile" aria-selected="true">Profile</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="newsTab" data-toggle="pill" href="#v-news" role="tab" aria-controls="v-news" aria-selected="true">News</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="newPhotosTab" data-toggle="pill" href="#v-newsPhoto" role="tab" aria-controls="v-newsPhoto" aria-selected="true">News Photo</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="newVideoTab" data-toggle="pill" href="#v-newsVideo" role="tab" aria-controls="v-newsVideo" aria-selected="true">News Video</a>
		</li>
	</ul>
	<div class="tab-content p-3" id="postNewsTabContent">
		<div class="tab-pane fade show active" id="v-profile" role="tabpanel" aria-labelledby="profileTab">
			<ul class="list-group list-group-unbordered mb-3">
				<li class="list-group-item">
					<b>First Name</b> <a class="float-right"><?= $userdata['first_name'];?></a>
				</li>
				<li class="list-group-item">
					<b>Last Name</b> <a class="float-right"><?= $userdata['last_name'];?></a>
				</li>
				<li class="list-group-item">
					<b>Display Name</b> <a class="float-right"><?= $userdata['display_name'];?></a>
				</li>
				<li class="list-group-item">
					<b>Gender</b> <a class="float-right"><?= ($userdata['gender'] == 'l' ? 'Laki - Laki':'Perempuan');?></a>
				</li>
				<li class="list-group-item">
					<b>Phone</b> <a class="float-right"><?= $userdata['phone'];?></a>
				</li>
				<li class="list-group-item">
					<b>Email</b> <a class="float-right"><?= $userdata['email'];?></a>
				</li>
				<li class="list-group-item">
					<b>Company</b> <a class="float-right"><?= $userdata['company'];?></a>
				</li>
				<li class="list-group-item">
					<b>Username</b> <a class="float-right"><?= $userdata['username'];?></a>
				</li>
				<li class="list-group-item">
					<b>Status</b> <a class="float-right"><?= ($userdata['active'] == 1? 'Active':'Nonactive');?></a>
				</li>
			</ul>
		</div>
		<div class="tab-pane fade show" id="v-news" role="tabpanel" aria-labelledby="news">
			<table id="table" class="table-sm table-striped" style="width:100%">
				<thead>
					<tr class="text-center text-white" style="background-color: #343a40">
						<th>No</th>
						<th data-priority="1">Title</th>
						<th>Rubrik</th>
						<th>Author</th>
						<th>Editor</th>
						<th>Published at</th>
					</tr>
				</thead>
			</table>
		</div>

		<div class="tab-pane fade show" id="v-newsPhoto" role="tabpanel" aria-labelledby="newsPhoto">
			<table id="tableNewsPhoto" class="table-sm table-striped" style="width:100%">
				<thead>
					<tr class="text-center text-white" style="background-color: #343a40">
						<th>No</th>
						<th data-priority="1">Title</th>
						<th>Author</th>
						<th>Editor</th>
						<th>Published at</th>
					</tr>
				</thead>
			</table>
		</div>

		<div class="tab-pane fade show" id="v-newsVideo" role="tabpanel" aria-labelledby="newsVideo">
			<table id="tableNewsVideo" class="table-sm table-striped" style="width:100%">
				<thead>
					<tr class="text-center text-white" style="background-color: #343a40">
						<th>No</th>
						<th data-priority="1">Title</th>
						<th>Author</th>
						<th>Editor</th>
						<th>Published at</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>