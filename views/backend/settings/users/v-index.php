<div class="card card-info card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<button type="button" id="reload" class="btn btn-tool"><i class="fas fa-sync-alt"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
			<button type="button" class="btn btn-tool btn-info" <?= ($this->access_menu->access('settings-users-create',$this->session->userdata('user_id')) ? '':'disabled');?> id="addUser">Add User</button>
		</div>
	</div>
	<div class="card-body table-responsive p-0">
		<table id="table" class="table-sm table-striped" style="width:100%">
			<thead>
				<tr class="text-center text-white" style="background-color: #343a40">
					<th>No</th>
					<th>Picture</th>
					<th data-priority="1">Full name</th>
					<th>email</th>
					<th>Phone</th>
					<th>Company</th>
					<th><i class="fa fa-users-cog text-info"></i></th>
					<th><i class="fa fa-check-circle text-info"></i></th>
					<th>Action</th> 
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="addUserModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Add new user</h6>
			</div>
			<?= form_open_multipart('root-cms/settings/users/add','id="addUserForm" method="post"');?>
			<div class="modal-body">
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">First name</label>
					<div class="col-sm-9">
						<input type="text" name="first_name" class="form-control form-control-sm" placeholder="First name" autocomplete="off" autofocus="true">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Last Name</label>
					<div class="col-sm-9">
						<input type="text" name="last_name" class="form-control form-control-sm" placeholder="Last name" autocomplete="off">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Display Name</label>
					<div class="col-sm-9">
						<input type="text" name="display_name" class="form-control form-control-sm" placeholder="Display name" autocomplete="off">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Gender</label>
					<div class="col-sm-9 mt-1">
						<div class="icheck-primary d-inline">
							<input type="radio" id="genderMale" value="l" name="gender" checked>
							<label for="genderMale">Male</label>
						</div>
						<div class="icheck-purple d-inline">
							<input type="radio" id="genderFemale" value="p" name="gender">
							<label for="genderFemale">Female</label>
						</div>
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Phone</label>
					<div class="col-sm-9">
						<input type="number" name="phone" class="form-control form-control-sm" placeholder="Phone">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Email</label>
					<div class="col-sm-9">
						<input type="email" name="email" class="form-control form-control-sm" placeholder="Email" autocomplete="off">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Company</label>
					<div class="col-sm-9">
						<input type="text" name="company" class="form-control form-control-sm" placeholder="Company" autocomplete="off">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Username</label>
					<div class="col-sm-9">
						<input type="text" name="username" class="form-control form-control-sm" placeholder="Username" autocomplete="off">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">password</label>
					<div class="col-sm-9">
						<input type="password" name="password" class="form-control form-control-sm" placeholder="Password" autocomplete="off">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Level User</label>
					<div class="col-sm-9">
						<select class="form-control form-control-sm select2" name="userLevel">
							<option selected disabled>Select Level user</option>
							<?php $groupss = json_decode($groups);
							foreach ($groupss as $k => $v) {
								echo '<option value="'.$v->id.'">'.$v->text.'</option>';
							}?>
						</select>
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Status</label>
					<div class="col-sm-9 mt-1">
						<div class="icheck-info d-inline">
							<input type="radio" id="statusActive" value="1" name="active" checked>
							<label for="statusActive">Active</label>
						</div>
						<div class="icheck-danger d-inline">
							<input type="radio" id="statusInactive" value="0" name="active">
							<label for="statusInactive">Inactive</label>
						</div>
					</div>
				</div>
				<div class="form-group row pb-0 mb-0">
					<label class="col-sm-3 col-form-label">Picture</label>
					<div class="col-sm-9">
						<input type="file" name="picture" class="">
						<br>
						<span class="text-red">max size 1080 x 1350 px & png | jpg | jpeg format</span>
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Cancel</button>
				<button type="submit" id="btnAddUser" class="btn btn-info btn-flat btn-sm">Save</button>
			</div>
			<?= form_close();?>
		</div>
	</div>
</div>