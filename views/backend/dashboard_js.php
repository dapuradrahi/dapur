<script>
	var dd = [<?= $dd;?>];
	var dp = [<?= $dp;?>];
	var ds = [<?= $ds;?>];

	var dnp = [<?= $dnp;?>];
	var dnv = [<?= $dnv;?>];

	var config = {
		type: 'line',
		data: {
			labels: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat','Sabtu'],
			datasets: [{
				label: 'News Publish',
				backgroundColor: window.chartColors541.info,
				borderColor: window.chartColors541.info,
				fill: false,
				data: dp,
			},{
				label: 'News Schedule',
				backgroundColor: window.chartColors541.green,
				borderColor: window.chartColors541.green,
				fill: false,
				data: ds,
			},{
				label: 'News Draft',
				backgroundColor: window.chartColors541.black,
				borderColor: window.chartColors541.black,
				fill: false,
				data: dd,
			},{
				label: 'News Photo',
				backgroundColor: window.chartColors541.yellow,
				borderColor: window.chartColors541.yellow,
				fill: false,
				data: dnp,
			},{
				label: 'News Video',
				backgroundColor: window.chartColors541.blue,
				borderColor: window.chartColors541.blue,
				fill: false,
				data: dnv,
			}]
		},
		options: {
			responsive: true,
			title: {
				display: true,
				text: 'POST GRAPH'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Minggu Ini'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Jumlah Posting'
					},
					ticks: {
						min: 0,
						// max: 10,
						stepSize: 1
					}
				}]
			}
		}
	};

	window.onload = function() {
		var ctx = document.getElementById('canvas').getContext('2d');
		window.myLine = new Chart(ctx, config);
	};
</script>