<?= form_open(backend.'managements/pages/update');?>
<div class="card card-info card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<a href="<?= backend.'managements/pages/view';?>"><button type="button" class="btn btn-tool btn-danger">Back</button></a>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<div class="input-group input-group">
						<input type="text" class="form-control form-control-sm" value="<?= $page->page_title;?>" id="title" name="title" placeholder="Title" autocomplete="off">
						<span class="input-group-append">
							<button type="button" class="btn btn-info btn-sm btn-flat" id="titleLeft">100</button>
						</span>
					</div>
				</div>
				<div class="form-group">
					<textarea class="form-control" name="excerpt" id="excerpt" placeholder="Deskripsi : 140 characters left" style="height: 70px"><?= $page->page_excerpt;?></textarea>
					<input type="range" min="0" max="140" id="rangVal" value="0" style="width: 100%;" class="progress-bar bg-danger">
				</div>
				<div class="form-group">
					<textarea id="mytextarea" name="content"><?= $page->page_content;?></textarea>
				</div>
			</div>
			<div class="col-md-4">
				<!-- <div class="form-group">
					<div class="form-group row mb-1">
						<label class="col-sm-3 col-form-label">Channel</label>
						<div class="col-sm-9">
							<select class="form-control form-control channel" name="channel">
							</select>
						</div>
					</div>
				</div> -->
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Status</label>
					<div class="col-sm-9 mt-1">
						<div class="icheck-info d-inline">
							<input type="radio" id="statusActive" value="published" name="status" <?= ($page->page_status == 'published' ? 'checked':'');?>>
							<label for="statusActive">Publish</label>
						</div>
						<div class="icheck-warning d-inline">
							<input type="radio" id="statusInactive" value="draft" name="status" <?= ($page->page_status == 'draft' ? 'checked':'');?>>
							<label for="statusInactive">Draft</label>
						</div>
						<div class="icheck-danger d-inline">
							<input type="radio" id="statusDeleted" value="deleted" name="status" <?= ($page->page_status == 'deleted' ? 'checked':'');?>>
							<label for="statusDeleted">Deleted</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer d-flex justify-content-center p-1">
		<input type="hidden" name="pageId" value="<?= $page->page_id;?>">
		<input type="submit" value="Update" class="btn btn-success btn-flat btn-sm">
		<a href="<?= backend.'';?>"><button type="button" class="btn btn-danger btn-flat btn-sm">cancel</button></a>
	</div>
</div>
<?= form_close();?>

<div class="modal fade" id="generalModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-scrollable" role="document" style="height: 900px">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Baca Juga</h6>
			</div>
			<div class="modal-body p-0">
				<div class="form-group">
					<div class="fileIframe">
						<section class="contentIframe">
							<div id="loadingContent"></div>
							<iframe id="general-modal-iframe" src="" allowfullscreen><div class="d-flex justify-content-center">
								<div class="spinner-grow" id="loader1" style="width: 5rem; height: 5rem;" role="status">
									<span class="sr-only">Loading...</span>
								</div>
							</div></iframe>
						</section>
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Close</button>
			</div>
		</div>
	</div>
</div>