<script type="text/javascript">
	$(document).ready(function() {
		table = $('#table').DataTable({
			processing: true, 
			serverSide: true, 
			responsive: true,
			order: [], 
			dom: "'<'row pt-2 pr-2 pl-2'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-2 filter'><'col-sm-12 col-md-8'f>>'"+"<'row'<'col-sm-12'tr>>"+"<'row p-2'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
			ajax: {
				url: '<?= backendUrl.'managements/pages/lists';?>',
				type: "POST",
				data: function (data) {
					data.status = $('#filter').val();
				}
			},

			columnDefs: [
			{ responsivePriority: 1, targets: 1 },
			{ responsivePriority: 1, targets: -1 },
			{ width: "50%", "targets": [1] },
			{ width: "15%", "targets": [2,3] },
			{ 
				targets: [1,2,3,4,5], 
				orderable: false,
			},
			{ 
				targets: [0,3,4,5], 
				className: 'text-center',
			}],
			drawCallback: function(dt) {
				$('.select2').select2({
					placeholder: "Filter Status",
					allowClear: true,
					data:[{"id":"published","text":"Published"},{"id":"draft","text":"Draft"},{"id":"scheduled","text":"Scheduled"},{"id":"deleted","text":"Deleted"}],
				});
				$('#filter').on('change',function() {
					table.ajax.reload();
				});
			}
		});
		$("div.filter").html('<select class="form-control form-control-sm select2" style="width: 100%;" name="filter" id="filter"><option></option></select>');
		$('#reload').on('click',function() {
			table.ajax.reload();
		});
		
		table.on('click', '.pageDelete', function () {
			var pageId = $(this).attr('data-id');
			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this.!",
				cancelButtonColor: '#d33',
				cancelButtonText: 'Cancel!',
				confirmButtonText: 'Yes, Delete it',
				showCancelButton: true,
				position: 'top',
				width:'20rem',
				showLoaderOnConfirm: true,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						request = $.ajax({
							type: 'POST',
							url: '<?= backendUrl.'managements/pages/delete';?>',
							data: {'pageId':pageId,'sypo':$('input[name=sypo]').val()},
							dataType: 'JSON',
							beforeSend: function(){
							},
						});
						request.done(function (response, textStatus, jqXHR){
							if (response.status) {
								resolve();
							}else{
								// location.reload();
							}
							$('input[name=sypo]').val(response.token);
						});
						request.fail(function (jqXHR, textStatus, errorThrown){
							// location.reload();
						});
						request.always(function () {
						});
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				if (result.value) {
					toastr.success('Page deleted.!');
					table.ajax.reload();
					swal.close();
				}
			});
		});
		table.on('click', '.deletePermanen', function () {
			var pageId = $(this).attr('data-id');
			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this.!",
				cancelButtonColor: '#d33',
				cancelButtonText: 'Cancel!',
				confirmButtonText: 'Yes, Delete it',
				showCancelButton: true,
				position: 'top',
				width:'20rem',
				showLoaderOnConfirm: true,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						request = $.ajax({
							type: 'POST',
							url: '<?= backendUrl.'managements/pages/delete_permanen';?>',
							data: {'pageId':pageId,'sypo':$('input[name=sypo]').val()},
							dataType: 'JSON',
							beforeSend: function(){
							},
						});
						request.done(function (response, textStatus, jqXHR){
							if (response.status) {
								resolve();
							}else{
								// location.reload();
							}
							$('input[name=sypo]').val(response.token);
						});
						request.fail(function (jqXHR, textStatus, errorThrown){
							// location.reload();
						});
						request.always(function () {
						});
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				if (result.value) {
					toastr.success('Page deleted.!');
					table.ajax.reload();
					swal.close();
				}
			});
		});
	});

</script>