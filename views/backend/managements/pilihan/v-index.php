<input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>">
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
			<button type="button" class="btn btn-tool btn-info addPilihan" id="addPilihan">Tambah Pilihan</button>
		</div>
	</div>
	<?= form_open(backend.'managements/pilihan/save');?>
	<div class="card-body">
		<div class="row" id="pilihanSelected">
			<?php if(!empty($pilihan)){?>
				<?php $no=1; foreach ($pilihan as $k => $v) {?>
					<?php
					preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $v['content'], $image);
					$author = $this->ion_auth->user($v['author_id'])->row();
					?>
					<div class="col-lg-3 col-12 cardPilihan">
						<div class="card">
							<h6 class="card-header p-2 bg-info">Pilihan Editor <?=$no++;?></h6>
							<a data-fancybox="gallery" href="<?= (!empty($image[1])) ? $image[1]:storage.'no-image.png';?>" data-caption="<?= $v['title'];?>"><img src="<?= (!empty($image[1])) ? $image[1]:storage.'no-image.png';?>" class="card-img-top"></a>
							<div class="card-body p-2" style="min-height: 60px">
								<p class="card-text"><?= substr($v['title'],0,65);?>...</p>
							</div>
							<div class="card-footer p-2">
								<h3 class="card-title text-sm text-info"><i class="fa fa-user"></i> <?= $author->first_name.' '.$author->last_name;?></h3>
								<div class="card-tools float-right">
									<button type="button" class="btn btn-tool deletePilihan" data-id=""><i class="fas fa-trash text-red"></i></button>
								</div>
							</div>
						</div>
						<input type="hidden" name="news_id[]" value="<?= $v['id'];?>">
					</div>
				<?php }?>
			<?php }?>
		</div>
	</div>
	<div class="card-footer d-flex justify-content-center p-2">
		<input type="hidden" name="pilihan_id" value="<?= $pilihan_id;?>">
		<input type="submit" id="btnSave" class="btn btn-info btn-flat btn-sm" value="Save">
	</div>
	<?= form_close();?>
</div>

<div class="modal fade" id="generalModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-scrollable" role="document" style="height: 900px">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Baca Juga</h6>
			</div>
			<div class="modal-body p-0">
				<div class="form-group">
					<div class="fileIframe">
						<section class="contentIframe">
							<div id="loadingContent"></div>
							<iframe id="general-modal-iframe" src="" allowfullscreen><div class="d-flex justify-content-center">
								<div class="spinner-grow" id="loader1" style="width: 5rem; height: 5rem;" role="status">
									<span class="sr-only">Loading...</span>
								</div>
							</div></iframe>
						</section>
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Close</button>
			</div>
		</div>
	</div>
</div>