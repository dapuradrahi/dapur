<script type="text/javascript">
	$("#general-modal-iframe").on('load',function () {
		LoadingContent('loadingContent','','stop');
		$(this).contents().find(".insertNews").click(function(){
			$('#generalModal').modal('hide');
			var newsId = $(this).attr('data-id');
			var newsTitle = $(this).attr('data-title');
			var newsAuthor = $(this).attr('data-author');
			var newsImage = $(this).attr('data-image');
			console.log(newsTitle);
			addCols(1,newsId,newsTitle,newsAuthor,newsImage);
		});
	});

	$('.addPilihan').on('click',function(){
		$('#generalModal').modal({backdrop: 'static', keyboard: false});
		$('#generalModal').find('.modal-title').text('Tambah Pilihan Editor');
		var url = '<?= backend.'IframeContent/newsList';?>';
		$('#general-modal-iframe').attr('src', url);
		LoadingContent('loadingContent','','start');
		return false;
	});

	var addCols = function (num,newsId,newsTitle,newsAuthor,newsImage){
		for (var i=1;i<=num;i++) {
			var myCol = $('<div class="col-lg-3 col-12 cardPilihan"></div>');
			var myPanel = $('<div class="card"><h6 class="card-header p-2 bg-info">Pilihan Baru</h6><a data-fancybox="gallery" href="'+newsImage+'" data-caption="'+newsTitle+'"><img src="'+newsImage+'" class="card-img-top"></a><div class="card-body p-2" style="min-height: 60px"><p class="card-text">'+newsTitle+'</p></div><div class="card-footer p-2"><h3 class="card-title text-sm text-info"><i class="fa fa-user"></i></h3><div class="card-tools float-right"><button type="button" class="btn btn-tool deletePilihan" data-id=""><i class="fas fa-trash text-red"></i></button></div></div><input type="hidden" name="news_id[]" value="'+newsId+'"></div>');
			myPanel.appendTo(myCol);
			myCol.appendTo('#pilihanSelected');
		}

		$('.deletePilihan').on('click', function(e){
			e.stopPropagation();  
			var $target = $(this).parents('.cardPilihan');
			$target.hide('slow', function(){ $target.remove(); });
		});
	}

	$('.deletePilihan').on('click', function(e){
		e.stopPropagation();  
		var $target = $(this).parents('.cardPilihan');
		$target.hide('slow', function(){ $target.remove(); });
	});
</script>