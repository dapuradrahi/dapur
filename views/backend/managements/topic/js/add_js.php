<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD-MM-YYYY, HH:mm:ss',
		});
	});

	$("#general-modal-iframe").on('load',function () {
		LoadingContent('loadingContent','','stop');
		$(this).contents().find(".InsertToEditor").click(function(){
			$('#generalModal').modal('hide');
			var imageId = $(this).attr('data-id');
			var imageFilename = $(this).attr('data-filename');
			document.getElementById("topic_picture_src").src = "<?= storage;?>picture/high/"+imageFilename;
			$("#topic_picture").val(imageFilename);
		});
	});

	$('#btnAddFoto').on('click', function(){
		$('#generalModal').modal({backdrop: 'static', keyboard: false});
		$('#generalModal').find('.modal-title').text('Insert image');
		var url = '<?= backend.'IframeContent/images';?>';
		$('#general-modal-iframe').attr('src', url);
		LoadingContent('loadingContent','','start');
		return false;
	});

</script>