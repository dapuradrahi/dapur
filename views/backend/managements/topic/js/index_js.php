<script type="text/javascript">
	$(document).ready(function() {
		table = $('#table').DataTable({
			processing: true, 
			serverSide: true, 
			responsive: true,
			order: [], 
			dom: "'<'row pt-2 pr-2 pl-2'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-10'f>>'"+"<'row'<'col-sm-12'tr>>"+"<'row p-2'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
			ajax: {
				url: '<?= backendUrl.'managements/topic/lists';?>',
				type: "POST",
				data: function (data) {
					data.status = $('#filter').val();
				}
			},

			columnDefs: [
			{ responsivePriority: 1, targets: 1 },
			{ responsivePriority: 1, targets: -1 },
			{ width: "30%", "targets": [2] },
			{ width: "40%", "targets": [3] },
			{ width: "10%", "targets": [1] },
			{ width: "15%", "targets": [4] },
			{ 
				targets: [1,2,3,4,5,6], 
				orderable: false,
			},
			{ 
				targets: [0,1,5], 
				className: 'text-center',
			}],
		});
		$('#reload').on('click',function() {
			table.ajax.reload();
		});
		
		table.on('click', '.topicDelete', function () {
			var topicId = $(this).attr('data-id');
			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this.!",
				cancelButtonColor: '#d33',
				cancelButtonText: 'Cancel!',
				confirmButtonText: 'Yes, Delete it',
				showCancelButton: true,
				position: 'top',
				width:'20rem',
				showLoaderOnConfirm: true,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						request = $.ajax({
							type: 'POST',
							url: '<?= backendUrl.'managements/topic/delete';?>',
							data: {'topicId':topicId,'sypo':$('input[name=sypo]').val()},
							dataType: 'JSON',
							beforeSend: function(){
							},
						});
						request.done(function (response, textStatus, jqXHR){
							if (response.status) {
								resolve();
							}else{
								// location.reload();
							}
							$('input[name=sypo]').val(response.token);
						});
						request.fail(function (jqXHR, textStatus, errorThrown){
							// location.reload();
						});
						request.always(function () {
						});
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				if (result.value) {
					toastr.success('Topic deleted.!');
					table.ajax.reload();
					swal.close();
				}
			});
		})
	});

</script>