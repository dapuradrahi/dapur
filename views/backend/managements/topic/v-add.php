<?= form_open(backend.'managements/topic/save');?>
<div class="card card-info card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<label>Name</label>
					<input type="text" class="form-control form-control-sm" name="name" placeholder="Name" autocomplete="off">
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea class="form-control" name="description" placeholder="Deskripsi" style="height: 200px"></textarea>
				</div>
				<div class="form-group">
					<label>Keywords</label>
					<textarea class="form-control" name="keywords" placeholder="Keywords" style="height: 100px"></textarea>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group text-right">
					<button type="button" class="btn btn-success btn-sm btn-flat" id="btnAddFoto">Add Image</button>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<img src="<?= storage.'no-image.png';?>" id="topic_picture_src" class="img-fluid">
						</div>
						<input type="hidden" name="topic_picture" id="topic_picture" value="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer d-flex justify-content-center p-1">
		<input type="submit" value="save" name="status" class="btn btn-info btn-flat btn-sm">
		<a href="<?= backend.'managements/topic/view';?>"><button type="button" class="btn btn-danger btn-flat btn-sm">cancel</button></a>
	</div>
</div>
<?= form_close();?>

<div class="modal fade" id="generalModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-scrollable" role="document" style="height: 900px">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Baca Juga</h6>
			</div>
			<div class="modal-body p-0">
				<div class="form-group">
					<div class="fileIframe">
						<section class="contentIframe">
							<div id="loadingContent"></div>
							<iframe id="general-modal-iframe" src="" allowfullscreen><div class="d-flex justify-content-center">
								<div class="spinner-grow" id="loader1" style="width: 5rem; height: 5rem;" role="status">
									<span class="sr-only">Loading...</span>
								</div>
							</div></iframe>
						</section>
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Close</button>
			</div>
		</div>
	</div>
</div>