<input type="hidden" name="<?= $this->security->get_csrf_token_name();?>" value="<?= $this->security->get_csrf_hash();?>">
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<button type="button" id="reload" class="btn btn-tool"><i class="fas fa-sync-alt"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
			<a href="<?= backend.'news/create/add';?>"><button type="button" class="btn btn-tool btn-info" <?= ($this->access_menu->access('news-create-create',$this->session->userdata('user_id')) ? '':'disabled');?>>Add News</button></a>
		</div>
	</div>
	<div class="card-body table-responsive p-0" style="margin-bottom: ">
		<table id="table" class="table-sm table-striped" style="width:100%">
			<thead>
				<tr class="text-center text-white" style="background-color: #343a40">
					<th>No</th>
					<th data-priority="1">Title</th>
					<th>Rubrik</th>
					<th>Author</th>
					<th>Editor</th>
					<th>Created at</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>