<?= form_open(backend.'news/create/update');?>
<div class="card card-warning card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-8">
				<div class="form-group">
					<div class="input-group input-group">
						<input type="text" class="form-control form-control-sm" id="title" value="<?= $news->news_title;?>" name="title" placeholder="Title" autocomplete="off">
						<span class="input-group-append">
							<button type="button" class="btn btn-info btn-sm btn-flat" id="titleLeft">100</button>
						</span>
					</div>
				</div>
				<div class="form-group">
					<textarea class="form-control" name="excerpt" id="excerpt" placeholder="Deskripsi : 140 characters left" style="height: 70px"><?= $news->news_excerpt;?></textarea>
					<input type="range" min="0" max="140" id="rangVal" value="0" style="width: 100%;" class="progress-bar bg-danger">
				</div>
				<div class="form-group">
					<textarea id="mytextarea" name="content"><?= $news->news_content ;?></textarea>
				</div>
				<div class="form-group">
					<label>Related</label>
					<select class="form-control form-control select2 related" name="related[]" multiple="multiple">
						<?php foreach ($related as $t => $vt) {?>
							<option value="<?= $vt['news_id'];?>" selected><?= $vt['news_title'];?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<input type="text" class="form-control form-control-sm" value="<?= $news->news_prefik;?>" id="prefik" name="prefik" placeholder="Prefik">
				</div>
				<hr>
				<div class="form-group">
					<label>Rubrik</label>
					<select class="form-control form-control rubrik" name="category_id">
						<option value="<?= $news->category_id;?>" selected><?= $news->category_name;?></option>
					</select>
				</div>
				<div class="form-group">
					<label>Tags</label>
					<select class="form-control form-control tags" name="tags[]" multiple="multiple">
						<?php if (!empty($tags)) {
							foreach ($tags->result_array() as $t) {
								echo '<option selected value="'.$t['news_tag_tag_id'].'">'.$t['tag_name'].'</option>';
							}
						}?>
					</select>
				</div>
				<br>
				<div class="form-group">
					<div class="form-group row mb-1">
						<label class="col-sm-3 col-form-label">Editor</label>
						<div class="col-sm-9">
							<select class="form-control form-control editor" name="editor">
								<?php if ($news->editor_id != NULL) {?>
									<option value="<?= $news->editor_id;?>" selected><?= $this->ion_auth->user($news->author_id)->row()->first_name.' '.$this->ion_auth->user($news->author_id)->row()->last_name;?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="form-group row mb-1">
						<label class="col-sm-3 col-form-label">Author</label>

						<div class="col-sm-9">
							<select class="form-control form-control author" name="author">
								<option value="<?= $news->author_id;?>" selected><?= $this->ion_auth->user($news->author_id)->row()->first_name.' '.$this->ion_auth->user($news->author_id)->row()->last_name;?></option>
							</select>
						</div>
					</div>
					<!-- <div class="form-group row mb-1">
						<label class="col-sm-3 col-form-label">Publisher</label>
						<div class="col-sm-9">
							<select class="form-control form-control publisher" name="publisher">
								<option value="<?= $news->publisher_id;?>" selected><?= $this->ion_auth->user($news->publisher_id)->row()->first_name.' '.$this->ion_auth->user($news->publisher_id)->row()->last_name;?></option>
							</select>
						</div>
					</div> -->
				</div>
				<div class="form-group">
					<label>Topic</label>
					<select class="form-control form-control topic" name="topic">
						<?php foreach ($topic as $t => $vt) {?>
							<option value="<?= $vt['topic_id'] ;?>" selected><?= $vt['topic_name'];?></option>
						<?php }?>
					</select>
				</div>
				<br>
				<div class="form-group">
					<label>Schedule</label>
					<div class="form-group">
						<div class="input-group date" id="datetimepicker1" data-target-input="nearest">
							<input type="text" name="schedule" class="form-control form-control-sm datetimepicker-input" data-target="#datetimepicker1" value="<?= date('d-m-Y H:i:s', strtotime($news->news_published_at));?>">
							<div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
								<div class="input-group-text"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer d-flex justify-content-center p-1">
		<input type="hidden" name="newsId" value="<?= $news->news_id;?>">
		<input type="hidden" name="hash" value="<?= $news->hash;?>">
		<input type="submit" value="Publish" name="status" class="btn btn-success btn-flat btn-sm">
		<input type="submit" value="Save" name="status" id="btnAddUser" class="btn btn-info btn-flat btn-sm">
		<a href="<?= backend.'';?>"><button type="button" class="btn btn-danger btn-flat btn-sm">Cancel</button></a>
	</div>
</div>

<div class="modal fade" id="generalModal" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-xl modal-dialog-scrollable" role="document" style="height: 900px">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Baca Juga</h6>
			</div>
			<div class="modal-body p-0">
				<div class="form-group">
					<div class="fileIframe">
						<section class="contentIframe">
							<div id="loadingContent"></div>
							<iframe id="general-modal-iframe" src="" allowfullscreen><div class="d-flex justify-content-center">
								<div class="spinner-grow" id="loader1" style="width: 5rem; height: 5rem;" role="status">
									<span class="sr-only">Loading...</span>
								</div>
							</div></iframe>
						</section>
					</div>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="linkEmbed" data-backdrop="static" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h6 class="modal-title">Insert Embed</h6>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Facebook, Twiiter dan Instagram</label>
					<textarea class="form-control" style="height: 100px" id="embedContent"></textarea>
				</div>
			</div>
			<div class="modal-footer d-flex justify-content-center p-1">
				<button type="button" data-dismiss="modal" class="btn btn-danger btn-flat btn-sm">Close</button>
			</div>
		</div>
	</div>
</div>
<?= form_close();?>