<div class="card card-info">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<button type="button" id="reload" class="btn btn-tool"><i class="fas fa-sync-alt"></i></button>
			<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
		</div>
	</div>
	<div class="card-body table-responsive p-0">
		<table id="table" class="table-sm table-striped" style="width:100%">
			<thead>
				<tr class="text-center text-white" style="background-color: #343a40">
					<th>No</th>
					<th data-priority="1">Title</th>
					<th>Category</th>
					<th>Published</th>
					<th>Authors</th>
					<th><i class="fa fa-check-circle text-info"></i></th>
					<th><i class="fa fa-eye text-info" title=""></i></th>
					<th>Action</th> 
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>