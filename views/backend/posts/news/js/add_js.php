<script type="text/javascript">
	$(function () {
		$('#datetimepicker1').datetimepicker({
			format: 'DD-MM-YYYY, HH:mm:ss',
		});
	});

	$("#general-modal-iframe").on('load',function () {
		LoadingContent('loadingContent','','stop');
		$(this).contents().find(".InsertToEditor").click(function(){
			$('#generalModal').modal('hide');
			var imageId = $(this).attr('data-filename');
			var imageTitle = $(this).attr('data-title');
			var imageCaption = $(this).attr('data-caption');
			var imageSource = $(this).attr('data-source');
			var imageAuthor = $(this).attr('data-author');
			var dataImage = '<img class="content-image" src="<?= storage.'picture/high/';?>'+imageId+'" title="'+imageTitle+'" alt="'+imageTitle+'" data-caption="'+imageCaption+'" data-source="'+imageSource+'" data-author="'+imageAuthor+'">';
			tinyMCE.activeEditor.insertContent(dataImage);
		});
		$(this).contents().find(".insertNews").click(function(){
			$('#generalModal').modal('hide');
			var newsLink = $(this).attr('data-link');
			var newsTitle = $(this).attr('data-title');
			var dataNews = '<strong>Baca Juga : <a href="<?= frontPage;?>'+newsLink+'">'+newsTitle+'</a></strong>';
			tinyMCE.activeEditor.insertContent(dataNews);
		});
	});

	$('#title').keyup(function(){
		$("#title").attr('maxlength','100');
		var str=$('#title').val();
		var left = 100-str.length
		$('#titleLeft').html(left);
	});

	$('#excerpt').keyup(function(){
		$("#excerpt").attr('maxlength','140');
		var str=$('#excerpt').val();
		$('#rangVal').val(str.length);
	});

	tinymce.init({
		selector: '#mytextarea',
		height : 500,
		toolbar: 'styleselect bold italic alignleft aligncenter alignright | bullist numlist outdent indent link code | addimage readmore insertImage youtube facebook twitter instagram',
		plugins: 'code lists advlist link image autolink wordcount readmore youtube facebook twitter instagram',
		image_advtab: true,

		theme: 'modern',
		// mobile: { theme: 'mobile' },
		menubar: false,
		convert_urls: false,

		valid_elements : '+*[*]',

		setup: function (editor) {
			editor.on('init', function (args) {
				editor_id = args.target.id;
			});
			editor.addButton('addimage', {
				tooltip: 'Tambah Gambar',
				icon: 'image',
				onclick: function () {
					$('#generalModal').modal({backdrop: 'static', keyboard: false});
					$('#generalModal').find('.modal-title').text('Insert image');
					var url = '<?= backend.'IframeContent/images';?>';
					$('#general-modal-iframe').attr('src', url);
					LoadingContent('loadingContent','','start');
					return false;
				}
			});
		}
	});

	$('.related').select2({
		allowClear: true,
		placeholder: "**Select a related**",
		minimumInputLength: 2,
		ajax: {
			url: '<?= backend.'JSON_select/related';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term
				}
				return query;
			}
		}
	});

	$('.rubrik').select2({
		allowClear: true,
		placeholder: "**Select rubrik**",
		// minimumInputLength: 2,
		ajax: {
			url: '<?= backend.'JSON_select/rubrik_list';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					channel: '1'
				}
				return query;
			}
		}
	});

	$('.tags').select2({
		allowClear: true,
		placeholder: "**Type tags**",
		minimumInputLength: 2,
		ajax: {
			url: '<?= backend.'JSON_select/tags';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term
				}
				return query;
			}
		}
	});

	$('#addNewTag').on('click',function(){
		$('#addNewTagsModal').modal({backdrop: 'static', keyboard: false});
	});
	$('#newTagForm').submit(function(e) {
		e.preventDefault();
		LoadingBtn('btnSaveTag','Save','start');
		var data = new FormData(this);
		request = $.ajax({
			type: 'POST',
			url: $('#newTagForm').attr('action'),
			data: data,
			dataType: 'JSON',
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function(){
			},
		});
		request.done(function (response, textStatus, jqXHR){
			if (response.status) {
				toastr.success(response.message);
				$("#newTagForm")[0].reset();
				$('#addNewTagsModal').modal('hide');
			}else{
				toastr.error(response.message);
			}
			$('input[name=sypo]').val(response.token);
		});
		request.fail(function (jqXHR, textStatus, errorThrown){
			location.reload();
		});
		request.always(function () {
			LoadingBtn('btnSaveTag','Save','Stop');
		});
	});


	$('.author').select2({
		allowClear: true,
		placeholder: "**Select author**",
		ajax: {
			url: '<?= backend.'JSON_select/author';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'author'
				}
				return query;
			}
		}
	});

	$('.editor').select2({
		allowClear: true,
		placeholder: "**Select editor**",
		ajax: {
			url: '<?= backend.'JSON_select/author';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'editor'
				}
				return query;
			}
		}
	});

	$('.publisher').select2({
		allowClear: true,
		placeholder: "**Select publisher**",
		ajax: {
			url: '<?= backend.'JSON_select/author';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'publisher'
				}
				return query;
			}
		}
	});

	$('.topic').select2({
		allowClear: true,
		placeholder: "**Select topic**",
		ajax: {
			url: '<?= backend.'JSON_select/topic';?>',
			dataType: 'json',
			data: function (params) {
				var query = {
					search: params.term,
				}
				return query;
			}
		}
	});

	tinymce.PluginManager.add('readmore', function(editor, url) {
		var icon_url='<?= assets.'img/readmore.svg';?>';

		editor.on('init', function (args) {
			editor_id = args.target.id;

		});
		editor.addButton('readmore',
		{
			text:false,
			icon: true,
			tooltip: 'Baca Juga',
			image:icon_url,

			onclick: function () {
				$('#generalModal').modal({backdrop: 'static', keyboard: false});
				$('#generalModal').find('.modal-title').text('Baca Juga');
				var url = '<?= backend.'IframeContent/newsList';?>';
				$('#general-modal-iframe').attr('src', url);
				LoadingContent('loadingContent','','start');
				return false;
			}
		});
	});

	tinymce.PluginManager.add('youtube', function(editor, url) {
		var icon_url='<?= assets.'img/youtube.svg';?>';

		editor.on('init', function (args) {
			editor_id = args.target.id;

		});
		editor.addButton('youtube',
		{
			text: false,
			icon: true,
			tooltip: 'Embed Youtube',
			image:icon_url,

			onclick: function () {
				editor.windowManager.open({
					title: 'Youtube Embed',

					body: [{
						type: 'textbox',
						size: 50,
						height: '100px',
						name: 'youtube',
						label: 'youtube'
					}],
					onsubmit: function(e) {
						var youtubeEmbed = e.data.youtube;
						tinyMCE.activeEditor.insertContent(youtubeEmbed);
					}
				});
			}
		});
	});

	tinymce.PluginManager.add('facebook', function(editor, url) {
		var icon_url='<?= assets.'img/facebook.svg';?>';

		editor.on('init', function (args) {
			editor_id = args.target.id;

		});
		editor.addButton('facebook',
		{
			text:false,
			icon: true,
			tooltip: 'Embed facebook',
			image:icon_url,

			onclick: function () {

				editor.windowManager.open({
					title: 'Facebook Embed',

					body: [{
						type: 'textbox',
						size: 50,
						height: '100px',
						name: 'facebook',
						label: 'Facebook'
					}],
					onsubmit: function(e) {
						var facebookEmbed = e.data.facebook;
						tinyMCE.activeEditor.insertContent(facebookEmbed);
					}
				});
			}
		});
	});

	tinymce.PluginManager.add('twitter', function(editor, url) {
		var icon_url='<?= assets.'img/twitter.svg';?>';

		editor.on('init', function (args) {
			editor_id = args.target.id;

		});
		editor.addButton('twitter',
		{
			text:false,
			icon: true,
			tooltip: 'Embed Twitter',
			image:icon_url,

			onclick: function () {

				editor.windowManager.open({
					title: 'Twitter Embed',

					body: [
					{   type: 'textbox',
					size: 50,
					height: '100px',
					name: 'twitter',
					label: 'Twitter'
				}
				],
				onsubmit: function(e) {
					var twitterEmbed = e.data.twitter;
					tinyMCE.activeEditor.insertContent(twitterEmbed);
				}
			});
			}
		});
	});

	tinymce.PluginManager.add('instagram', function(editor, url) {
		var icon_url='<?= assets.'img/instagram.svg';?>';

		editor.on('init', function (args) {
			editor_id = args.target.id;

		});
		editor.addButton('instagram',
		{
			text:false,
			icon: true,
			tooltip: 'Embed Instagram',
			image:icon_url,

			onclick: function () {

				editor.windowManager.open({
					title: 'Instagram Embed',

					body: [{
						type: 'textbox',
						size: 50,
						height: '100px',
						name: 'instagram',
						label: 'Instagram'
					}],
					onsubmit: function(e) {
						var youtubeEmbedCode = e.data.instagram;
						tinyMCE.activeEditor.insertContent(youtubeEmbedCode);
					}
				});
			}
		});
	});


</script>