<script type="text/javascript">
	$(document).ready(function() {
		$('.fancybox').fancybox({
			beforeShow : function(){
				this.title =  $(this.element).data("caption");
			}
		});
		var Youtube = (function () {
			'use strict';

			var video, results;

			var getThumb = function (url, size) {
				if (url === null) {
					return '';
				}
				size    = (size === null) ? 'big' : size;
				results = url.match('[\\?&]v=([^&#]*)');
				video   = (results === null) ? url : results[1];

				if (size === 'small') {
					return 'http://img.youtube.com/vi/' + video + '/2.jpg';
				}
				return 'http://img.youtube.com/vi/' + video + '/0.jpg';
			};

			return {
				thumb: getThumb
			};
		}());

		$('#link').keyup(function(){
			var str=$('#link').val();
			var thumb = Youtube.thumb(str, 'low');
			$.get(thumb)
			.done(function() {
				$('#editVideo').find('img').attr('src',thumb);
				$('#editVideo').find('a').attr('href',str);
			}).fail(function() {
				$('#editVideo').find('img').attr('src','<?= assets.'img/youtubeThumb.png';?>');
			});
		});
	});

</script>