<script type="text/javascript">
	$(document).ready(function() {
		$('#dateRang').daterangepicker({
			autoUpdateInput: false,
			locale: {
				cancelLabel: 'Clear'
			}
		});
		$('input[name="dateRange"]').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
		});

		$('input[name="dateRange"]').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});
		var Youtube = (function () {
			'use strict';

			var video, results;

			var getThumb = function (url, size) {
				if (url === null) {
					return '';
				}
				size    = (size === null) ? 'big' : size;
				results = url.match('[\\?&]v=([^&#]*)');
				video   = (results === null) ? url : results[1];

				if (size === 'small') {
					return 'http://img.youtube.com/vi/' + video + '/2.jpg';
				}
				return 'http://img.youtube.com/vi/' + video + '/0.jpg';
			};

			return {
				thumb: getThumb
			};
		}());

		$('.fancybox').fancybox({
			beforeShow : function(){
				this.title =  $(this.element).data("caption");
			}
		});

		$('#link').keyup(function(){
			var str=$('#link').val();
			var thumb = Youtube.thumb(str, 'low');
			$.get(thumb)
			.done(function() {
				$('#previewThumb').find('img').attr('src',thumb);
			}).fail(function() {
				$('#previewThumb').find('img').attr('src','<?= assets.'img/youtubeThumb.png';?>');
			});
		});

		$('#addVideos').on('click',function() {
			LoadingBtn('addVideos','Add Video','start');
			$('#addVideosModal').modal({backdrop: 'static', keyboard: false});
			setTimeout(function() {
				LoadingBtn('addVideos','Add Video','stop');
           }, 500);
		});

		$('#addVideosForm').submit(function(e) {
			e.preventDefault();
			LoadingBtn('btnAddVideo','Save','start');
			var data = new FormData(this);
			request = $.ajax({
				type: 'POST',
				url: $('#addVideosForm').attr('action'),
				data: data,
				dataType: 'JSON',
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function(){
				},
			});
			request.done(function (response, textStatus, jqXHR){
				if (response.status) {
					location.reload();
				}else{
					toastr.error(response.message);
					LoadingBtn('btnAddVideo','Save','stop');
				}
				$('input[name=sypo]').val(response.token);
			});
			request.fail(function (jqXHR, textStatus, errorThrown){
				location.reload();
			});
			request.always(function () {
			});
		});

		$('.deleteVideo').on('click',function(){
			var videoId = $(this).attr('data-id');
			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this.!",
				cancelButtonColor: '#d33',
				cancelButtonText: 'Cancel!',
				confirmButtonText: 'Yes, Delete it',
				showCancelButton: true,
				position: 'top',
				width:'20rem',
				showLoaderOnConfirm: true,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						request = $.ajax({
							type: 'POST',
							url: '<?= backendUrl.'filemanager/filemanagerVideos/delete';?>',
							data: {'videoId':videoId,'sypo':$('input[name=sypo]').val()},
							dataType: 'JSON',
							beforeSend: function(){
							},
						});
						request.done(function (response, textStatus, jqXHR){
							if (response.status) {
								location.reload();
							}else{
								location.reload();
							}
							$('input[name=sypo]').val(response.token);
						});
						request.fail(function (jqXHR, textStatus, errorThrown){
							location.reload();
						});
						request.always(function () {
						});
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				if (result.value) {
					location.reload();
				}
			});
		});
	});

</script>