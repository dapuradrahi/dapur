<?= form_open_multipart(backend.'filemanager/filemanagerVideos/update');?>
<div class="card card-warning card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<a href="<?= backend.'filemanager/filemanagerVideos/view';?>"><button type="button" class="btn btn-tool btn-danger">Back</button></a>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-4" id="editVideo">
				<a data-fancybox="gallery" href="<?= storage.'picture/high/'.$dataVideo->link;?>" data-caption="<?= 'Title : '.$dataVideo->title.' | Type : '.$dataVideo->source;?>"><img src="<?= $this->apps->thumbnailYoutube($dataVideo->link,'LOW');?>" class="img-fluid">
				</a>
				<!-- <a data-fancybox="gallery" href="<?= storage.'picture/high/'.$dataVideo->link;?>" data-caption="<?= 'Title : '.$dataVideo->title.' | Type : '.$dataVideo->source;?>"><img src="<?= $this->apps->thumbnailYoutube($dataVideo->link,'LOW');?>" class="img-fluid">
				</a> -->
			</div>
			<div class="col-md-8">
				<div class="form-group row mb-2">
					<label class="col-sm-2 col-form-label">URL</label>
					<div class="col-sm-9">
						<input type="text" name="link" id="link" class="form-control form-control-sm" value="<?= $dataVideo->link;?>" placeholder="Title">
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-2 col-form-label">Title</label>
					<div class="col-sm-9">
						<input type="text" name="title" class="form-control form-control-sm" value="<?= $dataVideo->title;?>" placeholder="Title">
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-2 col-form-label">Description</label>
					<div class="col-sm-9">
						<textarea class="form-control" name="description"><?= $dataVideo->description;?></textarea>
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-2 col-form-label">Source</label>
					<div class="col-sm-9">
						<input type="text" name="source" class="form-control form-control-sm" value="<?= $dataVideo->source;?>" placeholder="Source">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-2 col-form-label">Status</label>
					<div class="col-sm-9 mt-1">
						<div class="icheck-primary d-inline">
							<input type="radio" id="statusActive" value="1" name="status" <?= ($dataVideo->status == 1 ? 'checked':'') ;?>>
							<label for="statusActive">Publish</label>
						</div>
						<div class="icheck-danger d-inline">
							<input type="radio" id="statusInActive" value="0" name="status" <?= ($dataVideo->status == 0 ? 'checked':'') ;?>>
							<label for="statusInActive">Hide</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer d-flex justify-content-center p-1">
		<input type="hidden" name="videoId" value="<?= $dataVideo->videos_id;?>">
		<input type="hidden" name="channel" value="<?= $dataVideo->channel;?>">
		<button type="submit" class="btn btn-flat btn-sm btn-info" id="savePicture">Save</button>
	</div>
</div>
<?= form_close();?>