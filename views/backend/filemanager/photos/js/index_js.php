<script type="text/javascript">
	$(document).ready(function() {
		$('#dateRang').daterangepicker({
			autoUpdateInput: false,
			locale: {
				cancelLabel: 'Clear'
			}
		 });
		$('input[name="dateRange"]').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
		});

		$('input[name="dateRange"]').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
		});

		$('.fancybox').fancybox({
			beforeShow : function(){
				this.title =  $(this.element).data("caption");
			}
		});
		bsCustomFileInput.init();
		$('#addPictures').on('click',function() {
			LoadingBtn('addPictures','','start');
			$('#addPicturesModal').modal({backdrop: 'static', keyboard: false});
			setTimeout(function() {
				LoadingBtn('addPictures','Add Image','stop');
           }, 500);
		});

		$('#addPictureForm').submit(function(e) {
			e.preventDefault();
			LoadingBtn('btnAddUser','Save','start');
			var data = new FormData(this);
			request = $.ajax({
				type: 'POST',
				url: $('#addPictureForm').attr('action'),
				data: data,
				dataType: 'JSON',
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function(){
				},
			});
			request.done(function (response, textStatus, jqXHR){
				if (response.status) {
					toastr.success(response.message);
					LoadingBtn('btnAddUser','Save','Stop');
					location.reload();
				}else{
					toastr.error(response.message);
					LoadingBtn('btnAddUser','Save','Stop');
					// location.reload();
				}
				$('input[name=sypo]').val(response.token);
			});
			request.fail(function (jqXHR, textStatus, errorThrown){
				location.reload();
			});
			request.always(function () {
			});
		});
		$('.editPicture').on('click',function(){
			$('#editPicturesModal').modal({backdrop: 'static', keyboard: false});
			var pictureId = $(this).attr('data-id');
			request = $.ajax({
				type: 'POST',
				url: '<?= backendUrl.'filemanager/filemanagerPhotos/edit';?>',
				data: {'pictureId':pictureId,'sypo':$('input[name=sypo]').val()},
				dataType: 'JSON',
				beforeSend: function(){
				},
			});
			request.done(function (response, textStatus, jqXHR){
				if (response.status) {
					$('#dataUpdate').html(response.data);
				}else{
					// location.reload();
				}
				$('input[name=sypo]').val(response.token);
			});
			request.fail(function (jqXHR, textStatus, errorThrown){
				// location.reload();
			});
			request.always(function () {
			});
		});

		$('.deletePicture').on('click',function(){
			var pictureId = $(this).attr('data-id');
			swal.fire({
				title: 'Are you sure?',
				text: "You won't be able to revert this.!",
				cancelButtonColor: '#d33',
				cancelButtonText: 'Cancel!',
				confirmButtonText: 'Yes, Delete it',
				showCancelButton: true,
				position: 'top',
				width:'20rem',
				showLoaderOnConfirm: true,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						request = $.ajax({
							type: 'POST',
							url: '<?= backendUrl.'filemanager/filemanagerPhotos/delete';?>',
							data: {'pictureId':pictureId,'sypo':$('input[name=sypo]').val()},
							dataType: 'JSON',
							beforeSend: function(){
							},
						});
						request.done(function (response, textStatus, jqXHR){
							if (response.status) {
								location.reload();
							}else{
								location.reload();
							}
							$('input[name=sypo]').val(response.token);
						});
						request.fail(function (jqXHR, textStatus, errorThrown){
							location.reload();
						});
						request.always(function () {
						});
					});
				},
				allowOutsideClick: () => !Swal.isLoading()
			}).then((result) => {
				if (result.value) {
					location.reload();
				}
			});
		});
	});

</script>