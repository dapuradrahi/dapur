<?= form_open_multipart(backend.'filemanager/filemanagerPhotos/update');?>
<div class="card card-warning card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $title;?></h3>
		<div class="card-tools">
			<a href="<?= backend.'filemanager/filemanagerPhotos/view';?>"><button type="button" class="btn btn-tool btn-danger">Back</button></a>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-md-4">
				<a data-fancybox="gallery" href="<?= storage.'picture/high/'.$dataPicture->file_name;?>" data-caption="<?= 'Title : '.$dataPicture->title.' | Type : '.$dataPicture->type;?>"><img src="<?= storage.'picture/low/'.$dataPicture->file_name;?>" class="img-fluid"></a>
			</div>
			<div class="col-md-8">
				<div class="form-group row mb-2">
					<label class="col-sm-2 col-form-label">Images</label>
					<div class="col-sm-9">
						<div class="input-group">
							<div class="custom-file">
								<input type="file" name="picture" class="custom-file-input" id="PictureInputFile">
								<label class="custom-file-label" for="PictureInputFile">Choose file</label>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-2 col-form-label">Title</label>
					<div class="col-sm-9">
						<input type="text" name="title" class="form-control form-control-sm" value="<?= $dataPicture->title;?>" placeholder="Title">
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-2 col-form-label">Caption</label>
					<div class="col-sm-9">
						<textarea class="form-control" name="caption"><?= $dataPicture->caption;?></textarea>
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-2 col-form-label">Source</label>
					<div class="col-sm-9">
						<input type="text" name="source" class="form-control form-control-sm" value="<?= $dataPicture->source;?>" placeholder="Source">
					</div>
				</div>
				<div class="form-group row mb-2">
					<label class="col-sm-2 col-form-label">Type</label>
					<div class="col-sm-9">
						<select class="form-control form-control-sm" name="type">
							<option value="ilustration" <?= ($dataPicture->type == 'ilustration' ? 'selected':'') ;?>>Ilustration</option>
							<option value="graphic-design" <?= ($dataPicture->type == 'graphic-design' ? 'selected':'') ;?>>Graphic Design</option>
							<option value="photography" <?= ($dataPicture->type == 'photography' ? 'selected':'') ;?>>Photography</option>
						</select>
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-2 col-form-label">Status</label>
					<div class="col-sm-9 mt-1">
						<div class="icheck-primary d-inline">
							<input type="radio" id="statusActive" value="1" name="status" <?= ($dataPicture->picture_status == 1 ? 'checked':'') ;?>>
							<label for="statusActive">Publish</label>
						</div>
						<div class="icheck-danger d-inline">
							<input type="radio" id="statusInActive" value="0" name="status" <?= ($dataPicture->picture_status == 0 ? 'checked':'') ;?>>
							<label for="statusInActive">Hide</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer d-flex justify-content-center p-1">
		<input type="hidden" name="pictureId" value="<?= $dataPicture->picture_id;?>">
		<input type="hidden" name="channel" value="<?= $dataPicture->channel;?>">
		<input type="hidden" name="author" value="<?= $dataPicture->user_id;?>">
		<input type="hidden" name="file_name" value="<?= $dataPicture->file_name;?>">
		<button type="submit" class="btn btn-flat btn-sm btn-info" id="savePicture">Save</button>
	</div>
</div>
<?= form_close();?>