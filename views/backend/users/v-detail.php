<div class="card card-widget widget-user">
	<div class="widget-user-header text-white" style="background: url('<?= assets ;?>img/bg-profile.png') center center;">
		<h3 class="widget-user-username text-right"><?= $userdata['first_name'].' '.$userdata['last_name'];?></h3>
		<h5 class="widget-user-desc text-right"><?= $this->ion_auth->get_users_groups($userdata['id'])->row()->name;?></h5>
	</div>
	<div class="widget-user-image">
		<img class="img-circle" src="<?= storage.'profile/thumb/'.$userdata['picture'] ;?>" alt="User Avatar">
	</div>
	<div class="card-footer">
	</div>
</div>
<div class="card card-widget">
	<ul class="nav nav-tabs pt-2 pl-3" id="custom-content-below-tab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="profileTab" data-toggle="pill" href="#v-profile" role="tab" aria-controls="v-profile" aria-selected="true">Profile</a>
		</li>
	</ul>
	<div class="tab-content" id="postNewsTabContent">
		<div class="tab-pane fade show active" id="v-profile" role="tabpanel" aria-labelledby="profileTab">
			<?= form_open_multipart('root-cms/users/update');?>
			<div class="card-body">
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">First name</label>
					<div class="col-sm-8">
						<input type="text" name="first_name" value="<?= $userdata['first_name'];?>" class="form-control form-control-sm" placeholder="First name" autocomplete="off">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Last Name</label>
					<div class="col-sm-8">
						<input type="text" name="last_name" value="<?= $userdata['last_name'];?>" class="form-control form-control-sm" placeholder="Last name" autocomplete="off">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Display Name</label>
					<div class="col-sm-8">
						<input type="text" name="display_name" value="<?= $userdata['display_name'];?>" class="form-control form-control-sm" placeholder="Display name" autocomplete="off">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Gender</label>
					<div class="col-sm-8 mt-1">
						<div class="icheck-primary d-inline">
							<input type="radio" id="genderMale" value="l" name="gender" <?= ($userdata['gender'] == 'l' ? 'checked':'');?>>
							<label for="genderMale">Male</label>
						</div>
						<div class="icheck-purple d-inline">
							<input type="radio" id="genderFemale" value="p" name="gender" <?= ($userdata['gender'] == 'p' ? 'checked':'');?>>
							<label for="genderFemale">Female</label>
						</div>
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Phone</label>
					<div class="col-sm-8">
						<input type="number" name="phone" value="<?= $userdata['phone'];?>" class="form-control form-control-sm" placeholder="Phone">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Email</label>
					<div class="col-sm-8">
						<input type="email" name="email" value="<?= $userdata['email'];?>" class="form-control form-control-sm" placeholder="Email" autocomplete="off" disabled>
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Company</label>
					<div class="col-sm-8">
						<input type="text" name="company" value="<?= $userdata['company'];?>" class="form-control form-control-sm" placeholder="Company" autocomplete="off" disabled>
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">Username</label>
					<div class="col-sm-8">
						<input type="text" name="username" value="<?= $userdata['username'];?>" class="form-control form-control-sm" placeholder="Username" autocomplete="off">
					</div>
				</div>
				<div class="form-group row mb-1">
					<label class="col-sm-3 col-form-label">password</label>
					<div class="col-sm-8">
						<input type="password" name="password" class="form-control form-control-sm" placeholder="Empty if not change" autocomplete="off">
					</div>
				</div>
				<div class="form-group row pb-0 mb-0">
					<label class="col-sm-3 col-form-label">Picture</label>
					<div class="col-sm-8">
						<input type="file" name="picture" class="">
						<br>
						<span class="text-red">Empty if not change</span><br>
						<span class="text-red">max size 1080 x 1350 px & png | jpg | jpeg format</span>
					</div>
				</div>
				<hr>
				<span class="text-info">Hubungi admin untuk merubah informasi yang tidak bisa di rubah pengguna</span>
			</div>
			<div class="card-footer text-center">
				<input type="hidden" name="uid" value="<?= $userdata['id'];?>">
				<input type="hidden" name="oldPicture" value="<?= $userdata['picture'];?>">
				<button type="button" class="btn btn-danger btn-flat" onclick="window.location.href='<?= backend.'settings/users';?>'">Back</button>
				<button type="submit" class="btn btn-info btn-flat">Update</button>
			</div>
			<?= form_close();?>
		</div>
	</div>
</div>