<div class="card card-widget">
	<ul class="nav nav-tabs pt-2 pl-3" id="custom-content-below-tab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="harian" data-toggle="pill" href="#v-harian" role="tab" aria-controls="v-harian" aria-selected="true">Harian</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="mingguan" data-toggle="pill" href="#v-mingguan" role="tab" aria-controls="v-mingguan" aria-selected="true">Mingguan</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="bulanan" data-toggle="pill" href="#v-bulanan" role="tab" aria-controls="v-bulanan" aria-selected="true">Bulanan</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="report" data-toggle="pill" href="#v-report" role="tab" aria-controls="v-report" aria-selected="true">Custom Report</a>
		</li>
	</ul>
	<div class="tab-content p-3">
		<div class="tab-pane fade show active" id="v-harian" role="tabpanel" aria-labelledby="harian">
			<ul class="list-group list-group-unbordered mb-2">
				<canvas id="canvas_harian"></canvas>
			</ul>
		</div>
		<div class="tab-pane fade show" id="v-mingguan" role="tabpanel" aria-labelledby="mingguan">
			<ul class="list-group list-group-unbordered mb-2">
				<canvas id="canvas_mingguan"></canvas>
			</ul>
		</div>
		<div class="tab-pane fade show" id="v-bulanan" role="tabpanel" aria-labelledby="bulanan">
			<ul class="list-group list-group-unbordered mb-2">
				<canvas id="canvas_bulanan"></canvas>
			</ul>
		</div>
		<div class="tab-pane fade show" id="v-report" role="tabpanel" aria-labelledby="report">
			<ul class="list-group list-group-unbordered mb-2">
				<table id="table" class="table-sm table-striped" style="width:100%">
					<thead>
						<tr class="text-center text-white" style="background-color: #343a40">
							<th>No</th>
							<th data-priority="1">Title</th>
							<th>Rubrik</th>
							<th>Author</th>
							<th>Editor</th>
							<th>Published at</th>
						</tr>
					</thead>
				</table>
			</ul>
		</div>
	</div>
</div>