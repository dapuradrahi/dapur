<script type="text/javascript">
	$(document).ready(function() {
		table = $('#table').DataTable({
			processing: true, 
			serverSide: true, 
			responsive: true,
			order: [], 
			dom: "'<'row pt-2 pr-2 pl-2'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-2 filter'><'col-sm-12 col-md-4 filterDate'><'col-sm-12 col-md-4'f>>'"+"<'row'<'col-sm-12'tr>>"+"<'row p-2'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
			ajax: {
				url: '<?= backendUrl.'report/view/news';?>',
				type: "POST",
				data: function (data) {
					data.uid = ($('#filter').val() ? $('#filter').val():null);
					data.filterDate = ($('#filterDate').val()) ? $('#filterDate').val() : '<?= date('d/m/Y 00:00:00', strtotime('-1 day'));?> - <?= date('d/m/Y H:i:s');?>';
				}
			},

			columnDefs: [
			{ responsivePriority: 1, targets: 1 },
			{ responsivePriority: 1, targets: -1 },
			{ width: "50%", "targets": [1] },
			{ 
				targets: [1,2,3,4,5], 
				orderable: false,
			},
			{ 
				targets: [0,5], 
				className: 'text-center',
			}],
			drawCallback: function(dt) {
				$('#filterDate').daterangepicker({
					timePicker: true,
					timePickerIncrement: 30,
					locale: {
						format: 'DD/MM/YYYY HH:mm:ss'
					}
				});

				$('.select2').select2({
					placeholder: "Filter User",
					allowClear: true,
					ajax: {
						url: '<?= backend.'JSON_select/author';?>',
						dataType: 'json',
						data: function (params) {
							var query = {
								search: params.term,
								type: 'author'
							}
							return query;
						}
					}
				});

				$('#filterDate').on('apply.daterangepicker', function(ev, picker) {
					table.ajax.reload();
				});
				
				$('#filter').on('change',function() {
					table.ajax.reload();
				});

			}
		});
		$("div.filter").html('<select class="form-control form-control-sm select2" style="width: 100%;" name="filter" id="filter"></select>');
		$("div.filterDate").html('<input type="text" name="filterDate" id="filterDate" class="form-control form-control-sm float-right">');
	});

</script>
<script>
	$(function(){
		var hash = window.location.hash;
		hash && $('ul.nav a[href="' + hash + '"]').tab('show');

		$('.nav-tabs a').click(function (e) {
			$(this).tab('show');
			var scrollmem = $('body').scrollTop();
			window.location.hash = this.hash;
			$('html,body').scrollTop(scrollmem);
		});
	});

	var dp = [<?= $harian['dp'];?>];
	var ds = [<?= $harian['ds'];?>];
	var dd = [<?= $harian['dd'];?>];
	var dnp = [<?= $harian['dnp'];?>];
	var dnv = [<?= $harian['dnv'];?>];

	
	var wp = [<?= $mingguan['wp'];?>];
	var ws = [<?= $mingguan['ws'];?>];
	var wd = [<?= $mingguan['wd'];?>];
	var wnp = [<?= $mingguan['wnp'];?>];
	var wnv = [<?= $mingguan['wnv'];?>];

	var md = [<?= $bulanan['md'];?>];
	var mp = [<?= $bulanan['mp'];?>];
	var ms = [<?= $bulanan['ms'];?>];
	var mnp = [<?= $bulanan['mnp'];?>];
	var mnv = [<?= $bulanan['mnv'];?>];

	const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "July", "Agustus", "September", "Oktober", "November", "Desember"];
	const date = new Date();

	var harian = {
		type: 'line',
		data: {
			labels: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat','Sabtu'],
			datasets: [{
				label: 'News Publish',
				backgroundColor: window.chartColors541.info,
				borderColor: window.chartColors541.info,
				fill: false,
				data: dp,
			},{
				label: 'News Schedule',
				backgroundColor: window.chartColors541.green,
				borderColor: window.chartColors541.green,
				fill: false,
				data: ds,
			},{
				label: 'News Draft',
				backgroundColor: window.chartColors541.black,
				borderColor: window.chartColors541.black,
				fill: false,
				data: dd,
			},{
				label: 'News Photo',
				backgroundColor: window.chartColors541.yellow,
				borderColor: window.chartColors541.yellow,
				fill: false,
				data: dnp,
			},{
				label: 'News Video',
				backgroundColor: window.chartColors541.blue,
				borderColor: window.chartColors541.blue,
				fill: false,
				data: dnv,
			}]
		},
		options: {
			responsive: true,
			title: {
				display: true,
				text: 'POST GRAPH'
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Minggu Ini'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Jumlah Posting'
					},
					ticks: {
						min: 0,
						// max: 10,
						stepSize: 1
					}
				}]
			}
		}
	};

	var mingguan = {
		type: 'line',
		data: {
			labels: ['Minggu 1', 'Minggu ke 2', 'Minggu ke 3', 'Minggu ke 4','Minggu ke 5'],
			datasets: [{
				label: 'News Publish',
				backgroundColor: window.chartColors541.info,
				borderColor: window.chartColors541.info,
				fill: false,
				data: wp,
			},{
				label: 'News Schedule',
				backgroundColor: window.chartColors541.green,
				borderColor: window.chartColors541.green,
				fill: false,
				data: ws,
			},{
				label: 'News Draft',
				backgroundColor: window.chartColors541.black,
				borderColor: window.chartColors541.black,
				fill: false,
				data: wd,
			},{
				label: 'News Photo',
				backgroundColor: window.chartColors541.yellow,
				borderColor: window.chartColors541.yellow,
				fill: false,
				data: wnp,
			},{
				label: 'News Video',
				backgroundColor: window.chartColors541.blue,
				borderColor: window.chartColors541.blue,
				fill: false,
				data: wnv,
			}]
		},
		options: {
			responsive: true,
			title: {
				display: true,
				text: monthNames[date.getMonth()],
			},
			scales: {
				xAxes: [{
					display: true,
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Jumlah Posting'
					},
					ticks: {
						min: 0,
						// max: 10,
						stepSize: 100
					}
				}]
			}
		}
	};

	var bulanan = {
		type: 'line',
		data: {
			labels: ['Januari', 'Februari', 'Maret', 'April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'],
			datasets: [{
				label: 'News Publish',
				backgroundColor: window.chartColors541.info,
				borderColor: window.chartColors541.info,
				fill: false,
				data: mp,
			},{
				label: 'News Schedule',
				backgroundColor: window.chartColors541.green,
				borderColor: window.chartColors541.green,
				fill: false,
				data: ms,
			},{
				label: 'News Draft',
				backgroundColor: window.chartColors541.black,
				borderColor: window.chartColors541.black,
				fill: false,
				data: md,
			},{
				label: 'News Photo',
				backgroundColor: window.chartColors541.yellow,
				borderColor: window.chartColors541.yellow,
				fill: false,
				data: mnp,
			},{
				label: 'News Video',
				backgroundColor: window.chartColors541.blue,
				borderColor: window.chartColors541.blue,
				fill: false,
				data: mnv,
			}]
		},
		options: {
			responsive: true,
			title: {
				display: true,
				text: monthNames[date.getMonth()],
			},
			scales: {
				xAxes: [{
					display: true,
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Jumlah Posting'
					},
					ticks: {
						min: 0,
						// max: 10,
						stepSize: 100
					}
				}]
			}
		}
	};

	window.onload = function() {
		var canvas_harian = document.getElementById('canvas_harian').getContext('2d');
		window.myLine = new Chart(canvas_harian, harian);

		var canvas_mingguan = document.getElementById('canvas_mingguan').getContext('2d');
		window.myLine = new Chart(canvas_mingguan, mingguan);

		var canvas_bulanan = document.getElementById('canvas_bulanan').getContext('2d');
		window.myLine = new Chart(canvas_bulanan, bulanan);
	};
</script>