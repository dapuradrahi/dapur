<nav class="main-header navbar navbar-expand navbar-white navbar-light">
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
		</li>
	</ul>
	<ul class="navbar-nav ml-auto">
		<li class="nav-item dropdown user-menu">
			<a href="<?= backend.'users/'.$userLogin['user_id'];?>" class="nav-link">
				<img src="<?= storage;?>profile/thumb/<?= $userLogin['picture'];?>" class="user-image img-circle elevation-1" alt="User Image">
				<span class="d-none d-md-inline"><?= $userLogin['first_name'].' '.$userLogin['last_name'];?></span>
			</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="<?= base_url();?>auth/logout"><i class="fas fa-sign-out-alt"></i></a>
		</li>
	</ul>
</nav>