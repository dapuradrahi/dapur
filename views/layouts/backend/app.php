<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?= $title;?> | <?= app_name;?></title>
	<link rel="stylesheet" href="<?= assets;?>backend/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="<?= assets;?>backend/plugins/toastr/toastr.min.css">
	<link rel="stylesheet" href="<?= assets;?>backend/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
	
	<?php foreach($css as $file){?>
<link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" />
	<?php }?>
	
	<link rel="stylesheet" href="<?= assets;?>backend/css/adminlte.css">
	<link rel="stylesheet" href="<?= assets;?>backend/css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini text-sm">
	<div class="wrapper">
		<?= $this->load->get_section('navbar');?>
		<?= $this->load->get_section('sidebar');?>
		<div class="content-wrapper" style="padding-top: 10px">
			<div class="content">
				<div class="container-fluid">
					<?php echo $output; ?>
				</div>
			</div>
		</div>
		<footer class="main-footer">
			<div class="float-right d-none d-sm-inline">
				<?= app_version;?>
			</div>
			<strong>&copy; 2020-<?=date('Y');?> </strong> <?= cms_name;?>
		</footer>
	</div>
	<script src="<?= assets;?>backend/plugins/jquery/jquery.min.js"></script>
	<script src="<?= assets;?>backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?= assets;?>backend/plugins/moment/moment.min.js"></script>
	<script src="<?= assets;?>backend/plugins/toastr/toastr.min.js"></script>
	<script src="<?= assets;?>backend/plugins/sweetalert2/sweetalert2.all.min.js"></script>
	<script src="<?= assets;?>backend/js/app.js"></script>
	<?php foreach($js as $file){?>
<script src="<?php echo $file; ?>"></script>
    <?php } ?>
	<script src="<?= assets;?>backend/js/adminlte.min.js"></script>
	<?php $error = $this->session->flashdata('error'); if(isset($error)){?>
		<script type="text/javascript">
			toastr.error('<?= $error;?>');
		</script>
	<?php } ?>
	<?php $success = $this->session->flashdata('success'); if(isset($success)){?>
		<script type="text/javascript">
			toastr.success('<?= $success;?>');
		</script>
	<?php } ?>
	<?= $this->load->get_section('js');?>	
</body>
</html>
