<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<a href="<?= backend;?>" class="brand-link">
		<img src="<?= assets;?>backend/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
		style="opacity: .8">
		<span class="brand-text font-weight-light"><?= cms_name;?></span>
	</a>
	<div class="sidebar">
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
				<?php foreach ($menu['menu'] as $k => $v) {?>
					<?php if (isset($v['children'])) {?>
						<?php if (in_array($v['name'], $menu['mainMenu'])) {?>
						<li class="nav-item has-treeview <?= ($this->uri->segment(2) == $v['name'] ? 'menu-open':'');?>">
							<a href="<?= $v['url'];?>" class="nav-link <?= ($this->uri->segment(2) == $v['name'] ? 'active':'');?>">
								<i class="<?= $v['icon'];?>"></i>
								<p><?= $v['alias'];?><i class="fas fa-angle-left right"></i></p>
							</a>
							<ul class="nav nav-treeview">
								<?php foreach ($v['children'] as $key => $value) {?>
									<?php if (in_array($value['name'], $menu['subMenu'])) {?>
										<li class="nav-item">
											<a href="<?= $value['url'];?>" class="nav-link <?= ($this->uri->segment(3) == $value['name'] ? 'active':'');?>">
												<i class="far fa-circle nav-icon text-red"></i>
												<p><?= $value['alias'];?></p>
											</a>
										</li>
									<?php } ;?>
								<?php }?>
							</ul>
						</li>
					<?php }?>
					<?php }else{?>
						<?php if (in_array($v['name'], $menu['mainMenu'])) {?>
							<li class="nav-item">
								<a href="<?= $v['url'];?>" class="nav-link <?= ($this->uri->segment(2) == $v['name'] ? 'active':'');?>">
									<i class="<?= $v['icon'];?>"></i>
									<p><?= $v['alias'];?></p>
								</a>
							</li>
						<?php }?>
					<?php }?>
				<?php }?>
			</ul>
		</nav>
	</div>
</aside>