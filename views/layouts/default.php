<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?= app_name;?></title>
	<link rel="stylesheet" href="<?= assets;?>backend/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="<?= assets;?>backend/css/adminlte.min.css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini text-sm">
	<div class="wrapper">
		<nav class="main-header navbar navbar-expand navbar-white navbar-light">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
				</li>
			</ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown user-menu">
					<a href="#" class="nav-link">
						<img src="<?= assets;?>backend/img/user2-160x160.jpg" class="user-image img-circle elevation-1" alt="User Image">
						<span class="d-none d-md-inline">Alexander Pierce</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#"><i class="fas fa-external-link-alt"></i></a>
				</li>
			</ul>
		</nav>
		<aside class="main-sidebar sidebar-dark-primary elevation-4">
			<a href="index3.html" class="brand-link">
				<img src="<?= assets;?>backend/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
				style="opacity: .8">
				<span class="brand-text font-weight-light"><?= cms_name;?></span>
			</a>
			<div class="sidebar">
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
						<li class="nav-item">
							<a href="<?= backend.'users';?>" class="nav-link">
								<i class="nav-icon fas fa-th"></i>
								<p>Users</p>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</aside>
		<div class="content-wrapper">
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1 class="m-0 text-dark">Starter Page</h1>
						</div>
						<div class="col-sm-6">
							<?= $breadcrumb;?>
						</div>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="container-fluid">
					<?php echo $output; ?>
				</div>
			</div>
		</div>
		<aside class="control-sidebar control-sidebar-dark">
			<div class="p-3">
				<h5>Title</h5>
				<p>Sidebar content</p>
			</div>
		</aside>
		<footer class="main-footer">
			<div class="float-right d-none d-sm-inline">
				<?= app_version;?>
			</div>
			<strong>&copy; 2020-<?=date('Y');?> </strong> <?= cms_name;?>
		</footer>
	</div>
	<script src="<?= assets;?>backend/plugins/jquery/jquery.min.js"></script>
	<script src="<?= assets;?>backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?= assets;?>backend/js/adminlte.min.js"></script>
</body>
</html>
