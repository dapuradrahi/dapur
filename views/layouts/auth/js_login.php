<script type="text/javascript">
	$(document).ready(function () {
		$('#loginForm').validate({
			rules: {
				identity: {
					required: true,
				},
				password: {
					required: true,
				},
			},
			messages: {
				identity: {
					required: "Please enter a email address",
				},
				password: {
					required: "Please provide a password",
				},
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
</script>