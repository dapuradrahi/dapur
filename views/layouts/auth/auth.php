<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?= $title;?> | <?= app_name;?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="<?= assets;?>backend/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?= assets;?>backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<link rel="stylesheet" href="<?= assets;?>backend/plugins/toastr/toastr.min.css">
	<link rel="stylesheet" href="<?= assets;?>backend/css/adminlte.min.css">
	<link rel="stylesheet" href="<?= assets;?>backend/css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
	<?= $output;?>
	<script src="<?= assets;?>backend/plugins/jquery/jquery.min.js"></script>
	<script src="<?= assets;?>backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?= assets;?>backend/plugins/jquery-validation/jquery.validate.min.js"></script>
	<script src="<?= assets;?>backend/plugins/jquery-validation/additional-methods.min.js"></script>
	<script src="<?= assets;?>backend/plugins/toastr/toastr.min.js"></script>
	<script src="<?= assets;?>backend/js/adminlte.min.js"></script>
	<script src="<?= assets;?>backend/js/app.js"></script>
	<?php $error = $this->session->flashdata('error'); if(isset($error)){?>
		<script type="text/javascript">
			toastr.error('<?= $error;?>');
		</script>
	<?php } ?>
	<?= $this->load->get_section('js');?>
</body>
</html>