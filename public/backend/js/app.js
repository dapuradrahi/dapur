function LoadingBtn(id,text,status){
	if (status == 'start') {
		$('#'+id).prop("disabled", true);
		$('#'+id).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');
	}else{
		$('#'+id).prop("disabled", false);
		$('#'+id).html(text);
	}
}

function LoadingContent(id,text,status){
	if (status == 'start') {
		$('#'+id).html('<div class="d-flex justify-content-center"><div class="spinner-border text-danger" role="status"><span class="sr-only">Loading...</span></div></div>');
	}else{
		$('#'+id).html('');
	}
}

toastr.options = {
	"closeButton": false,
	"debug": false,
	"newestOnTop": true,
	"progressBar": true,
	"positionClass": "toast-top-center",
	"preventDuplicates": false,
	"onclick": null,
	"showDuration": "1000",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "500",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "slideDown",
	"hideMethod": "slideUp"
}